function pagenavi(){
	if(_input = document.getElementsByName('paged')[0]){
		if(_input.addEventListener){
			_input.addEventListener('change',myFunction);
		}else if(_input.attachEvent){
			_input.attachEvent("onchange", myFunction);
		}
	}
}
function myFunction(){
	var _urlsend = this.baseURI.split("page");
	if(1 >= Number(this.value)){
		window.location.href = _urlsend[0];
	}else{
		window.location.href = _urlsend[0] + 'page/' + this.value + '/';
	}
}
pagenavi();
$.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
    // Prevent default anchor event
    e.preventDefault();

    // Set values for window
    intWidth = intWidth || '500';
    intHeight = intHeight || '400';
    strResize = (blnResize ? 'yes' : 'no');

    // Set title and open popup with focus on it
    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
            strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
            objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
}
$('.snsSub li a').on("click", function(e) {
    $(this).customerPopup(e);
});