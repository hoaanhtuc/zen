jQuery(document).ready(function($) {
    $('a[href*="#"]:not([href="#"])').click(function(event) {
        var w_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var _target = $(this).attr('href');
        if($(_target).length){
             event.preventDefault();
            var target = $(_target);
            if(w_w > 767){
                var _outh = $('header').outerHeight();
            }else{
                var _outh = $('.logo-page').outerHeight();
            }
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - _outh
                }, 500,  'swing',function(){});
                return false;
            }else{
                $('html, body').animate({
                    scrollTop: 0
                }, 500 , 'swing',function(){});
            }
        }else{
            $('html, body').animate({
                scrollTop: 0
            }, 500 , 'swing',function(){});
            event.stopPropagation();
        }
    });
    $('.btn_menu').click(function(){
        $(this).toggleClass('active');
        $('header').toggleClass('active');
        $('body').toggleClass('active');
    });

    
});
var _posfix ,_btop,_clienth,_botcss;
window.addEventListener('load',function(event) {
    access_scroll();
    $(window).on('scroll resize',function(){
        access_scroll();
    });
});
function access_scroll(){
    _posfix = document.getElementsByClassName('mapb')["0"].offsetTop;
    _btop  = $('.btn_2top');
    _clienth = document.documentElement.clientHeight;
    _botcss = _btop.css('bottom').replace('px','');
   if(typeof _posfix != "undefined"){
        var _poswin = $(window).scrollTop();
        var _bodyh = $('body').outerHeight();
        if(_poswin + _clienth - (_btop.outerHeight()/2) - _botcss >= _posfix){
            _btop.css({
                "bottom" : (_poswin + _clienth - _posfix - (_btop.outerHeight()/2))
            });
        }else{
            _btop.removeAttr('style');
        }
    }
}
$(".imgs_demo ul").slick({
    dots: false,
    infinite: false,
    arrows: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3 ,
    drag:false,
    responsive: [
    {
        breakpoint: 767,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots:false,
            arrows:true,
            infinite:true,
            drag:true,
        }
    }
    ]
});

    
