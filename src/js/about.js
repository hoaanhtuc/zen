function access_see_more_btn(){
	var w_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var _over = document.getElementsByClassName('blist_over');
	var _mark = document.getElementsByClassName('mark');
	var _see_more = document.getElementsByClassName('see_more');
	if(w_w < 768){
		_over["0"].style.height = _mark["0"].offsetTop + _see_more["0"].clientHeight + 'px';
	}else{
		_over["0"].removeAttribute('style');
	}
}

function toggle_content(event){
	var w_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var _over = document.getElementsByClassName('blist_over');
	var _mark = document.getElementsByClassName('mark');
	var _see_more = document.getElementsByClassName('see_more');
	var _x = _mark["0"].offsetTop + _see_more["0"].clientHeight;
	var _max = _over["0"].scrollHeight;
	var _delay = 500;
	var _range = (_max - _x)*3/_delay;
	function _downx(){
		_x += _range;
		if(_x > _max){
			_over["0"].style.height = _max+'px';
			myStopFunction();
		}
		_over["0"].style.height = _x+'px';
	}
	function _upx(){
		
		_max -= _range;
		if(_max < _x){
			_over["0"].style.height = _x+'px';
			myStopFunction();
		}
		_over["0"].style.height = _max+'px';
	}
	function myStopFunction() {
	    clearInterval(_mytime);
	    clearInterval(_mytime_ex);
	}
	if(w_w < 768){
		if(-1 < _over["0"].className.indexOf('act')){
			_over["0"].className = _over["0"].className.replace(/\b act\b/g, "");
			var _mytime_ex = setInterval(_upx,1);
		}else{
			_over["0"].className += ' act';
			var _mytime = setInterval(_downx,1);
		}
	}
}

document.addEventListener("DOMContentLoaded", function(event) {
	access_see_more_btn();
	window.addEventListener('resize',function(event) {
		access_see_more_btn();
	},true);
});
