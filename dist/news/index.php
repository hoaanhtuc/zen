<?php
$thispage = 'news';
$path = realpath(dirname(__FILE__) . '/../');
include_once('app_config.php');
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="news" class='news-archive'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<div class="banner">
				<div class="banner-wrap">
					<h1><?php __elang(array(
						LANG_VN => '',
						LANG_EN => 'news'
					));?></h1>
				</div>
			</div>
			<div class="main-content clearfix">
				<div class="descriptions">
					<div class="container-1000">
						<p><?php __elang(array(
							LANG_EN => 'ZEN Saigon is a Japanese restaurant opened in 2001, located in the heart of Ho Chi Minh City, Le Thanh Ton street. We have been loved by many of customer for nearly 20 years so far. We keep going to be a profestional Japanese cuisine caterer with assurance safety fresh seafood and meat imported directly from Japan. We hope you can enjoy the Japanese tast joyfully in Ho Chi Minh City with our service.',
							LANG_VN => ''));?></p>
					</div>
				</div>
				<?php slider_demo();?>
				<div class="box-menu">
					<div class="container-1300">
						<div class="texts">
							<h2 class="box-title"><?php __elang(array(
								LANG_VN => 'tay1',
								LANG_EN => 'menu'
								))?></h2>
							<p><?php __elang(array(
								LANG_VN => '',
								LANG_EN => 'Main dishes of Zen Saigon, it is grilled chicken Yakitori and Sushi which are made with fresh ingredients. We are ready to serve you about 60 kinds of dishes and kinds of drinks. Moreover, at Zen Saigon, you can also enjoy the original taste of our Japanese sake, named "ICHIZEN".'
								))?></p>
						</div>
						<div class="imgs">
							<ul>
								<li><img src="<?php echo APP_ASSETS_IMG;?>top/img04.jpg" alt=""></li>
								<li><img src="<?php echo APP_ASSETS_IMG;?>top/img05.jpg" alt=""></li>
								<li><img src="<?php echo APP_ASSETS_IMG;?>top/img06.jpg" alt=""></li>
								<li><img src="<?php echo APP_ASSETS_IMG;?>top/img07.jpg" alt=""></li>
							</ul>
						</div>
						<div class="btn_detail">
							<a href="<?php echo APP_URL_MENU;?>"><?php __elang(array(
								LANG_VN => '',
								LANG_EN => 'view all menu'
								))?></a>
						</div>
					</div>
				</div>

				<div class="box-img-store">
					<img class="pc" src="<?php echo APP_ASSETS_IMG;?>/top/banner_inpage01.jpg" alt="">
					<img class="sp" src="<?php echo APP_ASSETS_IMG;?>/top/banner_inpage02.jpg" alt="">
				</div>

				<div class="box-news_campaign">
					<div class="container-1000">
						<h2 class="box-title"><?php __elang(array(
							LANG_VN => '',
							LANG_EN => 'news & campaign'
							));?></h2>
						<ul class="box-list-three">
							<li><a href="<?php echo APP_URL;?>">
								<span class="img"><img src="<?php echo APP_ASSETS_IMG;?>top/img08.jpg" alt=""></span>
								<span class="date">18.09.14</span>
								<span class="title"><?php __elang(array(
									LANG_VN => '',
									LANG_EN => 'Dummy Tittle here Lorem ipsum dolor sit amet, consectetuer ...'
									));?></span>
							</a></li>
							<li><a href="<?php echo APP_URL;?>">
								<span class="img"><img src="<?php echo APP_ASSETS_IMG;?>top/img09.jpg" alt=""></span>
								<span class="date">18.09.14</span>
								<span class="title"><?php __elang(array(
									LANG_VN => '',
									LANG_EN => 'Dummy Tittle here Lorem ipsum dolor sit amet, consectetuer ...'
									));?></span>
							</a></li>
							<li><a href="<?php echo APP_URL;?>">
								<span class="img"><img src="<?php echo APP_ASSETS_IMG;?>top/img10.jpg" alt=""></span>
								<span class="date">18.09.14</span>
								<span class="title"><?php __elang(array(
									LANG_VN => '',
									LANG_EN => 'Dummy Tittle here Lorem ipsum dolor sit amet, consectetuer ...'
									));?></span>
							</a></li>
						</ul>
						<div class="btn_detail">
							<a href="<?php echo APP_URL_MENU;?>"><?php __elang(array(
								LANG_VN => '',
								LANG_EN => 'view list'
								))?></a>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>

</body>
</html>