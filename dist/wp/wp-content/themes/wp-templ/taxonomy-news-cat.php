<?php
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="blog" class='blog tax'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
<?php
	//$paged
	$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); // get current category
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$list_news_column = query_posts($query_string . '&orderby=post_date&order=desc&posts_per_page=9&paged=' . $paged);	
?>
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h1 class="bHead"><span><i>お知らせ</i><b>NEWS</b></span></h1>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li><a href="<?php echo APP_URL_NEWS ;?>">お知らせ</a></li>
				<li><?php echo $current_term->name; ?></li>
			</ul>

			<div class="blogBlock clearfix">
				<ul class="clearfix blogList">
					<?php foreach($list_news_column as $sg){
						$img_ft = get_img_src($sg,true,'large');
						$terms = get_the_terms($sg->ID,'news-cat');
						$defaults = array(
					        'img' => $img_ft,
					        'width' => 520,
					        'height' => 400,
					        'zoom' => 1,
					        'crop' => true,
					        'quality' => 100,
					    );
						
					?>
					<li>
						<a href="<?php echo get_permalink($sg->ID);?>">
							<p class="img"><img src="<?php echo thumbCrop($defaults);?>" alt=""><?php new_icon($sg->post_date);?></p>
							<h3><span class="date"><i><?php echo date('Y.m.d',strtotime($sg->post_date));?></i><b><?php echo $terms[0]->name;?></b></span>
							<strong><?php echo my_cut_string($sg->post_title,100);?></strong></h3>
						</a>
					</li>
					<?php } ?>
					
				</ul>
				<div class="wp-pagenavi">
				<?php 
				if(function_exists('pagenavi_t')) { 
					pagenavi_t($wp_query->max_num_pages,$paged,get_term_link($current_term->term_id,'news-cat')); 
				} ?>
				</div>

				<div class="catBlock">
					<div class="inner">
					<h3>CATEGORY</h3>
				<?php
				$args = array(
					'post_type'                => 'news-column',
					'orderby'                  => 'id',
					'order'                    => 'desc',
					'hide_empty'               => 1,
					'taxonomy'                 => 'news-cat',
					'pad_counts'               => false ,
				);

				$categories = get_categories( $args );
				?>
					<ul>
					<?php 
					foreach($categories as $cate){ 
						$param_terms = array(
							'post_type'=>'news-column',
							'order' => 'DESC',
							'post_status' => 'publish',
							'posts_per_page' => -1,
							'tax_query' => array(
								array(
									'taxonomy' => 'news-cat',
									'field'    => 'term_id',
									'terms'    => $cate->term_id,
								)
							),
							'meta_query' => array(
								array(
									'key' => 'show_hide',
									'value' => 'show',
									'compare' => "LIKE"
								)
							)
						);
						$wp_query_term = new WP_Query();
						$wp_query_term->query($param_terms);
						if($wp_query_term->found_posts > 0){
					?>
						<li><a href="<?php echo get_term_link($cate->slug,'news-cat');?>"><?php echo $cate->name;?>( <?php echo $wp_query_term->found_posts;?> )</a></li>
					<?php 
						}
					} 
					?>
					</ul>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>

<script src="<?php echo APP_ASSETS; ?>js/lib/jquery.matchHeight.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/blog.min.js"></script>
<script type="text/javascript">
$(window).bind("load", function() { 
	$('.blogList li a').matchHeight();
});
</script>
</body>
</html>