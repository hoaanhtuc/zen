<?php
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="faq" class='faq'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->

	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php get_banner_page_common();?>
		<?php 
			$args = array(
				'post_type'                => 'faq',
				'order'                    => 'desc',
				'hide_empty'               => 1,
				'taxonomy'                 => 'faqcat',
				'pad_counts'               => false );
			
			$categories = get_categories( $args );
			$content = '';
		if(!empty($categories)){ ?>
			<div class="faqBlock faqAnchor">
				<ul class="anchor anchor_txt clearfix">
					<?php for($i = 0 ; $i < count($categories) ; $i++){ ?>
					<li><a href="#cate-<?php echo $categories[$i]->slug;?>"><span><b class="txt"><?php echo $categories[$i]->name;?></b></span></a></li>
					<?php ob_start();?>
					<h3 id="cate-<?php echo $categories[$i]->slug;?>" class="mHead"><span><?php echo $categories[$i]->name;?></span></h3>
					<?php
						$wp_query = new WP_Query();
						$param=array(
							'post_type'=>'faq',
							'order' => 'DESC',
							'posts_per_page' => '-1',
							'tax_query' => array(
								array(
									'taxonomy' => 'faqcat',
									'field' => 'slug',
									'terms' => $categories[$i]->slug
									)
								)
							);
						$faq_list = $wp_query->query($param);
					?>
					<div class="faqBlock">
						<ul class="listTxt listFaq">
							<?php foreach($faq_list as $faq_sg){ ?>
							<li>
								<h3 ><span><?php echo esc_html($faq_sg->post_title);?></span></h3>
								<div class="desc"><?php echo apply_filters('the_content',$faq_sg->post_content);?></div>
							</li>
							<?php } ?>
						</ul>
					</div>
					<?php 
					$content .= ob_get_contents();
					ob_get_clean();
					?>

					<?php } ?>
				</ul>

			</div>
			<?php echo $content;?>
		<?php } ?>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
<script type="text/javascript">
$(window).bind("load", function() { 
	$('.listTxt li h3').click(function(){
		$(this).next().slideToggle('fast');
		$(this).toggleClass('active');
	})
});
</script>
</body>
</html>