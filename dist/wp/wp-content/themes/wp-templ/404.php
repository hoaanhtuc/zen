<?php
// Author: A+LIVE
include(APP_PATH.'libs/head.php');
?>
</head>
<body id="page404" class='404'>
    <!-- Header
    ================================================== -->
    <?php include(APP_PATH.'/libs/header.php'); ?>
    <style>
	    html{
	        margin: 0 !important;
	    }
	    #page404 .taC{
	        display: block;
	        font-size: 20px;
	        line-height: 30px;
	        letter-spacing: 3px;
	        text-align: center;
	        margin: 20px 0 0 0;
	    }
	    #page404 .taC a{font-size: 20px;text-decoration: none; color: #00B0FF;}
	    #page404 .page-title{
	        font-size: 35px;
	        font-weight: bold;
	        letter-spacing: 5px;
	        padding-bottom: 50px;
	    }
	    .page_404{
	        padding: 200px 0 200px;
	    }
	</style>


        <main>
            <section class="page_404 ">
            	<div class="container-1000">
	                <h2 class="page-title text-center"><span>4</span>04</h2>
	                <div class='text-center'>
	                    <!-- /mainContent start -->
	                    <p class="taC ">
	                    アクセスしようとしたページは、移動したか削除されました。<br />下記リンクに移動して下さい。
	                    <br><br>
	                    <a href="<?php echo esc_url( home_url( '/' ) )?>"><?php echo esc_url( home_url( '/' ) )?></a>
	                    </p>

	                    <!-- /maincontent end -->
	                </div>
                </div>
            </section>

        </main>
    <!-- Footer
    ================================================== -->
    <?php include(APP_PATH.'/libs/footer.php'); ?>
<!-- End Document
================================================== -->
</body>
</html>
