<?php
if(@file_exists(get_template_directory() . '/inc/create_post_type.php')){
	include_once(get_template_directory() . '/inc/create_post_type.php');
}
if(@file_exists(get_template_directory() . '/inc/modify_get.php')){
	include(get_template_directory() . '/inc/modify_get.php');
}
//本体のアップデート通知を非表示
add_filter('pre_site_transient_update_core', create_function('$a', "return  null;"));
//プラグイン更新通知を非表示
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
include_once(dirname(ABSPATH)."/app_config.php");


// WordPressの管理画面ログインURLを変更する
define( 'LOGIN_CHANGE_PAGE', 'wp-login-bisai.php' );
add_action( 'login_init', 'login_change_init' );
add_filter( 'site_url', 'login_change_site_url', 10, 4 );
add_filter( 'wp_redirect', 'login_change_wp_redirect', 10, 2 );
// 指定以外のログインURLは403エラーにする
if ( ! function_exists( 'login_change_init' ) ) {
function login_change_init() {
if ( !defined( 'LOGIN_CHANGE' ) || sha1( 'bisai2018' ) != LOGIN_CHANGE ) {
status_header( 403 );
exit;
}
}
}
// ログイン済みか新設のログインURLの場合はwp-login.phpを置き換える
if ( ! function_exists( 'login_change_site_url' ) ) {
function login_change_site_url( $url, $path, $orig_scheme, $blog_id ) {
if ( $path == 'wp-login.php' &&
( is_user_logged_in() || strpos( $_SERVER['REQUEST_URI'], LOGIN_CHANGE_PAGE ) !== false ) )
$url = str_replace( 'wp-login.php', LOGIN_CHANGE_PAGE, $url );
return $url;
}
}
// ログアウト時のリダイレクト先の設定
if ( ! function_exists( 'login_change_wp_redirect' ) ) {
function login_change_wp_redirect( $location, $status ) {
if ( strpos( $_SERVER['REQUEST_URI'], LOGIN_CHANGE_PAGE ) !== false )
$location = str_replace( 'wp-login.php', LOGIN_CHANGE_PAGE, $location );
return $location;
}
}


//login logo
function custom_login_logo() {
	echo '<style type="text/css">h1 a { background: url('.APP_ASSETS_IMG.'/common/header/logo.svg) 50% 50% no-repeat !important; width:100% !important;}</style>';
}

add_action('login_head', 'custom_login_logo');

// link for logo
function new_wp_login_url() {
	return home_url();
}
add_filter('login_headerurl', 'new_wp_login_url');

// title for logo
function new_wp_login_title() {
	return get_option('blogname');
}
add_filter('login_headertitle', 'new_wp_login_title');

// Theme support
add_theme_support( 'post-thumbnails' );

function get_img_src($postthis,$noimg = true,$size='full',$return_object = false){
  if($fbimage = wp_get_attachment_image_src( get_post_thumbnail_id($postthis->ID),$size)){
    if(true === $return_object){
      return $fbimage;
    }else{
      return $fbimage[0];
    }
    
  }
  if(empty($postthis->post_content)){
    return;
  }
  preg_match('/<img.*.+src=[\'"]([^\'"]+)[\'"].*.+width=[\'"]([^\'"]+)[\'"].*.+height=[\'"]([^\'"]+)[\'"].*>/i',$postthis->post_content, $matches);
  if(true === $return_object){
    if($matches[1]){
      $fbimage[0] = $matches[1];
      $fbimage[1] = $matches[2];
      $fbimage[2] = $matches[3];
    }else{
       $fbimage[0] = get_home_url().'/assets/img/common/other/img_nophoto.jpg';
        $fbimage[1] = 700;
        $fbimage[2] = 300;
    }
    
  }else{
    $fbimage = $matches[1];
  }
  
  if(empty($fbimage)) {
    if(true === $noimg){
      if(true === $return_object){
        $fbimage[0] = get_home_url().'/assets/img/common/other/img_nophoto.jpg';
        $fbimage[1] = 700;
        $fbimage[2] = 300;
      }else{
        $fbimage = get_home_url().'/assets/img/common/other/img_nophoto.jpg';
      }
    }else{
      $fbimage = '';
    }
  }
  return $fbimage;
}
//timthumb
define('THEME_DIR', get_template_directory_uri());
/* Timthumb CropCropimg */
function thumbCrop($args = array() ){
	/*if($h) $h = "&amp;h=$h";
	else $h = "";
	if($w) $w = "&amp;w=$w";
	else $w = "";
	if($a) $a = "&amp;a=$a";
	else $a = "";
	if($cc) $cc = "&amp;cc=$cc";   
	else $cc = "";	

	$img = str_replace(get_bloginfo('url'), '', $img);
	$image_url = THEME_DIR . "libs/timthumb/timthumb.php?src=" . $img . $h . $w. "&amp;zc=".$zc .$a .$cc;	
	return $image_url;*/
	$defaults = array(
        'img' => '',
        'width' => get_option( "medium_size_w" ),
        'height' => get_option( "medium_size_h" ),
        'zoom' => 1,
        'crop' => true,
        'quality' => 100,
    );

    $args  = wp_parse_args( (array) $args, $defaults );
   // $img = str_replace( get_bloginfo( 'url' ), '', $args['img'] );
	require_once( get_template_directory() . '/inc/bfi_thumb.php' );
	// @define( BFITHUMB_UPLOAD_DIR, 'other_dir' );
	$image_url = bfi_thumb( $args['img'], $args );
	return $image_url;
}

// paging
function my_option_posts_per_page() {
  return 0;
}
function my_modify_posts_per_page() {
    add_filter( 'option_posts_per_page', 'my_option_posts_per_page' );
}
add_action( 'init', 'my_modify_posts_per_page', 0);

function wp_post_type_archive($post_type = "post", $home_url="", $havecount = false){
	global $wpdb;
	if($home_url == "") $home_url  = home_url("/");
	$html = '';
	$txtCount = "";
	$posttype = get_post_type_object($post_type);
	$slug = $posttype->rewrite['slug'];
	$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date)
		FROM $wpdb->posts WHERE post_status = 'publish'
		AND post_type = '{$post_type}' ORDER BY post_date DESC");

	foreach($years as $year) :
	if($havecount) {
		$count = $wpdb->get_col("SELECT COUNT(*) countpost
			FROM $wpdb->posts WHERE post_status = 'publish'
			AND post_type = '{$post_type}' and YEAR(post_date) = '".$year."'");
		$txtCount = '('.$count[0].')';
	}
	$html .= '<li id="year'.$year.'"><a href="javascript:void(0);" class="dropdown">'.$year.'年 '.$txtCount.'</a><ul class="sub">';

	$months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date)
		FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '{$post_type}'
		AND YEAR(post_date) = '".$year."' ORDER BY post_date DESC");

	foreach($months as $month) :
		if($havecount) {
			$count = $wpdb->get_col("SELECT COUNT(*) countpost
				FROM $wpdb->posts WHERE post_status = 'publish'
				AND post_type = '{$post_type}' and YEAR(post_date) = '".$year."' and MONTH(post_date) = '".$month."'");
			$txtCount = '('.$count[0].')';
		}
		$html .= '<li><a href="'.$home_url.$slug."/".$year.'/'.$month.'">'.$month.'月 '.$txtCount.'</a></li>';
	endforeach;
	$html .= '</ul></li>';
	endforeach;
	return $html;
}
// for rewrite - this is alway at bottom of page
add_filter('post_type_link', 'custom_blog_permalink', 1, 3);
 function custom_blog_permalink($post_link, $id = 0, $leavename) {
	if ( strpos('%post_id%', $post_link) === 'FALSE' ) {
		return $post_link;
	}
	$post = get_post($id);
	if ( is_wp_error($post)) {
		return $post_link;
	}
	$post_type = get_post_type_object($post->post_type);
	return home_url($post_type->rewrite['slug'].'/p'.$post->ID.'/');
 }
function add_rewrites_init(){
	global $wp_rewrite;
	$postoj =  get_post_types( '', 'object' );
	foreach ( $postoj as $key=> $ar ) {
		$posttype = $ar->name;
		$slug = $ar->rewrite['slug'];
		$sgc = get_template_directory() . "/single-" . $posttype . ".php";
		$agr = get_template_directory() . "/archive-" . $posttype . ".php";
		if(@file_exists($sgc)){
			add_rewrite_rule($slug.'/p([0-9]+)?$', 'index.php?post_type='.$posttype.'&p=$matches[1]', 'top');
		}
		if(@file_exists($agr)){
			add_rewrite_rule($slug.'/([0-9]{4})/([0-9]{1,2})/?$', 'index.php?post_type='.$posttype.'&year=$matches[1]&monthnum=$matches[2]', 'top');
			add_rewrite_rule($slug.'/([0-9]{4})/([0-9]{1,2})/page/([0-9]{1,})/?$', 'index.php?post_type='.$posttype.'&year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]', 'top');
		}
	}
	$wp_rewrite->flush_rules(false);
}
add_action('init', 'add_rewrites_init');
//end for rewrite - this is alway at bottom of page