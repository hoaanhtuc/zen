<?php
$thispage = 'top';
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="top" class='top'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<div class="banner">
				<h1><a href="<?php echo APP_URL;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/header/logo.svg" width="255" alt="<?php echo ALT_SITE;?>"></a></h1>
				<h2><img src="<?php echo APP_ASSETS_IMG;?>top/txt_01.svg" width="650" height="139" alt=""></h2>
				<div class="pc">
					<div class="banner-content">
						<div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img00.jpg)no-repeat fixed center center; background-size:cover;"></div>
						<!-- <div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img01.jpg)no-repeat fixed center center; background-size:cover;"></div>
						<div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img02.jpg)no-repeat fixed center center; background-size:cover;"></div>
						<div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img03.jpg)no-repeat fixed center center; background-size:cover;"></div> -->
					</div>
				</div>
				<div class="sp">
					<div class="banner-content">
						<div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img00_sp@2x.jpg)no-repeat scroll center center; background-size:cover;"></div>
						<!-- <div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img00_sp@2x.jpg)no-repeat fixed center center; background-size:cover;"></div>
						<div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img00_sp@2x.jpg)no-repeat fixed center center; background-size:cover;"></div>
						<div class="imgs" style="background:url(<?php echo APP_ASSETS_IMG;?>top/img00_sp@2x.jpg)no-repeat fixed center center; background-size:cover;"></div> -->
					</div>
				</div>
			</div>
			<div class="main-content clearfix">
				<div class="top_bar pc">
					<?php get_sidebar_mdf('top','pc');?>
				</div>
				<div class="slider_top_01">
					<div class="container-1200">
						<div class="sliders">
							<div><a href="<?php echo APP_URL_PRICE;?>#anchor01"><img src="<?php echo APP_ASSETS_IMG;?>top/slider_01.jpg" alt=""></a></div>
							<div><a href="<?php echo APP_URL_TEACHING;?>#anc01"><img src="<?php echo APP_ASSETS_IMG;?>top/slider_02.jpg" alt=""></a></div>
							<div><a href="<?php echo APP_URL_ABOUT;?>#anc03a"><img src="<?php echo APP_ASSETS_IMG;?>top/slider_03.jpg" alt=""></a></div>
							<!-- <div><a href="javascript:void(0);"><img src="<?php echo APP_ASSETS_IMG;?>top/slider_04.jpg" alt=""></a></div> -->
						</div>
					</div>
				</div>

				<div class="driving-school">
					<div class="driving-school-first">
						<img class="pc" src="<?php echo APP_ASSETS_IMG;?>top/driving_school.svg" alt="BISAI DRIVING SCHOOL" width="578" height="309" />
						<img class="sp" src="<?php echo APP_ASSETS_IMG;?>top/driving_school_sp.svg" alt="BISAI DRIVING SCHOOL" width="364" height="" />
					</div>
					<h3>一宮市で自動車免許を取るなら<br class="sp">尾西自動車学校で決まり！</h3>
					<p>仮のテキストです彼らも十月けっしてその出立目というののために受けるでです。よく今が意味順はどうしてもその呈ですませまでを要らとしまえでのはお話なさましだって、ますますには思いたらですませた。</p>
					<div class="btn_inside">
						<a href="<?php echo APP_URL_REASON;?>">尾西自動車学校が選ばれる理由</a>
					</div>
				</div>
<?php
	$args_news = array(
		'post_type' => 'news-column',
		'post_status' => 'publish',
		'order' => 'desc',
		'orderby' => 'post_date',
		'posts_per_page' => 3,
		'paged' => 1,
		'meta_query' => array(
			array(
				'key' => 'show_hide',
				'value' => 'show',
				'compare' => 'LIKE'
			)
		)
	);
	$wp_news = new WP_Query();
	if($post_news = $wp_news->query($args_news)){
?>
				<div class="news-box">
					<div class="container-1000">
						<div class="title-inbox">
							<h2 class="title-inbox-bigen">NEWS</h2>
							<div class="btn_archive">
								<a href="<?php echo APP_URL_NEWS;?>"><span>ニュース一覧</span></a>
							</div>
						</div>
						<ul>
						<?php foreach($post_news as $pn){ 
							$terms_pn = get_the_terms($pn->ID,'news-cat');
						?>
							<li>
								<label><?php echo date('Y.m.d',strtotime($pn->post_date));?></label>
								<?php if($terms_pn){ ?>
								<span><b style="color:<?php echo get_field('color','news-cat_'.$terms_pn[0]->term_id);?>" href="javascript:void(0)"><?php echo $terms_pn[0]->name;?></b></span>
								<?php }else{ ?>
								<span><a href="<?php echo APP_URL_NEWS;?>">お知らせ</a></span>
								<?php } ?>
								<p><a href="<?php echo get_permalink($pn->ID);?>"><?php echo $pn->post_title;?></a></p>
							</li>
						<?php } ?>
						</ul>
					</div>
				</div>
<?php } ?>
				<div class="guide-box">
					<div class="container-1000">
						<div class="title-inbox">
							<p class="title-inbox-sub">教習案内</p>
							<h2 class="title-inbox-bigen">TEACHING GUIDE</h2>
						</div>
						<div class="guide-box-links">
							<ul>
								<li><a href="<?php echo APP_URL_STANDARD;?>"><img src="<?php echo APP_ASSETS_IMG;?>top/img01.jpg" alt="普通車"><span>普通車</span></a></li>
								<li><a href="<?php echo APP_URL_MOTOBIKE;?>"><img src="<?php echo APP_ASSETS_IMG;?>top/img02.jpg" alt="二輪車"><span>二輪車</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="course-box">
					<div class="container-1000">
						<div class="title-inbox">
							<p class="title-inbox-sub">各種教習</p>
							<h2 class="title-inbox-bigen">SHORTCOURSE</h2>
						</div>
						<div class="course-box-links">
							<ul>
								<li>
									<a href="<?php echo APP_URL_TEACHING;?>#anc01">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_3.svg" width="63" height="32" alt="企業様向け研修コース"></i>
										<span>企業様向け<br>研修コース</span>
									</a>
								</li>
								<li>
									<a href="<?php echo APP_URL_TEACHING;?>#anc02">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_4.svg" width="66" height="39" alt="学校様向け教育コース"></i>
										<span>学校様向け<br>教育コース</span>
									</a>
								</li>
								<li>
									<a href="<?php echo APP_URL_TEACHING;?>#anc03">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_5.svg" width="53" height="58" alt="高齢者講習"></i>
										<span>高齢者講習</span>
									</a>
								</li>
								<li>
									<a href="<?php echo APP_URL_TEACHING;?>#anc04">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_6.svg" width="36" height="52" alt="ペーパードライバー講習"></i>
										<span>ペーパー<br>ドライバー講習</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="price-about-box">
					<div class="container-1000">
						<ul>
							<li><a href="<?php echo APP_URL_PRICE;?>"><img src="<?php echo APP_ASSETS_IMG;?>top/img03@2x.jpg" alt="PRICE"></a></li>
							<li><a href="<?php echo APP_URL_ABOUT;?>"><img src="<?php echo APP_ASSETS_IMG;?>top/img04@2x.jpg" alt="ABOUT"></a></li>
						</ul>
					</div>
				</div>
<?php
	$args_blog = array(
		'post_type' => 'news-column',
		'post_status' => 'publish',
		'order' => 'desc',
		'orderby' => 'post_date',
		'posts_per_page' => 3,
		'paged' => 1,
		'meta_query' => array(
			array(
				'key' => 'show_hide',
				'value' => 'show',
				'compare' => 'NOT LIKE'
			)
		)
	);
	$wp_blog = new WP_Query();
	if($post_blog = $wp_blog->query($args_blog)){
?>
				<div class="blog-box">
					<div class="container-1000">
						<div class="title-inbox">
							<h2 class="title-inbox-bigen">BLOG</h2>
							<div class="btn_archive">
								<a href="<?php echo APP_URL_BLOG;?>"><span>ブログ一覧</span></a>
							</div>
						</div>
						<ul>
						<?php 
						foreach($post_blog as $blog){
							$img_ft = get_img_src($blog,true,'large');
							$blogcate = get_the_terms($blog->ID,'blog-cat');
							$defaults = array(
						        'img' => $img_ft,
						        'width' => 634,
						        'height' => 460,
						        'zoom' => 1,
						        'crop' => true,
						        'quality' => 100,
						    );
						?>
							<li>
								<div class="wrap_col">
									<?php new_icon($blog->post_date);?>
									<p class="imgs">
										<img src="<?php echo APP_ASSETS_IMG;?>top/img05.jpg" alt="">
									</p>
									<p class="date-category">
										<span class="date"><?php echo date('Y.m.d',strtotime($blog->post_date));?></span>
										<?php if($blogcate){ ?>
										<span class="category"><a style="color:<?php echo get_field('color','blog-cat_'.$blogcate->term_id);?>" href="<?php echo get_term_link($blogcate[0]->term_id,'blog-cat');?>"><?php echo $blogcate[0]->name;?></a></span>
										<?php } ?>
									</p>
									<h3 class="blog-col-title"><?php echo my_cut_string($blog->post_title,40);?></h3>
									<a href="<?php echo get_permalink($blog->ID);?>"></a>
								</div>
							</li>
						<?php } ?>
							
						</ul>
					</div>
				</div>
<?php } ?>
<?php
$args_faq = array(
	'post_type' => 'faq',
	'post_status' => 'publish',
	'order' => 'desc',
	'orderby' => 'post_date',
	'posts_per_page' => 4,
	'paged' => 1,
	'meta_query' => array(
		array(
			'key' => 'show_top',
			'value' => true,
			'compare' => 'LIKE'
		)
	)
);
$wp_query_faq = new WP_Query();
$list_faq = $wp_query_faq->query($args_faq);
if($list_faq){
?>
				<div class="faq-box">
					<div class="container-1000">
						<div class="title-inbox">
							<p class="title-inbox-sub">よくあるご質問</p>
							<h2 class="title-inbox-bigen">FAQ</h2>
						</div>
						<ul>
						<?php foreach($list_faq as $faq){ ?>
							<li>
								<h3 class="quest"><span><?php echo $faq->post_title;?></span></h3>
								<div class="answer"><?php echo $faq->post_content;?></div>
							</li>
						<?php } ?>
						</ul>
						<div class="btn_inside_border">
							<a href="<?php echo APP_URL_FAQ;?>">よくあるご質問一覧</a>
						</div>
					</div>
				</div>
<?php } ?>
				<div class="description-box">
					<div class="description-box-wrap">
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>top/img06.jpg" alt="キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります">
							<div class="comtent"><div class="text bgpink"><div class="text-content">キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります</div></div></div>
						</div>
						<div class="right">
							<p>ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。</p>
							<p>ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入ります、</p>
							<p>ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。</p>
						</div>
					</div>
					<div class="description-box-wrap">
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>top/img06_b.jpg" alt="キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります">
							<div class="comtent"><div class="text bgblue"><div class="text-content">キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります</div></div></div>
						</div>
						<div class="right">
							<p>ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。</p>
							<p>ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入ります、</p>
							<p>ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。</p>
						</div>
					</div>
					<div class="description-box-wrap">
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>top/img06_c.jpg" alt="キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります">
							<div class="comtent"><div class="text bggreen"><div class="text-content">キャッチコピーが入りますキャッチコピーが入りますキャッチコピーが入ります</div></div></div>
						</div>
						<div class="right">
							<p>ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。</p>
							<p>ここにSEO対策用の文章が入ります、ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。ここにSEO対策用の文章が入ります、</p>
							<p>ここにSEO対策用の文章が入りますここにSEO対策用の文章が入ります。</p>
						</div>
					</div>
				</div>

				<div class="line-box">
					<div class="line_btn">
						<a href="javascript:void(0)" target="_blank">
							<i><img src="<?php echo APP_ASSETS_IMG;?>top/line@.svg" width="190" height="430" alt="LINE@"></i>
							<span>最新情報配信中！<br>お友達登録はコチラから</span>
						</a>
					</div>
				</div>
<?php
	$pdf = get_field('upload_pdf',59);
?>
				<div class="study-box">
					<div class="container-1100">
						<h2>在校生向けコンテンツ</h2>
						<ul>
							<li><a href="<?php echo APP_URL_CLASSES;?>#anc02"><img src="<?php echo APP_ASSETS_IMG;?>top/img07.png" alt=""></a></li>
							<li><a href="<?php echo $pdf['url'];?>" target="_blank"><img src="<?php echo APP_ASSETS_IMG;?>top/img08.png" alt=""></a></li>
							<li><a href="http://www.be-sai.com/yoyaku/" target="_blank"><img src="<?php echo APP_ASSETS_IMG;?>top/img09.png" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
	<script src="<?php echo APP_ASSETS_JS;?>lib/slick.min.js"></script>
	<script src="<?php echo APP_ASSETS_JS;?>lib/biggerlink.js"></script>
	<script src="<?php echo APP_ASSETS_JS;?>top.min.js"></script>
</body>
</html>