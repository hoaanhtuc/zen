<?php
$tax = get_query_var('catet');
$args = array(
	'post_type'                => 'news-column',
	'orderby'                  => 'id',
	'order'                    => 'desc',
	'hide_empty'               => 1,
	'taxonomy'                 => $tax,
	'pad_counts'               => false );

$categories = get_categories( $args );
$arcate = array();
foreach($categories as $catesingle){
	$arcate[] = intval($catesingle->term_id);
}
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="blog" class='blog'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
<?php
	//$paged
	$paged = get_query_var( 'paged') > 0 ? get_query_var( 'paged') : 1;
	$param = array(
		'post_type'=>'news-column',
		'order' => 'DESC',
		'post_status' => 'publish',
		'posts_per_page' => '9',
		'paged' => $paged,
		'tax_query' => array(
			array(
				'taxonomy' => $tax,
				'field' => 'term_id',
				'terms' => $arcate
			)
		)
	);
	$wp_query = new WP_Query();
	$list_news_column = $wp_query->query($param);
?>
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
		<?php if($tax == 'news-cat'){ ?>
			<h1 class="bHead"><span><i>お知らせ</i><b>NEWS</b></span></h1>
		<?php }else{ ?>
			<h1 class="bHead"><span><i>ブログ</i><b>BLOG</b></span></h1>
		<?php } ?>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
			<?php if($tax == 'news-cat'){ ?>
				<li>お知らせ</li>
			<?php }else{ ?>
				<li>ブログ</li>
			<?php } ?>
			</ul>

			<div class="blogBlock clearfix">
				<ul class="clearfix blogList">
					<?php foreach($list_news_column as $sg){
						$img_ft = get_img_src($sg,true,'large');
						$terms = get_the_terms($sg->ID,$tax);
						$defaults = array(
					        'img' => $img_ft,
					        'width' => 520,
					        'height' => 400,
					        'zoom' => 1,
					        'crop' => true,
					        'quality' => 100,
					    );
						
					?>
					<li>
						<a href="<?php echo get_permalink($sg->ID);?>">
							<p class="img"><img src="<?php echo thumbCrop($defaults);?>" alt=""><?php new_icon($sg->post_date);?></p>
							<h3><span class="date"><i><?php echo date('Y.m.d',strtotime($sg->post_date));?></i>
								<?php foreach($terms as $term){ ?>
								<b><?php echo $term->name;?></b>
								<?php } ?>
							</span>
							<strong><?php echo my_cut_string($sg->post_title,100);?></strong></h3>
						</a>
					</li>
					<?php } ?>
					
				</ul>
				<div class="wp-pagenavi">
				<?php 
				if(function_exists('pagenavi_t')) { 
					pagenavi_t($wp_query->max_num_pages,$paged,$tax == 'blog-cat' ? APP_URL_BLOG : APP_URL_NEWS); 
				} ?>
				</div>

				<div class="catBlock">
					<div class="inner">
					<h3>CATEGORY</h3>
				
					<ul>
					<?php 
					foreach($categories as $cate){ 
						$param_terms = array(
							'post_type'=>'news-column',
							'order' => 'DESC',
							'post_status' => 'publish',
							'posts_per_page' => -1,
							'tax_query' => array(
								array(
									'taxonomy' => $tax,
									'field'    => 'term_id',
									'terms'    => $cate->term_id,
								)
							),
						);
						$wp_query_term = new WP_Query();
						$wp_query_term->query($param_terms);
						if($wp_query_term->found_posts > 0){
					?>
						<li><a href="<?php echo get_term_link($cate->slug,$tax);?>"><?php echo $cate->name;?>( <?php echo $wp_query_term->found_posts;?> )</a></li>
					<?php 
						}
					} 
					?>
					</ul>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>

<script src="<?php echo APP_ASSETS; ?>js/lib/jquery.matchHeight.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/blog.min.js"></script>
<script type="text/javascript">
$(window).bind("load", function() { 
	$('.blogList li a').matchHeight();
});
</script>
</body>
</html>