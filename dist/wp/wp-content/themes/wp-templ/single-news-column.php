<?php
// Author: A+LIVE
global $wpdp,$post;
$show_hide = get_field('show_hide',$post->ID)[0];
if($show_hide == 'show'){
	$tax = 'news-cat';
}else{
	$tax = 'blog-cat';
}
$args = array(
	'post_type'                => 'news-column',
	'orderby'                  => 'id',
	'order'                    => 'desc',
	'hide_empty'               => 1,
	'taxonomy'                 => $tax,
	'pad_counts'               => false );

$categories = get_categories( $args );
$arcate = array();
foreach($categories as $catesingle){
	$arcate[] = $catesingle->term_id;
}
$terms = get_the_terms($post->ID,$tax);
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="blog" class='blog'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php if($show_hide == 'show'){ ?>
			<h1 class="bHead"><span><i>お知らせ</i><b>NEWS</b></span></h1>
			<?php }else{ ?>
			<h1 class="bHead"><span><i>ブログ</i><b>BLOG</b></span></h1>
			<?php } ?>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<?php if($show_hide == 'show'){ ?>
				<li><a href="<?php echo APP_URL_NEWS; ?>">お知らせ</a></li>
				<?php }else{ ?>
				<li><a href="<?php echo APP_URL_BLOG; ?>">ブログ</a></li>
				<?php } ?>
				<li><?php echo $post->post_title;?></li>
			</ul>

			<div class="blogBlock clearfix">
				<div class="primary">
					<h3 class="mHead"><i><?php echo date('Y.m.d',strtotime($post->post_date));?></i>
						<?php 
						if($terms){
						foreach($terms as $term){ ?>
						<em><?php echo $term->name;?></em>
						<?php } } ?>
						<span><?php echo $post->post_title;?></span></h3>
					<?php 
						if($meta_img = wp_get_attachment_metadata( get_post_thumbnail_id($post->ID))){
					?>
					<p class="mainImg"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID),'full');?>" alt="<?php echo $meta_img['image_meta']['caption'] ? $meta_img['image_meta']['caption'] : $post->post_title;?>"></p>
					<?php } ?>
					<div class="cmsContent">
						<?php echo apply_filters('the_content',$post->post_content);?>
						<ul class="snsSub">
							<li>SHARE</li>
							<li class="tw_btn"><a href="https://twitter.com/share?url=<?php echo get_permalink($post->ID);?>&amp;text=<?php echo urlencode($post->post_title);?>" class="share" data-network="twitter"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_tw.svg" alt="twitter" class="opa" style="opacity: 1;"></a></li>
							<li class="line_btn"><a class="share" data-text="" data-network="line" href="https://timeline.line.me/social-plugin/share?url=<?php echo get_permalink($post->ID);?>">
								<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_line.svg" alt="facebook" class="opa"></a></li>
						</ul>
					</div>
<?php
	$prevp = mod_get_adjacent_post($post,'prev',array('news-column'),$tax);
	$nextp = mod_get_adjacent_post($post,'next',array('news-column'),$tax);
?>
					<ul class="pagerLink">
						<li class="prev">
							<?php if($prevp){ 
							    $img_prevp = get_img_src($prevp,true,'large');
								$defaults_prevp = array(
							        'img' => $img_prevp,
							        'width' => 520,
							        'height' => 400,
							        'zoom' => 1,
							        'crop' => true,
							        'quality' => 100,
							    );
							?>
							<a href="<?php echo get_permalink($prevp->ID);?>">
								<span class="img"><img src="<?php echo thumbCrop($defaults_prevp);?>" alt="<?php echo $prevp->post_title;?>"></span>
								<strong><?php echo my_cut_string($prevp->post_title,50);?></strong>
							</a>
							<?php } ?>
						</li>
						<?php if($show_hide == 'show'){ ?>
						<li class="back"><a href="<?php echo APP_URL_NEWS;;?>">一覧へ戻る</a></li>
						<?php }else{ ?>
						<li class="back"><a href="<?php echo APP_URL_BLOG;?>">一覧へ戻る</a></li>
						<?php } ?>
						<li class="next">
							<?php if($nextp){ 
								$img_nextp = get_img_src($nextp,true,'large');
								$defaults_nextp = array(
							        'img' => $img_nextp,
							        'width' => 520,
							        'height' => 400,
							        'zoom' => 1,
							        'crop' => true,
							        'quality' => 100,
							    );
							?>
							<a href="<?php echo get_permalink($nextp->ID);?>">
								<span class="img"><img src="<?php echo thumbCrop($defaults_nextp);?>" alt="<?php echo $nextp->post_title;?>"></span>
								<strong><?php echo my_cut_string($nextp->post_title,50);?></strong>
							</a>
							<?php } ?>
						</li>
					</ul>
				</div>

				<div class="secondary">
					<div class="sBox">
						<h3>CATEGORY</h3>
						<ul class="catLink">
						<?php 
						foreach($categories as $cate){ 
						?>
							<li><a href="<?php echo get_term_link($cate->slug,$tax);?>"><?php echo $cate->name;?>( <?php echo $cate->count;?> )</a></li>
						<?php } ?>
						</ul>
					</div>
<?php
	$args_np = array(
		'post_type' => 'news-column',
		'posts_per_page' => 3,
		'paged' => 1,
		'post_status' => 'publish',
		'post__not_in' => array($post->ID),
		'tax_query' => array(
			array(
				'taxonomy' => $tax,
				'field' => 'term_id',
				'terms' => $arcate
			)
		),
	);
	$wp_query = new WP_Query();
	$list_np = $wp_query->query($args_np);
	if($list_np){
?>
					<div class="sBox pc">
						<h3>NEW POSTS</h3>
						<ul class="blogList">
						<?php foreach($list_np as $np){ 
							$img_ft = get_img_src($np,true,'large');
							$termnp = get_the_terms($np->ID,$tax);
							$defaults = array(
						        'img' => $img_ft,
						        'width' => 520,
						        'height' => 400,
						        'zoom' => 1,
						        'crop' => true,
						        'quality' => 100,
						    );
						?>
							<li>
								<a href="<?php echo get_permalink($np->ID);?>">
									<p class="img"><img src="<?php echo thumbCrop($defaults);?>" alt=""><?php new_icon($np->post_date);?></p>
									<h3><span class="date"><i><?php echo date('Y.m.d',strtotime($np->post_date));?></i><b><?php echo $termnp[0]->name;?></b></span>
									<strong><?php echo my_cut_string($np->post_title,50);?></strong></h3>
								</a>
							</li>
						<?php } ?>
						</ul>
					</div>
<?php } ?>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
	<script src="<?php echo APP_ASSETS_JS;?>blog.min.js"></script>
</body>
</html>