<?php
// Custom post
// FAQ
add_action('init', 'my_custom_faq');
function my_custom_faq() {
	$labels = array(
		'name' => _x('faq', 'post type general name'),
		'singular_name' => _x('faq', 'post type singular name'),
		'add_new' => _x('新しくfaqを書く', 'news'),
		'add_new_item' => __('faq記事を書く'),
		'edit_item' => __('faq記事を編集'),
		'new_item' => __('新しいfaq記事'),
		'view_item' => __('faq記事を見る'),
		'search_sample' => __('faq記事を探す'),
		'not_found' =>  __('faq記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にfaq記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor',),
		'has_archive' => true
	);
	register_post_type('faq',$args);
}

// make taxonomy
add_action ('init','create_cat_taxonomy_faq','0');
function create_cat_taxonomy_faq () {
	$taxonomylabels = array(
		'name' => _x('faqcat','post type general name'),
		'singular_name' => _x('faqcat','post type singular name'),
		'search_items' => __('faqcat'),
		'all_items' => __('faqcat'),
		'parent_item' => __( 'Parent Cat' ),
		'parent_item_colon' => __( 'Parent Cat:' ),
		'edit_item' => __('Cat記事を編集'),
		'add_new_item' => __('Cat記事を書く'),
		'menu_name' => __( 'カテゴリー' ),
	);
	$args = array(
		'labels' => $taxonomylabels,
		'hierarchical' => true,
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'faqcat' )
	);
	register_taxonomy('faqcat','faq',$args);
}

// NEWS_COLUMN
add_action('init', 'my_custom_news_column');
function my_custom_news_column() {
	$labels = array(
		'name' => _x('ブログ', 'post type general name'),
		'singular_name' => _x('ブログ', 'post type singular name'),
		'add_new' => _x('新しくブログを書く', 'news'),
		'add_new_item' => __('ブログ記事を書く'),
		'edit_item' => __('ブログ記事を編集'),
		'new_item' => __('新しいブログ記事'),
		'view_item' => __('ブログ記事を見る'),
		'search_sample' => __('ブログ記事を探す'),
		'not_found' =>  __('ブログ記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にブログ記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail'),
		'has_archive' => true
	);
	register_post_type('news-column',$args);
}

// make taxonomy
add_action ('init','create_cat_taxonomy_news_column','0');
function create_cat_taxonomy_news_column () {
	$taxonomylabels = array(
		'name' => _x('コラム','post type general name'),
		'singular_name' => _x('ブログカテゴリ','post type singular name'),
		'search_items' => __('ブログカテゴリ'),
		'all_items' => __('ブログカテゴリ'),
		'parent_item' => __( 'Parent Cat' ),
		'parent_item_colon' => __( 'Parent Cat:' ),
		'edit_item' => __('Cat記事を編集'),
		'add_new_item' => __('Cat記事を書く'),
		'menu_name' => __( 'カテゴリー' ),
	);
	$args = array(
		'labels' => $taxonomylabels,
		'hierarchical' => true,
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'blog-cat' )
	);
	register_taxonomy('blog-cat','news-column',$args);
}
// make taxonomy
add_action ('init','create_cat_taxonomy_news_column2','0');
function create_cat_taxonomy_news_column2 () {
	$taxonomylabels = array(
		'name' => _x('お知らせ','post type general name'),
		'singular_name' => _x('ニュースカテゴリ','post type singular name'),
		'search_items' => __('ニュースカテゴリ'),
		'all_items' => __('ニュースカテゴリ'),
		'parent_item' => __( 'Parent Cat' ),
		'parent_item_colon' => __( 'Parent Cat:' ),
		'edit_item' => __('Cat記事を編集'),
		'add_new_item' => __('Cat記事を書く'),
		'menu_name' => __( 'お知らせ' ),
	);
	$args = array(
		'labels' => $taxonomylabels,
		'hierarchical' => true,
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'news-cat' )
	);
	register_taxonomy('news-cat','news-column',$args);
}

// STAFF
add_action('init', 'my_custom_staff');
function my_custom_staff() {
	$labels = array(
		'name' => _x('スタッフ', 'post type general name'),
		'singular_name' => _x('スタッフ', 'post type singular name'),
		'add_new' => _x('新しくスタッフを書く', 'news'),
		'add_new_item' => __('スタッフ記事を書く'),
		'edit_item' => __('スタッフ記事を編集'),
		'new_item' => __('新しいスタッフ記事'),
		'view_item' => __('スタッフ記事を見る'),
		'search_sample' => __('スタッフ記事を探す'),
		'not_found' =>  __('スタッフ記事はありません'),
		'not_found_in_trash' => __('ゴミ箱にスタッフ記事はありません'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail'),
		'has_archive' => true
	);
	register_post_type('staff',$args);
}

// make taxonomy
add_action ('init','create_cat_taxonomy_staff','0');
function create_cat_taxonomy_staff () {
	$taxonomylabels = array(
		'name' => _x('staffcat','post type general name'),
		'singular_name' => _x('staffcat','post type singular name'),
		'search_items' => __('staffcat'),
		'all_items' => __('staffcat'),
		'parent_item' => __( 'Parent Cat' ),
		'parent_item_colon' => __( 'Parent Cat:' ),
		'edit_item' => __('Cat記事を編集'),
		'add_new_item' => __('Cat記事を書く'),
		'menu_name' => __( 'カテゴリー' ),
	);
	$args = array(
		'labels' => $taxonomylabels,
		'hierarchical' => true,
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'staffcat' )
	);
	register_taxonomy('staffcat','staff',$args);
}