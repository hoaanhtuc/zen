<?php 
function add_query_vars($vars) {
    $vars[]     = "catet";
    return $vars;
}
add_filter('query_vars', 'add_query_vars');
add_filter('rewrite_rules_array', 'add_rewrite_rules');
function add_rewrite_rules($rules) {
    $NewRules = array(
    	'news/?$'            =>    'index.php?post_type=news-column&catet=news-cat',
    	'news/page/([0-9]{1,})/?$'            =>    'index.php?post_type=news-column&catet=news-cat&paged=$matches[1]',
    	'blog/?$'            =>    'index.php?post_type=news-column&catet=blog-cat',
    	'blog/page/([0-9]{1,})/?$'            =>    'index.php?post_type=news-column&catet=blog-cat&paged=$matches[1]',
    );
    $rules = $NewRules + $rules;
    return $rules;
}
add_action( 'admin_head', 'remove_my_meta_boxen' );
function remove_my_meta_boxen() {
    remove_meta_box( 'postimagediv', 'news-column', 'side' );
    add_meta_box('postimagediv', __('メインイメージ'), 'post_thumbnail_meta_box', 'news-column', 'side', 'high');
}

/*add_action('admin_menu', 'my_menu_pages');
function my_menu_pages(){
    add_menu_page('PDF Upload', 'PDF Upload', 'manage_options', 'pdf-file', 'pdf_file_output' );
}
function pdf_file_output(){
   ob_start();
?>
    <div>tay</div>
<?php
   ob_get_contents();
}

*/


function mod_get_adjacent_post($postobj, $direction = 'prev', $post_types = 'news-column' , $taxonomy = 'news-cat') {
    global $wpdb;
    $post = $postobj;

    if(empty($post)) return NULL;
    if(!$post_types) return NULL;

    if(is_array($post_types)){
        $txt = '';
        for($i = 0; $i <= count($post_types) - 1; $i++){
            $txt .= "'".$post_types[$i]."'";
            if($i != count($post_types) - 1) $txt .= ', ';
        }
        $post_types = $txt;
    }

    $current_post_date = $post->post_date;

    $join = "INNER JOIN $wpdb->term_relationships ON object_id = ID INNER JOIN $wpdb->term_taxonomy ON $wpdb->term_taxonomy.term_taxonomy_id = $wpdb->term_relationships.term_taxonomy_id";
    $in_same_cat = false;
    $excluded_categories = '';
    $adjacent = $direction == 'prev' ? 'previous' : 'next';
    $op = $direction == 'prev' ? '<' : '>';
    $order = $direction == 'prev' ? 'DESC' : 'ASC';

    $join  = apply_filters( "get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories );
    $where = apply_filters( "get_{$adjacent}_post_where", $wpdb->prepare("WHERE p.post_date $op %s AND p.post_type IN({$post_types}) AND p.post_status = 'publish' AND taxonomy LIKE '%s'", $current_post_date, $taxonomy), $in_same_cat, $excluded_categories );
    $sort  = apply_filters( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1" );

    $query = "SELECT p.* FROM $wpdb->posts AS p $join $where $sort";
    $query_key = 'adjacent_post_' . md5($query);
    $result = wp_cache_get($query_key, 'counts');
    if ( false !== $result )
        return $result;

    $result = $wpdb->get_row("SELECT p.* FROM $wpdb->posts AS p $join $where $sort");
    if ( null === $result )
        $result = '';

    wp_cache_set($query_key, $result, 'counts');
    return $result;
}

function new_icon($entry=false){
    $days=3;
    $today=date('U'); 
    if($entry){
        $entry1= strtotime($entry);
    }else{
        $entry1 = get_the_time('U');
    }
    $diff1=date('U',($today - $entry1))/86400;
    //$icon = "";
    if ($days > $diff1) {
        ob_start();
        ?>
        <span class="iconNew newpost"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_new.svg" alt=""></span>
        <?php
        ob_get_contents();
    }
    return;
}
function pagenavi_t($maxpage,$paged,$url){
    ob_start();
    if($maxpage > 1){
        if($paged == 1){
            $prevpage = 'javascript:void(0)';
        }else if($paged == 2){
            $prevpage = $url;
        }else{
            $prevpage = $url.'page/'.($paged - 1).'/';
        }
        $nextpage = $paged < $maxpage ? $url.'page/'.($paged + 1).'/' : 'javascript:void(0)';

        if($paged != 1){
        ?>
        <a class="previouspostslink" rel="prev" href="<?php echo $prevpage;?>">PREV</a>
        <?php } ?>
        <input type="number" name="paged" class="current" min="1" max="<?php echo $maxpage;?>" value="<?php echo $paged?>" /><span class="pages">/ <?php echo $maxpage;?> ページ</span>
        <?php if($paged != $maxpage){ ?>
        <a class="nextpostslink" rel="next" href="<?php echo $nextpage?>">NEXT</a>
        <?php } ?>
        <?php
    }
    ob_get_contents();
}

function my_cut_string ($text, $length, $more = "･･･",$noecho = false) {
    $text = strip_tags ( $text);
    if(mb_strlen($text)>$length) {
      $tit= mb_substr($text,0,$length) ;
      $text = $tit. $more;
    }
    if(false === $noecho){
        echo $text;
    }else{
        return $text;
    }
}
function my_cut_string_en ($text, $length, $more = "･･･",$noecho = false) {
    $text = strip_tags ( $text);
    if(strlen($text)>$length) {
      $tit= substr($text,0,$length) ;
      $result = substr($tit, 0, strrpos($tit, ' '));
      $text = $result. $more;
    } 
    if(false === $noecho){
        echo $text;
    }else{
        return $text;
    }
}

?>