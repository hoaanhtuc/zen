<?php
	$dist = '';
	
	// get protocol.
	$url = $_SERVER['HTTP_HOST'].'/'.$dist;
	$protocol = empty($_SERVER["HTTPS"]) ? 'http://' : 'https://';
	
	// get host.
	$app_url = $protocol.$_SERVER['HTTP_HOST'].'/'.$dist;
	define('APP_URL', $app_url);
	define('APP_PATH', dirname(__FILE__).'/');
	define("APP_URL_SHORT", "//".$url);
	define("APP_URL_HTTPS", "https://".$url);
	define('APP_ASSETS', APP_URL_SHORT.'assets/');
	define('APP_ASSETS_IMG', APP_ASSETS.'img/');
	define('APP_ASSETS_JS', APP_ASSETS.'js/');
	define('APP_ASSETS_CSS', APP_ASSETS.'css/');
	define('APP_PATH_WP', dirname(__FILE__).'/wp/');
	
	define("APP_SP_URL",  APP_URL."sp/");
	define("APP_SP_PATH", APP_PATH."sp/");

	define('GOOGLE_MAP_API_KEY', '');
	define('GOOGLE_RECAPTCHA_KEY_API', '');
	define('GOOGLE_RECAPTCHA_KEY_SECRET', '');
	define('ALT_SITE','尾西自動車学校');
	define('APP_URL_CLASSES',APP_URL.'classes/');
	define('APP_URL_REASON',APP_URL.'reason/');
	define('APP_URL_STANDARD',APP_URL_CLASSES.'standard/');
	define('APP_URL_MOTOBIKE',APP_URL_CLASSES.'motorbike/');
	define('APP_URL_TEACHING',APP_URL_CLASSES.'teaching/');
	define('APP_URL_PRICE',APP_URL.'price/');
	define('APP_URL_ABOUT',APP_URL.'about/');
	define('APP_URL_FAQ',APP_URL.'faq/');
	define('APP_URL_NEWS',APP_URL.'news/');
	define('APP_URL_BLOG',APP_URL.'blog/');
	define('APP_URL_SITEMAP',APP_URL.'sitemap/');
	define('APP_URL_PRIVACY',APP_URL.'privacy/');
	define('APP_URL_ABOUT_ANCHOR1',APP_URL_ABOUT.'#anc02');
	define('APP_URL_CONTACT',APP_URL.'contact/');
	define('APP_URL_APPLICATION',APP_URL.'application/');
	define('PHONE_T01','028-3825-0782');
	define('PHONE_T02','0586-45-2882');
	define('FAX_T01','0586-45-9576');
	define('LANG_EN','en');
	define('LANG_VN','vn');
	$GLOBALS['lang'] = '';
	/* email list for forms */

	//Application 
	$aMailtoApp = array('hagiwara@alive-web.co.jp');
	$aBccToApp = array('vntesthatch2@gmail.com');
	$fromApp = "vntesthatch@gmail.com"; 

	//contact 
	$aMailtoContact = array('hagiwara@alive-web.co.jp');
	$aBccToContact = array('vntesthatch2@gmail.com');
	$fromContact = "vntesthatch@gmail.com";  
	
	//event 
	$aMailtoEvent = array('vntesthatch@gmail.com');
	$aBccToEvent = array('vntesthatch2@gmail.com');
	$fromEvent = "vntesthatch@gmail.com"; 
	
	//request 
	$aMailtoRequest = array('vntesthatch@gmail.com');
	$aBccToRequest = array('vntesthatch2@gmail.com');
	$fromRequest = "vntesthatch@gmail.com";

	$current_url = curPageURL();
	setup_lang();
	content_lang();
	
	function curPageURL() {
		$pageURL = 'http';
		if (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") $pageURL .= "s";
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		else $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		return $pageURL;
	}
	
	function setup_lang(){
		global $lang,$current_url;
		if(strpos($current_url,'/'.LANG_EN.'/') != false){
			$lang = LANG_EN;
		}else{
			$lang = LANG_VN;
		}
		
	}
	function __elang($arr=array()){
		global $lang;
		echo $arr[$lang];
	}
	function content_lang(){
		global $lang,$current_url;
		if(LANG_EN == $lang){
			$fileis = explode('?', $current_url);
			$file_path = str_replace('/'.LANG_EN.'/', '/', $fileis[0]);
			$file_path = str_replace(APP_URL, APP_PATH, $file_path);
			if(@is_file($file_path)){
				get_content_page($file_path);
			}else if(@is_file($file_path.'index.php')){
				get_content_page($file_path.'index.php');
			}else{
				get_content_page(APP_PATH.'index.php');
			}
		}
	}
	function get_content_page($file_path = ''){
		ob_start();
		include_once($file_path);
		ob_get_contents();
	}
	function get_banner_page_common(){
		global $current_url;
		ob_start();
		$array_common = array(
			APP_URL_REASON => array(
				'sub' => '尾西自動車学校が選ばれる理由',
				'main' => 'REASON',
				'bread' => array(
					'尾西自動車学校が選ばれる理由' => '',
				),
			),
			APP_URL_STANDARD => array(
				'sub' => '普通車',
				'main' => 'STANDARD',
				'bread' => array(
					'教習案内' => APP_URL_CLASSES,
					'普通車' => ''
				),
			),
			APP_URL_MOTOBIKE => array(
				'sub' => '二輪車',
				'main' => 'MOTORBIKE',
				'bread' => array(
					'教習案内' => APP_URL_CLASSES,
					'二輪車' => ''
				),
			),
			APP_URL_TEACHING => array(
				'sub' => '各種講習',
				'main' => 'TEACHING',
				'bread' => array(
					'教習案内' => APP_URL_CLASSES,
					'各種講習' => ''
				),
			),
			APP_URL_CLASSES => array(
				'sub' => '教習案内',
				'main' => 'CLASSES',
				'bread' => array(
					'教習案内' => '',
				),
			),
			APP_URL_FAQ => array(
				'sub' => 'よくあるご質問',
				'main' => 'FAQ',
				'bread' => array(
					'よくあるご質問' => '',
				),
			),
			APP_URL_ABOUT => array(
				'sub' => '学校案内 / スタッフ紹介',
				'main' => 'ABOUT',
				'bread' => array(
					'学校案内 / スタッフ紹介' => '',
				),
			),
			APP_URL_PRICE => array(
				'sub' => '料金案内',
				'main' => 'PRICE',
				'bread' => array(
					'料金案内' => '',
				),
			),
			APP_URL_CONTACT => array(
				'sub' => '資料請求・お問い合わせ',
				'main' => 'CONTACT',
				'bread' => array(
					'資料請求・お問い合わせ' => '',
				),
			),
			APP_URL_APPLICATION => array(
				'sub' => '仮入校お申し込み',
				'main' => 'APPLICATION',
				'bread' => array(
					'仮入校お申し込み' => '',
				),
			),
		);
		$arcurt = array();
		foreach($array_common as $k=>$v){
			if(strpos($current_url, $k) !== false){
				$arcurt = $v;
				break;
			}
		}
		if(!empty($arcurt)){
	?>
		<h1 class="bHead"><span><i><?php echo $arcurt['sub'];?></i><b><?php echo strtoupper($arcurt['main']);?></b></span></h1>
		<ul class="breadCum">
			<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
			<?php 
			if(!empty($arcurt['bread'])){ 
				foreach($arcurt['bread'] as $x => $y){
					if($y == ''){
			?>
				<li><?php echo $x;?></li>
			<?php
					}else{
			?>
				<li><a href="<?php echo $y?>"><?php echo $x;?></a></li>
			<?php
					}
				}
			}
			?>
		</ul>
	<?php
		}
		$content = ob_get_contents();
		ob_get_clean();
		echo $content;
	} 