<?php 
	function get_sidebar_mdf($position='all',$pcsp='all'){
	$class = 'container-1300';
	if($position == 'top'){
		$class = 'container-1200';
	}
	if($pcsp == 'pc' || $pcsp == 'all'){	
?>
		<div class="pc">
			<div class="wrap_bar <?php echo $position;?>">
				<div class="<?php echo $class;?>">
					<ul class="main_menu">
					<?php if($position == 'top'){ ?>
						<li><a href="<?php echo APP_URL;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_13.svg" width="23" height="23" alt="TOP"></i><b>TOP</b></a></li>
					<?php }else{ ?>
						<li><a href="<?php echo APP_URL;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/header/logo.svg" width="219" height="71" alt="<?php echo ALT_SITE;?>"></a></li>
					<?php } ?>
						<li><a href="<?php echo APP_URL_REASON;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_7.svg" width="28" height="21" alt="選ばれる理由"></i><b>選ばれる理由</b></a></li>
						<li>
							<a href="<?php echo APP_URL_CLASSES;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_8.svg" width="22" height="22" alt="教習・講習"></i><b>教習・講習</b></a>
							<div class="childs">
								<div class="childs-node">
									<a href="<?php echo APP_URL_STANDARD;?>">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_3.svg" width="40" height="20" alt="普通自動車"></i>
										<span>普通自動車</span>
									</a>
								</div>
								<div class="childs-node">
									<a href="<?php echo APP_URL_MOTOBIKE;?>">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_2.svg" width="48" height="26" alt="二輪車"></i>
										<span>二輪車</span>
									</a>
								</div>
								<div class="childs-node">
									<a href="<?php echo APP_URL_TEACHING;?>">
										<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_18.svg" width="34" height="34" alt="各種講習"></i>
										<span>各種講習</span>
									</a>
								</div>
							</div>
						</li>
						<li><a href="<?php echo APP_URL_PRICE;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_9.svg" width="25" height="25" alt="料金案内"></i><b>料金案内</b></a></li>
						<li><a href="<?php echo APP_URL_ABOUT;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_10.svg" width="17" height="22" alt="学校案内"></i><b>学校案内</b></a></li>
						<li><a href="<?php echo APP_URL_ABOUT_ANCHOR1;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_11.svg" width="29" height="29" alt="スタッフ紹介"></i><b>スタッフ紹介</b></a></li>
						<li><a href="<?php echo APP_URL_FAQ;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_12.svg" width="24" height="24" alt="よくあるご質問"></i><b>よくあるご質問</b></a></li>
					</ul>
				</div>
			</div>
		</div>
	<?php }
	if($pcsp == 'sp' || $pcsp == 'all'){
	?>
		<div class="sp">
			<div class="wrap_bar <?php echo $position;?>">
				<div class="box00">
					<div class="logo"><a href="<?php echo APP_URL;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/header/logo.svg" width="180" height="53" alt=""></a></div>
					<div class="toplogo">
						<a href="<?php echo APP_URL;?>">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_13.svg" width="23" height="23" alt="TOP"></i>
							<b>TOP</b>
						</a>
					</div>
				</div>
				<div class="box">
					
					<div class="box01">
						<p class="box01_title">尾西自動車学校について</p>
						<ul>
							<li class="full">
								<a href="<?php echo APP_URL_REASON;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_7.svg" width="34" height="25" alt="尾西自動車学校が選ばれる理由"></i>
									<b>尾西自動車学校が選ばれる理由</b>
								</a>
							</li>
							<li>
								<a href="<?php echo APP_URL_ABOUT;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_10.svg" width="23" height="30" alt="学校案内アクセス"></i>
									<b>学校案内<br>アクセス</b>
								</a>
							</li>
							<li>
								<a href="<?php echo APP_URL_ABOUT_ANCHOR1;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_11.svg" width="34" height="34" alt="スタッフ紹介"></i>
									<b>スタッフ<br>紹介</b>
								</a>
							</li>
							<li>
								<a href="<?php echo APP_URL_FAQ;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_12.svg" width="32" height="32" alt="よくあるご質問"></i>
									<b>よくある<br>ご質問</b>
								</a>
							</li>
						</ul>
					</div>
					<div class="box02">
						<p class="box02_title">教習案内</p>
						<ul>
							<li>
								<a href="<?php echo APP_URL_STANDARD;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_3.svg" width="40" height="20" alt="普通自動車"></i>
									<b>普通自動車</b>
								</a>
							</li>
							<li>
								<a href="<?php echo APP_URL_MOTOBIKE;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_2.svg" width="48" height="26" alt="二輪車"></i>
									<b>二輪車</b>
								</a>
							</li>
							<li>
								<a href="<?php echo APP_URL_TEACHING;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_18.svg" width="34" height="34" alt="各種教習"></i>
									<b>各種教習</b>
								</a>
							</li>
							<li class="full">
								<a href="<?php echo APP_URL_PRICE;?>">
									<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_9.svg" width="34" height="34" alt="料金案内"></i>
									<b>料金案内</b>
								</a>
							</li>
						</ul>
					</div>
					<div class="box03">
						<p class="box03_title">在校生向けコンテンツ</p>
						<ul>
							<li>
								<a href="http://wwww.be-sai.com/yoyaku/" target="_blank">
									<b>自宅送迎<br>バス予約</b>
								</a>
							</li>
							<?php
							if(!(defined('ABSPATH'))){
								include_once(APP_PATH.'wp/wp-load.php');
							}
							$pdf = get_field('upload_pdf',59);
							?>
							<li>
								<a href="<?php echo $pdf['url'];?>" target="_blank">
									<b>学科時間割</b>
								</a>
							</li>
							<li>
								<a href="<?php echo APP_URL_CLASSES;?>#anc02">
									<b>満点様</b>
								</a>
							</li>
						</ul>
					</div>
					<div class="box04">
						<p class="box04_title">採用情報はこちら</p>
						<?php get_contact_phone();?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
		ob_start();
		$content = ob_get_contents();
		echo $content;
}

	function get_contact_phone(){
	?>
		<div class="wrap-box-phone">
			<ul>
				<li class="contact-info-box_ct"><a href="<?php echo APP_URL_CONTACT;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_1_a.svg" width="33" height="38" alt="資料請求"></i><span>資料請求</span></a></li>
				<li class="contact-info-box_if"><a href="<?php echo APP_URL_APPLICATION;?>"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_1_b.svg" width="35" height="33" alt="仮入校申込"></i><span>仮入校申込</span></a></li>
			</ul>
			<div class="phone">
				<a href="tel:<?php echo PHONE_T01;?>">
					<label class="sp">タップして<br>電話をかける</label>
					<i>
						<img class="pc" src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_phone_white.svg" width="28" height="38" alt="Phone">
						<img class="sp" src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_14.svg" width="18" height="25" alt="Phone">
					</i>
					<span class="phone-number"><?php echo PHONE_T01;?></span>
					<span class="time">受付時間｜平日 9:00-20:00／日・祝 9:00-16:30</span>
				</a>
			</div>
		</div>
	<?php
		ob_start();
		$content = ob_get_contents();
		echo $content;
	}
?>