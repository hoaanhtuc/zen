<div class="contact-info-box">
	<div class="infob">
		<h2>Reservation & Contact</h2>
		<p>We looking forward to your reservation. <br>Feel free to contact us.</p>
		<div class="phone_worktime">
			<span>
				<label class="sp">Tap <br>to call</label>
				<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_phone_gold.svg" width="26" height="26" alt=""></i>
				<b><?php echo PHONE_T01;?></b>
			</span>
			<span>
				Service time : 10:00 ~ 19:00<br class="pc"><em class="sp">|</em>
				Closed on Monday
			</span>
		</div>
		<div class="btn_contact">
			<a href="<?php echo APP_URL_CONTACT;?>">Reserve / contact us here</a>
		</div>
	</div>
	<div class="mapb">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.3902734636913!2d106.70281591514974!3d10.781391462049339!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!3m2!1sen!2s!4v1539937005037" width="1000" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>