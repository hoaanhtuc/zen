<?php
if(!defined("APP_PATH")){
	exit(0);
}
if(!defined("WP_PLUGIN_DIR")){
	include(APP_PATH . '/wp/wp-load.php');
}
class Calendar {  

    public function __construct(){     
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }
     
    /********************* PROPERTY ********************/  
    private $dayLabels = array("月","火","水","木","金","土","日");
     
    private $currentYear=0;
     
    private $currentMonth=0;
     
    private $currentDay=0;
     
    private $currentDate=null;
     
    private $daysInMonth=0;
     
    private $naviHref= null;
     
    /********************* PUBLIC **********************/  
        
    /**
    * print out the calendar
    */
    public function show($listdate=array()) {  
		$year = $listdate['year'];
		$month = $listdate['month'];
		$listday = $listdate['listday'];
        $this->currentYear=$year;
		
		$this->currentDay=0;
         
        $this->currentMonth=$month;
         
        $this->daysInMonth=$this->_daysInMonth($month,$year);  
         
		$content='<div class="cld">'.$this->_createNavi().
                   '<div class="cld_ct">'.
					'<div class="calendar">'.
                        '<div class="box-content">'.
                                '<ul class="label">'.$this->_createLabels().'</ul>';   
                                $content.='<div class="clear"></div>';     
                                $content.='<ul class="dates">';    
                                 
                                $weeksInMonth = $this->_weeksInMonth($month,$year);
                                // Create weeks in a month
                                for( $i=0; $i<$weeksInMonth; $i++ ){
                                     
                                    //Create days in a week
                                    for($j=1;$j<=7;$j++){
                                        $content.=$this->_showDay($i*7+$j,$listday);
                                    }
                                }
                                 
                                $content.='</ul>';
                                 
                                $content.='<div class="clear"></div>';     
             
                        $content.='</div>';
                 
        $content.='</div></div></div>';
        return $content;   
    }
     
    /********************* PRIVATE **********************/ 
    /**
    * create the li element for ul
    */
    private function _showDay($cellNumber,$listday=array()){
         
        if($this->currentDay==0){
             
            $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
                     
            if(intval($cellNumber) == intval($firstDayOfTheWeek)){
                 
                $this->currentDay=1;
                 
            }
        }
         
        if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){
             
            $this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
             
            $cellContent = $this->currentDay;
             
            $this->currentDay++;   
             
        }else{
             
            $this->currentDate =null;
 
            $cellContent=null;
        }
             
         
        return '<li id="li-'.$this->currentDate.'" class="'.($cellNumber%7==1?' start':($cellNumber%7==0?' end': ($cellNumber%7==6 ? ' kend' : ''))).($cellContent==null?' mask':'').(in_array($cellContent,$listday)? ' active' : '').'">'.$cellContent.'</li>';
    }
     
    /**
    * create navigation
    */
    private function _createNavi(){
         
        $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
         
        $nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
         
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
         
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
         
        return
             '<p class="cld_my">'.
                /*'<a class="prev" href="'.$this->naviHref.'?month='.sprintf('%02d',$preMonth).'&year='.$preYear.'">Prev</a>'.*/
                    date('Y年m月',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).
                /*'<a class="next" href="'.$this->naviHref.'?month='.sprintf("%02d", $nextMonth).'&year='.$nextYear.'">Next</a>'.*/
            '</p>';
    }
         
    /**
    * create calendar week labels
    */
    private function _createLabels(){  
                 
        $content='';
         
        foreach($this->dayLabels as $index=>$label){
             
            $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';
 
        }
         
        return $content;
    }
     
     
     
    /**
    * calculate number of weeks in a particular month
    */
    private function _weeksInMonth($month=null,$year=null){
         
        if( null==($year) ) {
            $year =  date("Y",time()); 
        }
         
        if(null==($month)) {
            $month = date("m",time());
        }
         
        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month,$year);
         
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
         
        $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
         
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
         
        if($monthEndingDay<$monthStartDay){
             
            $numOfweeks++;
         
        }
         
        return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    private function _daysInMonth($month=null,$year=null){
         
        if(null==($year))
            $year =  date("Y",time()); 
 
        if(null==($month))
            $month = date("m",time());
             
        return date('t',strtotime($year.'-'.$month.'-01'));
    }
	private function get_data_sbc(){
		$file_require = WP_PLUGIN_DIR . "/wp-simple-booking-calendar/library/WpSimpleBookingCalendar/Model.php";
		if(@file_exists($file_require)){
			include_once( $file_require );
			$c_opt = get_option(WpSimpleBookingCalendar_Model::OPTIONS_KEY );
		}else{
			$c_opt = get_option('wp-simple-booking-calendar-options');
		}
		$data_calendar = array();
		if($c_opt && sizeof($c_opt['calendars']) > 0){
			foreach($c_opt['calendars'] as $c_opt_sg){
				if("calendar01" === $c_opt_sg['calendarName'] && !empty($c_opt_sg['calendarJson'])){
					$data_calendar = (array) json_decode($c_opt_sg['calendarJson']);
					break;
				}	
			}
		}
		return $data_calendar;
	}
    public function setup_data_calendar(){
		$data_calendar = $this->get_data_sbc();
		//return $data_calendar;
		if(sizeof($data_calendar)>0){
			$arrlist = array();
			$year_cur = (int)date("Y",time());
			$month_cur = (int)date("m",time());
			$day_cur = (int)date("d",time());
			foreach($data_calendar as $key=>$val){
				$year_il = (int)str_replace('year','',$key);
				if($year_cur <= $year_il){
					$l_m = (array)$val;
					foreach($l_m as $key1=>$val1){
						$month_il = (int)str_replace('month','',$key1);
						if(($month_il >= $month_cur) || ($year_cur < $year_il)){
							$arr_sg = array();
							$date_il = array();
							$l_d = (array)$val1;
							foreach($l_d as $key2=>$val2){
								$dayb = (int)str_replace('day','',$key2);
								if("booked" === $val2 && (($dayb >= $day_cur ) || ($month_il >= $month_cur) || ($year_cur < $year_il))){
									$date_il[] = str_replace('day','',$key2);
								}
							}
							$arr_sg['year'] = $year_il;
							$arr_sg['month'] = $month_il;
							$arr_sg['listday'] = $date_il;
							$arrlist[] = $arr_sg;
							unset($arr_sg);
						}
					}
				}
			}
			if(sizeof($arrlist)>0){
				return $this->sort_data_list($arrlist);
			}
		}
		return;
	} 
	
	public function day_name($date_string){
		$nbd = date("N",strtotime($date_string));
		return $this->dayLabels[$nbd-1];
	}
	
	public function sort_data_list($list_array){
		usort($list_array, function($a, $b) { 
			return $a['year'] - $b['year'] ? : $a['month'] - $b['month']; 
		});
		return $list_array;
	}
	
	static function compare_lastname($a, $b){
		return strnatcmp($a, $b);
	}
	public function list_date_tt($arrlist,$ntext = false){
		if(!empty($arrlist)){
			foreach($arrlist as $listday){
				foreach($listday['listday'] as $listday_day_sg){
					if($ntext){
						$list_select_form[] = $listday['year'].'.'.sprintf("%'.02d",$listday['month']).'.'.sprintf("%'.02d",$listday_day_sg).' ('.$this->day_name($listday['year'].'-'.$listday['month'].'-'.$listday_day_sg).')';
					}else{
						$list_select_form[] = $listday['year'].'年'.$listday['month'].'月'.$listday_day_sg.'日 ('.$this->day_name($listday['year'].'-'.$listday['month'].'-'.$listday_day_sg).')';
					}
				}
			}
			return $list_select_form;
		}
		return;
	}
}

	

?>