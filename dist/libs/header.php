<!-- Google Tag Manager -->

<!-- End Google Tag Manager -->

<header class="header clearfix">
	<div class="header_wrap clearfix">
		<div class="logo">
			<a href="<?php echo APP_URL;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_logo.svg" width="160" height="37" alt="ZEN Saigon"></a>
		</div>
		<div class="header_menu">
			<ul class="pc">
				<li class="active"><a href="<?php echo APP_URL;?>">HOME</a></li>
				<li class="menu"><a href="<?php echo APP_URL_MENU;?>">MENU</a></li>
				<li class="shop-info"><a href="<?php echo APP_URL_SHOPINFO;?>">SHOP INFO</a></li>
				<li class="swap-lang"><a href="<?php echo APP_URL;?>"><label class="active">EN</label>/<label>VN</label></a></li>
				<li class="phone-worktime"><a href="<?php echo APP_URL;?>">
					<span><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_phone_b.svg" width="19" height="19" alt=""></i><b><?php echo PHONE_T01;?></b></span>
					<span>10:00 ~ 19:00 / Closed on Monday</span>
				</a></li>
				<li class="reserve"><a href="<?php echo APP_URL_RESERVE;?>">
					<span><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_rever_w.svg" width="23" height="25" alt=""></i><b>RESERVE</b></span>
				</a></li>
			</ul>
			<div class="sp">
				<div class="phone">
					<a href="tel:<?php echo PHONE_T01;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_phone_w.svg" width="20" height="" alt=""></a>
				</div>
				<div class="reserve">
					<a href="<?php echo APP_URL_RESERVE;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_rever_w.svg" width="23" height="25" alt=""></a>
				</div>
				<div class="btn_menu">
					<span></span>
				</div>
			</div>
		</div>
	</div>
	<div class="menu_sp">
		<ul>
			<li><a href="<?php echo APP_URL;?>">HOME</a></li>
			<li><a href="<?php echo APP_URL_MENU;?>">Menu</a></li>
			<li><a href="<?php echo APP_URL_SHOPINFO;?>">Shop info</a></li>
			<li><a href="<?php echo APP_URL_NEWS;?>">News</a></li>
			<li><a href="<?php echo APP_URL_RESERVE;?>">Reservation</a></li>
		</ul>
		<div class="phone_worktime">
			<a href="tel:<?php echo PHONE_T01;?>">
				<p class="phone">
					<span>Tap to<br>call us</span>
					<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_phone_w.svg" width="24" height="24" alt=""></i>
					<b>028-3825-0782</b>
				</p>
				<p class="worktime">Service time : 10:00 ~ 19:00 Closed on Monday</p>
			</a>
		</div>
		<div class="swap-lang">
			<a href="<?php echo APP_URL;?>"><label class="active">EN</label>/<label>VN</label></a>
		</div>
	</div>
</header>
