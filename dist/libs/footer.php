<?php include_once(APP_PATH.'libs/contact-box.php');?>
<footer class="footer">
    <div class="container">
    	<div class="left">
            <div class="logo">
                <a href="<?php echo APP_URL;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_logo_w.svg" width="167" height="38" alt=""></a>
            </div>
            <p class="address">20 Le Thanh Ton, Ben Nghe Ward, Dictrict 1, TP. HCM</p>   
            <div class="sp sns"><a href="<?php echo APP_URL_FACEBOOK;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_face.svg" width="20" height="20" alt=""></a></div>
        </div>
        <div class="right pc">
            <ul>
                <li><a href="<?php echo APP_URL;?>">home</a></li>
                <li><a href="<?php echo APP_URL_MENU;?>">menu</a></li>
                <li><a href="<?php echo APP_URL_SHOPINFO;?>">shop info</a></li>
                <li><a href="<?php echo APP_URL_NEWS;?>">news</a></li>
                <li><a href="<?php echo APP_URL_RESERVATION;?>">reservation</a></li>
            </ul>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p class="text">Copyright(C) 2018 Zen Saigon All rights reserved.</p>
            <div class="swap-lang pc">
                <p>
                    <a href="<?php echo APP_URL;?>"><label class="active">EN</label>/<label>VN</label></a>
                </p>
                <p>
                    <a href="<?php echo APP_URL_FACEBOOK;?>"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_face.svg" width="27" height="27" alt=""></a>
                </p>
            </div>
        </div>
    </div>
    <div class="btn_2top">
        <a href="#totop"></a>
    </div>
</footer>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo APP_ASSETS; ?>js/lib/jquery1-12-4.min.js"><\/script>')</script>
<!-- <script src="<?php echo APP_ASSETS; ?>js/lib/common.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/smoothscroll.js"></script> -->
<!-- <script src="<?php echo APP_ASSETS; ?>js/lib/biggerlink.js"></script> -->
<script src="<?php echo APP_ASSETS_JS;?>lib/slick.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/functions.min.js"></script>