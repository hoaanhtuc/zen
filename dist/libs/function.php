<?php
// for rewrite URL
function slider_demo(){
	ob_start();
?>
	<div class="imgs_demo">
		<ul>
			<li><img src="<?php echo APP_ASSETS_IMG;?>top/img01.jpg" alt="" /></li>
			<li><img src="<?php echo APP_ASSETS_IMG;?>top/img02.jpg" alt="" /></li>
			<li><img src="<?php echo APP_ASSETS_IMG;?>top/img03.jpg" alt="" /></li>
		</ul>
	</div>
<?php
	ob_get_contents();
}
function getArrUrl($var) {
	$nvar = Array();
	$na = explode("/", $var);
	for($i=0; $i<count($na)-1;$i+=4) {
		$nvar["$na[$i]"] = $na[$i+1];
	}
	return $nvar;
}
$args = (!empty($_GET['args'])) ? getArrUrl($_GET['args']) : '';

/*how to use
URL: http://abc.com/sp/blog/title-of-single
$title = $_GET['args'];
 
or http://abc.com/sp/blog/page/2
$paged = $args['page'];
*/

//end for rewrite URL

function cutString($str,$len, $moreStr = "...") {		
	$mystr = "";
	$str = strip_tags($str);
	$str = preg_replace('/\r\n|\n|\r/','',$str);
	if(mb_strlen($str) > $len) {
		$newstr = mb_substr($str,0,$len);			
		$mystr = $newstr.$moreStr;
	} else $mystr = $str;
	return $mystr;			
}


//get image from content
function get_first_image($cnt, $noimg = true){
	$first_img = '';
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $cnt, $matches);
	for($i=0;$i<=10;$i++){
			$first_img = $matches[1][$i];
			$ext = substr( $first_img, -3);
			if($ext == 'jpg' or $ext == 'png'){
				return $first_img;
				break;
			}
		}
	 if(empty($first_img) || $first_img == "") {
		if($noimg) $first_img = APP_URL . "assets/img/common/other/img_nophoto.jpg";
		else return false;
	 }  
	 return $first_img;
}

//get image from content 
function catch_that_image($noimg = true) {
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches[1][0];

	if(empty($first_img) || $first_img == "") {
		if($noimg) $first_img = APP_URL . "assets/img/common/other/img_nophoto.jpg";
		else return false;
	}  
	return $first_img;
}