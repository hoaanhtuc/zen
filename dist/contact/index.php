<?php
session_start();
header("Cache-control: private"); 
header_remove("Expires"); 
header_remove("Cache-Control"); 
header_remove("Pragma"); 
header_remove("Last-Modified");
ob_start();
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
<meta name = "format-detection" content = "telephone=no">

<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/exvalidation.css">

<!-- Anti spam part1: the contact form start -->
</head>
<body id="contact" class="contact_not_app">
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<h2 id="logoIcon" class="taC"><a href="<?php echo APP_URL; ?>"><img src="<?php echo APP_ASSETS ?>/img/common/header/logo.svg" width="214" height="66" alt="尾西自動車学校"></a></h2>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php get_banner_page_common();?>
	<?php

	// 設定
	require(APP_PATH."libs/form/jphpmailer.php");
	$script = "";
	$gtime = time();

	//always keep this
	if(isset($_POST['action'])) {
		$action = htmlspecialchars($_POST['action']);
	} else {
		$action = "";
	}
	if(isset($_POST['url'])) {
		$reg_url = htmlspecialchars($_POST['url']);
	}
	//end always keep this


	//お問い合わせフォーム内容
	if(isset($_POST['choose'])) {
		$reg_choose = htmlspecialchars($_POST['choose']);
	}
	$reg_check01 = (!empty($_POST['check01'])) ? $_POST['check01'] : array();
	if(isset($_POST['checkAll01'])) {
		$reg_checkAll01 = htmlspecialchars($_POST['checkAll01']);
	}
	if(isset($_POST['nameuser'])) {
		$reg_name = htmlspecialchars($_POST['nameuser']);
	}
	if(isset($_POST['zipcode'])) {
		$reg_zipcode = htmlspecialchars($_POST['zipcode']);
	}
	if(isset($_POST['add'])) {
		$reg_add = htmlspecialchars($_POST['add']);
	}
	if(isset($_POST['tel'])) {
		$reg_tel = htmlspecialchars($_POST['tel']);
	}
	if(isset($_POST['email'])) {
		$reg_email = htmlspecialchars($_POST['email']);
	}

	if(isset($_POST['content'])) {
		$reg_content = htmlspecialchars($_POST['content']);
	}
	// 処理分岐
	if($action == "confirm") {
		if(!isset($_SESSION['ses_gtime_step2'])){
			$_SESSION['ses_gtime_step2'] = $gtime;
		}
		require("step2.php");
	} elseif($action == "send") {
		require("step3.php");
	} else {
		require("step1.php");
	}
	?>
		</main>
	</div>
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<p id="copyright"><a href="<?php echo APP_URL;?>" class="sp"><img src="<?php echo APP_ASSETS_IMG;?>common/header/logo.svg"  width="271" height="81" alt="尾西自動車学校"></a>Copyright © 2018 Bisai Driving School All Rights Reserved.</p>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/common.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/smoothscroll.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/ajaxzip3.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/jquery.cookie.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/exvalidation.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/form/exchecker-ja.js"></script>
	<script type="text/javascript">
		function callExvalidation() {
			$('input.err').removeClass('err');
			$('.formError').remove();
		  	$("form.form-1").exValidation({
		    rules: {
				nameuser: "chkrequired",
				// zipcode: "chkrequired",
				// add: "chkrequired",
				tel: "chkrequired chktel",
				email: "chkrequired chkemail"
		    },
		    stepValidation: true,
		    scrollToErr: true,
			errHoverHide: true,
			// firstValidate: true,
			// customListener     : "blur"
		  });
		};
		callExvalidation();
	</script>
	<script type="text/javascript">
		<!--
		function check(){
			var flag = 0;
			if(!document.form1.checkReq.checked){
				flag = 1;
			}
			if(flag){
				window.alert('「個人情報の取扱いに同意する」にチェックを入れて下さい');
				return false;
			} else{
				return true;
			}
		}
		// -->
	</script>
	<script>
		$(document).ready(function() {
			var address = "info" + "@" + "bisai.co.jp";
			$("#mailContact").attr("href", "mailto:" + address);
			$("#mailContact").text(address);
		});
	</script>
	<script>
		function checkBox() {
			$("input:radio[name=choose]").each(function() {
				if($('#yes')[0].checked === true) {
					$('.div_chk').removeClass('dpn');
					$('.zip_add').removeClass('dpn');
					$('.txt_editor').addClass('dpn');
					$('#checkbox01').addClass('chkcheckbox errPosRight');
					$('#zipcode').addClass('chkrequired errPosRight');
					$('#add').addClass('chkrequired errPosRight');
					$('#content').removeClass('chkrequired errPosRight').val("");
					callExvalidation();
				} else if($('#no')[0].checked === true) {
					$('.div_chk').addClass('dpn');
					$('.zip_add').addClass('dpn');
					$('.txt_editor').removeClass('dpn');
					$('#content').addClass('chkrequired errPosRight');
					$('#checkbox01').removeClass('chkcheckbox errPosRight err');
					$('#zipcode').removeClass('chkrequired errPosRight err').val("");
					$('#add').removeClass('chkrequired errPosRight err').val("");
					$('#checkbox01 input[type="checkbox"]').prop("checked",false);
				}	
			});
		}
		checkBox();
		$("input:radio[name=choose]").on('change', function(){
	        checkBox();
	        callExvalidation();
	    })
	</script>
</body>
</html>