<div class="formContent">
	<div class="sp"><p class="contact_not_app__p01">資料請求や各種お問い合わせは<br>以下のフォームまたはお電話より<br>受け付けております</b></div>
	<p class="ttl btn01">
		<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_animal.svg" alt="">
		<span>まだ入校を迷っている方も<br>お気軽にお問い合わせください！</span>
	</p>
	<p class="contact_not_app__p01"><span class="pc"><b>資料請求や各種お問い合わせは以下のフォーム<br>またはお電話より受け付けております</b></span></p>
</div>
<div class="telBox">
	<h3 class="formTtl">お電話からのお問い合わせ</h3>
	<div class="pc"><p class="txtTel"><i class="sp">タップして<br>電話をかける</i><span>0120-615-618</span><em>受付時間｜平日 9:00-20:00／日・祝 9:00-16:30</em></p></div>
	<div class="sp"><p class="txtTel"><a href="tel:0120-615-618"><i class="sp">タップして<br>電話をかける</i><span>0120-615-618</span><em>受付時間｜平日 9:00-20:00／日・祝 9:00-16:30</em></a></p></div>
</div>
<h3 class="formTtl">フォームからのお問い合わせ</h3>
<p class="txtStep pc"><img src="<?php echo APP_ASSETS; ?>img/contact/txt_step1.svg" alt=""/></p>
<p class="txtStep sp"><img src="<?php echo APP_ASSETS; ?>img/contact/txt_step1_sp.svg" alt=""/></p>
<p class="txtContact">下記の情報に御記入の上、【入力内容を確認する】ボタンを押してください。</p>
<form method="post" class="form-1" action="<?php echo $script ?>?g=<?php echo $gtime ?>" name="form1" onSubmit="return check()">
	<div class="formBlock">
		<p class="hid_url">Leave this empty: <input type="text" name="url"></p><!-- Anti spam part1: the contact form -->
		<table class="tableContact" cellspacing="0">
			<tr>
				<th><p><em>必須</em><span>お問い合わせ内容</span></p></th>
				<td>
					<div>
						<p class="chkradio checkStyle" id="radioarray01">
							<label for="yes"><input type="radio" id="yes" name="choose" value="資料請求">
							<span>資料請求</span></label>
							<label for="no"><input type="radio" id="no" name="choose" value="その他お問い合わせ">
							<span class="lh23">その他<br>お問い合わせ</span></label>
						</p>
						<div class="div_chk dpn">
							<p class="txt">ご希望の車種をお選びください</p>
							<p class="checkStyle" id="checkbox01">
								<label for="check1"><input type="checkbox" name="check01[]" id="check1" value="四輪"><span>四輪</span></label>
								<label for="check2"><input type="checkbox" name="check01[]" id="check2" value="二輪"><span>二輪</span></label>
							</p>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<th><p><em>必須</em><span>お名前</span></p></th>
				<td><div><input type="text" name="nameuser" id="nameuser" placeholder="例）尾西 太郎" class="size1"></div></td>
			</tr>
			<tr class="zip_add dpn">
				<th><p><em>必須</em><span>住所</span></p></th>
				<td><div>
					<p style="margin-bottom: 10px;"><span class="txtZip">〒</span><input type="text" placeholder="例）494-0002" name="zipcode" id="zipcode" onChange="AjaxZip3.zip2addr(this,'','add','add')" class="size3"><em class="txtNote">※郵便番号を入力すると<br>自動で住所が入力されます</em></p>
					<p><input type="text" name="add" id="add" placeholder="例）一宮市篭屋3丁目12番45号" class="size4"></p>
				</div></td>
			</tr>
			<tr>
				<th><p><em>必須</em><span>電話番号</span></p></th>
				<td><div>
					<input type="text" name="tel" id="tel" placeholder="例）0900000000" class="size1">
				</div></td>
			</tr>
			<tr class="nobd">
				<th><p><em>必須</em><span>メールアドレス</span></p></th>
				<td><div>
					<input placeholder="例）mail@sample.com" type="email" name="email" id="email" class="size1">
				</div></td>
			</tr>
			
			<tr class="txt_editor dpn">
				<th><p><em>必須</em><span>お問い合わせ内容</span></p></th>
				<td><div><textarea name="content" id="content"></textarea></div></td>
			</tr>
		</table>
		<div class="privacyBox">
			<div class="inner">
				<p class="t0b10">【個人情報の取扱いについて】<br>
				本フォームからお客様が記入・登録された個人情報は、資料送付・電子メール送信・電話連絡などの目的で利用・保管します。</p>
				<div class="privacyTxt">
					<h3>プライバシーポリシー</h3>
					<p>尾西自動車学校（以下「当校」）は、以下のとおり個人情報保護方針を定め、個人情報保護の仕組みを構築し、全従業員に個人情報保護の重要性の認識と取組みを徹底させることにより、個人情報の保護を推進致します。</p>
					<h3>個人情報の管理</h3>
					<p>当社は、お客さまの個人情報を正確かつ最新の状態に保ち、個人情報への不正アクセス・紛失・破損・改ざん・漏洩などを防止するため、セキュリティシステムの維持・管理体制の整備・社員教育の徹底等の必要な措置を講じ、安全対策を実施し個人情報の厳重な管理を行ないます。</p>
					<h3>個人情報の利用目的</h3>
					<p>お客さまからお預かりした個人情報は、当社からのご連絡や業務のご案内やご質問に対する回答として、電子メールや資料のご送付に利用いたします。</p>
					<h3>個人情報の第三者への開示・提供の禁止</h3>
					<p>当社は、お客さまよりお預かりした個人情報を適切に管理し、次のいずれかに該当する場合を除き、個人情報を第三者に開示いたしません。
					お客さまの同意がある場合
					お客さまが希望されるサービスを行なうために当社が業務を委託する業者に対して開示する場合
					法令に基づき開示することが必要である場合</p>
					<h3>個人情報の安全対策</h3>
					<p>当社は、個人情報の正確性及び安全性確保のために、セキュリティに万全の対策を講じています。</p>
					<h3>ご本人の照会</h3>
					<p>お客さまがご本人の個人情報の照会・修正・削除などをご希望される場合には、ご本人であることを確認の上、対応させていただきます。</p>
					<h3>法令、規範の遵守と見直し</h3>
					<p>当社は、保有する個人情報に関して適用される日本の法令、その他規範を遵守するとともに、本ポリシーの内容を適宜見直し、その改善に努めます。</p>
				</div>
				<p class="btnCheck"><label><input type="checkbox" name="checkReq" value="ok"><span>個人情報の取り扱いに同意する</span></label></p>
			</div>
		</div>
		<p class="btnConfirm">
			<button id="btn"><span>入力内容を確認する</span></button>
			<input type="hidden" name="action" value="confirm">
		</p>
		<p class="txtContact01">上記フォームで送信できない場合は、必要項目をご記入の上、
		<a id="mailContact" href="#"></a>までメールをお送りください。</p><!-- Anti spam part2: clickable email address -->
	</div>
</form>