<?php
// Author: A+LIVE
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>
<body id="blog" class='blog'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h2 class="bHead"><span><i>ブログ</i><b>BLOG</b></span></h2>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li><a href="<?php echo APP_URL; ?>">ブログ</a></li>
				<li>見出しが入ります</li>
			</ul>

			<div class="blogBlock clearfix">
				<div class="primary">
					<h3 class="mHead"><i>2018.00.00</i><em>コラム</em><span>見出しが入ります見出しが入ります見出しが入ります</span></h3>
					<p class="mainImg"><img src="<?php echo APP_ASSETS; ?>img/blog/img_photo1.jpg" alt=""></p>
					<div class="cmsContent">
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
						<h3>大型普通二輪の場合</h3>
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
						<p><img src="<?php echo APP_ASSETS; ?>img/blog/img_photo2.jpg" alt=""></p>
						<h4>h4見出しが入りますh4見出しが入りますh4見出しが入ります</h4>
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>

						<ul class="snsSub">
							<li>SHARE</li>
							<li class="tw_btn"><a href="" class="share" data-network="twitter"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_tw.svg" alt="twitter" class="opa" style="opacity: 1;"></a></li>
							<li class="line_btn"><a class="share" data-text="" data-network="line" href=""><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_line.svg" alt="facebook" class="opa"></a></li>
						</ul>
					</div>

					<ul class="pagerLink">
						<li class="prev">
							<a href="">
								<span class="img"><img src="<?php echo APP_ASSETS ?>img/blog/img_thumb.jpg" alt=""></span>
								<strong>ここに次の記事のタイトルが表示されますここに次の記事のタイトルが表</strong>
							</a>
						</li>
						<li class="back"><a href="">一覧へ戻る</a></li>
						<li class="next">
							<a href="">
								<span class="img"><img src="<?php echo APP_ASSETS ?>img/blog/img_thumb.jpg" alt=""></span>
								<strong>ここに次の記事のタイトルが表示されますここに次の記事のタイトルが表</strong>
							</a>
						</li>
					</ul>
				</div>

				<div class="secondary">
					<div class="sBox">
						<h3>CATEGORY</h3>
						<ul class="catLink">
							<li><a href="">カテゴリ１が入ります ( 1 )</a></li>
							<li><a href="">カテゴリ２（0）</a></li>
							<li><a href="">カテゴリ3（0）</a></li>
						</ul>
					</div>
					<div class="sBox pc">
						<h3>NEW POSTS</h3>
						<ul class="blogList">
							<li>
								<a href="#">
									<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""><span class="iconNew"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_new.svg" alt=""></span></p>
									<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
									<strong>ブログのタイトルブログの、タイトルブログのタイトル！</strong></h3>
								</a>
							</li>
							<li>
								<a href="#">
									<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""></p>
									<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
									<strong>ブログのタイトルブログのタイトルブログのタイトルブログのタイトル！</strong></h3>
								</a>
							</li>
							<li>
								<a href="#">
									<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""></p>
									<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
									<strong>ブログのタイトルブログのタイトルブログのタイトルブログのタイトル！</strong></h3>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>