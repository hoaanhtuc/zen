<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../');
include_once($path.'/app_config.php');
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="reason" class='reason'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h2 class="bHead"><span><i>尾西自動車学校が選ばれる理由</i><b>REASON</b></span></h2>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li>尾西自動車学校が選ばれる理由</li>
			</ul>
			
			<div class="container-1000 rbox_01">
				<div class="container-800">
					<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
				</div>
				<ul class="anchor clearfix">
					<li><a href="#anc01">
						<span>
							<i>01</i>
							<b><em>優しく丁寧</em>な指導</b>
						</span>
					</a></li>
					<li><a href="#anc02">
						<span>
							<i>02</i>
							<b>無料で便利！<em>自宅送迎バス</em></b>
						</span>
					</a></li>
					<li><a href="#anc03">
						<span>
							<i>03</i>
							<b><em>自分の都合に合わせて</em> <br>予定を組みやすい</b>
						</span>
					</a></li>
					<li><a href="#anc04">
						<span>
							<i>04</i>
							<b><em>女性に嬉しい</em> 環境づくり</b>
						</span>
					</a></li>
					<li><a href="#anc05">
						<span>
							<i>05</i>
							<b>高速教習車があの<em>“レクサス?!”</em></b>
						</span>
					</a></li>
					<li><a href="#anc06">
						<span>
							<i>06</i>
							<b><em>周辺にお店が多く</em> て <br>空き時間も充実</b>
						</span>
					</a></li>
				</ul>
			</div>

			<div class="box rbox_02">
				<div class="wrap_nb">
					<div class="nb" id="anc01">
						<span>
							<label>reason</label>01
						</span>
					</div>
					<h3 class="mHead"><span>優しく丁寧な指導</span></h3>
				</div>
				<div class="wrap_two_col">
					<div class="imgs">
						<img src="<?php echo APP_ASSETS_IMG;?>reason/img01@2x.jpg" alt="優しく丁寧な指導">
					</div>
					<div class="txt">
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>

						<p>仮）おもてなしサービス、最後まで面倒を見てくれる、ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
					</div>
				</div>
				<div class="container-1000">
					<div class="box_quote">
						<div class="box_quote_wrap">
							<div class="animal_text">
								<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="90" height="142" alt=""></i>
								<span>ここが安心！</span>
							</div>
							<p class="txt_big">インストラクターの指名/拒否の希望があれば受け付けます</p>
							<p>仮）インストラクターの指導に自信があるからこそ…ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
							<div class="btn_inside_border"><a href="javascript:void(0)">スタッフ紹介</a></div>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="box rbox_03">
				<div class="wrap_nb">
					<div class="nb" id="anc02">
						<span>
							<label>reason</label>02
						</span>
					</div>
					<h3 class="mHead"><span>無料で便利！<br class="sp">自宅送迎バス</span></h3>
				</div>
				<div class="wrap_two_col">
					<div class="imgs">
						<img src="<?php echo APP_ASSETS_IMG;?>reason/img02@2x.jpg" alt="無料で便利！自宅送迎バス">
					</div>
					<div class="txt">
						<p>普通免許取得コースの方は、自宅から当校までの個別送迎をご利用いただけます。（片道約25分以内のエリアが対象）</p>
						<p>前日までに予約すれば、雨の日や、夜間教習で帰りが暗くなったり寒かったりしても、ラクラク通学できるので安心です。<br>
						また、自宅送迎のWeb予約システムにご登録いただくと、どこでも手軽に予約可能。ぜひこの便利なサービスをご利用ください！</p>
						<p class="note">※普通免許以外の方は、一宮駅と当校間のシャトルバスのみご利用いただけます。</p>
						<div class="btn_inside_border"><a href="javascript:void(0)">自宅送迎・シャトルバスについて</a></div>
					</div>
				</div>
			</div>

			<div class="box rbox_04">
				<div class="wrap_nb">
					<div class="nb" id="anc03">
						<span>
							<label>reason</label>03
						</span>
					</div>
					<h3 class="mHead"><span>自分の都合に合わせて<br class="sp">予定を組みやすい</span></h3>
				</div>
				<div class="wrap_two_col">
					<div class="imgs">
						<img src="<?php echo APP_ASSETS_IMG;?>reason/img03@2x.jpg" alt="自分の都合に合わせて予定を組みやすい">
					</div>
					<div class="txt">
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
					</div>
				</div>
				<div class="container-1000">
					<h4 class="sHead"><span>土日のみの通学で卒業可能！</span></h4>
					<div class="container-800 mta">
						<p>仮）学校や仕事で忙しく、平日にはなかなか通学できないという方でも大丈夫。…ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
					</div>
					<div class="box_quote">
						<div class="box_quote_wrap">
							<div class="animal_text">
								<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="90" height="142" alt=""></i>
								<span>一宮市では登校だけ！</span>
							</div>
							<p class="txt_big">仮免許試験を日曜日にも実施しています</p>
							<p>仮）日曜日に仮免許試験を行なっているのは、一宮市の自動車学校では尾西自動車学校だけ…ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
						</div>
					</div>

					<h4 class="sHead"><span>夜間講習を活用して時間を有効利用</span></h4>
					<div class="container-800 mtb">
						<div class="right">
							仮）できるだけ短期間で卒業を目指したいけど、平日は授業やお仕事の予定が入っていることも多いですよね。そんな方には、夜間講習がおすすめ。<br>
尾西自動車学校の営業時間は10:00-20:50と、夜遅くまで営業しているため、授業や仕事の後に少し空いた時間を使って教習を受けていただくこともできます。
						</div>
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>reason/img03_a@2x.jpg" alt="">
						</div>
					</div>

					<h4 class="sHead"><span>スケジュール作成の手厚いサポート</span></h4>
					<div class="container-800 mtc">
						<p>仮）できるだけ短期間で卒業を目指したいけど、平日は授業やお仕事の予定が入っていることも多いですよね。そんな方には、夜間講習がおすすめ。<br>
尾西自動車学校の営業時間は10:00-20:50と、夜遅くまで営業しているため、授業や仕事の後に少し空いた時間を使って教習を受けていただくこともできます。</p>
						<div class="btn_inside_border"><a href="javascript:void(0)">プランの詳細について</a></div>
					</div>
				</div>
			</div>

			<div class="box rbox_05">
				<div class="wrap_nb">
					<div class="nb" id="anc04">
						<span>
							<label>reason</label>04
						</span>
					</div>
					<h3 class="mHead"><span>女性に嬉しい環境づくり</span></h3>
				</div>
				<div class="wrap_two_col">
					<div class="imgs">
						<img src="<?php echo APP_ASSETS_IMG;?>reason/img04@2x.jpg" alt="女性に嬉しい環境づくり">
					</div>
					<div class="txt">
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
					</div>
				</div>
				<div class="banner_adv">
					<img class="pc" src="<?php echo APP_ASSETS_IMG;?>reason/img_adv@2x.png" alt="">
					<img class="sp" src="<?php echo APP_ASSETS_IMG;?>reason/img_adv_sp@2x.png" alt="">
				</div>
				<div class="btn_inside_border"><a href="javascript:void(0)">詳しくはこちら</a></div>
			</div>

			<div class="box rbox_06">
				<div class="wrap_nb">
					<div class="nb" id="anc05">
						<span>
							<label>reason</label>05
						</span>
					</div>
					<h3 class="mHead"><span>教習車が<br class="sp">あの”レクサス?!”</span></h3>
				</div>
				<div class="wrap_two_col">
					<div class="imgs">
						<img src="<?php echo APP_ASSETS_IMG;?>reason/img05@2x.jpg" alt="教習車があの”レクサス?!”">
					</div>
					<div class="txt">
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
					</div>
				</div>
			</div>
			<div class="box rbox_07">
				<div class="wrap_nb">
					<div class="nb" id="anc06">
						<span>
							<label>reason</label>06
						</span>
					</div>
					<h3 class="mHead"><span>周辺にお店が多くて、<br class="sp">空き時間も充実</span></h3>
				</div>
				<div class="wrap_two_col">
					<div class="imgs">
						<img src="<?php echo APP_ASSETS_IMG;?>reason/img06@2x.jpg" alt="周辺にお店が多くて、空き時間も充実">
					</div>
					<div class="txt">
						<p>仮）ここに尾西自動車学校が選ばれる理由の、文章が入ります、ここに尾西自動車学校が選ばれる理由の、文章が入ります。ここに尾西自動車学校が選ばれる理由の、文章が入ります。</p>
					</div>
				</div>
				<div class="container-1000">
					<div class="list_content">
						<div class="container-800">
							<div class="title"><span>徒歩５分圏内にたくさんの飲食店！</span></div>
							<ul>
								<li class="circle">マクドナルド</li>
								<li class="circle">ケンタッキーフライドチキン</li>
								<li class="circle">シェルボン（喫茶店）</li>
								<li class="circle">スシロー</li>
								<li class="circle">ブロンコビリー</li>
								<li class="circle">サガミ</li>
							</ul>
							<div class="title"><span>コンビニ、100円ショップ、家電量販店、アミューズメント施設も！</span></div>
							<ul>
								<li class="circle">ファミリーマート</li>
								<li class="circle">ダイソー</li>
								<li class="circle">ジョーシン</li>
								<li class="circle">アソビックスびさい</li>
							</ul>
						</div>
					</div>
					<div class="btn_inside_border"><a href="javascript:void(0)">周辺地図はこちら</a></div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
</body>
</html>