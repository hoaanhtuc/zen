<?php
// Author: A+LIVE
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>
<body id="faq" class='faq'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>

<h3 class="mHead"><span>料金やお支払いについて</span></h3>
<h4 class="sHead"><span>自宅送迎バス</span></h4>
<a href="#" class="button"><span>スタッフ紹介</span></a>


<ul class="anchor clearfix">
	<li><a href="#"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_1.svg" alt=""></i><b class="txt"><em>企業向け</em> 研修コース</b></span></a></li>
	<li><a href="#"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_2.svg" alt=""></i><b class="txt">企業向け 研修コース</b></span></a></li>
	<li><a href="#"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_3.svg" alt=""></i><b class="txt">企業向け 研修コース</b></span></a></li>
	<li><a href="#"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_4.svg" alt=""></i><b class="txt">企業向け 研修コース</b></span></a></li>
	<li><a href="#"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_5.svg" alt=""></i><b class="txt">企業向け 研修コース</b></span></a></li>
	<li><a href="#"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_6.svg" alt=""></i><b class="txt">企業向け 研修コース</b></span></a></li>
</ul>
<ul class="anchor anchor_txt clearfix">
	<li><a href="#"><span><b class="txt">料金やお支払いについて</b></span></a></li>
	<li><a href="#"><span><b class="txt">免許や制度について</b></span></a></li>
	<li><a href="#"><span><b class="txt">尾西自動車学校について</b></span></a></li>
</ul>

<p>icon svg</p>
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_1.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_2.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_3.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_4.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_5.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_6.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_7.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_8.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_9.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_10.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_11.png" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_12.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_13.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_14.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_15.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_16.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_17.svg" alt="">
<img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_18.svg" alt="">

<p>breadcum</p>
<ul class="breadCum">
	<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
	<li>よくあるご質問</li>
</ul>
<p>Heading</p>
<h2 class="bHead"><span><i>よくあるご質問</i><b>FAQ</b></span></h2>

<p>pagenavi</p>

				<div class="wp-pagenavi">
					<a class="previouspostslink" rel="prev" href="#">PREV</a><span class="current">1</span><span class="pages">/ 6 ページ</span><a class="nextpostslink" rel="next" href="">NEXT</a>
					</div>


				<p>faq block</p>

				<ul class="listTxt listFaq">
					<li>
						<h3><span>仮）卒業するまでにどのくらいの期間がかかりますか？</span></h3>
						<div class="desc">短期集中コースであれば、3週間ぐらいで卒業可能ですが、普通コースですと入校される時期にもよりますが、1ヶ月から2ヶ月程度が目安となります。</div>
					</li>
					<li>
						<h3><span>仮）卒業するまでにどのくらいの期間がかかりますか？</span></h3>
						<div class="desc">短期集中コースであれば、3週間ぐらいで卒業可能ですが、普通コースですと入校される時期にもよりますが、1ヶ月から2ヶ月程度が目安となります。</div>
					</li>
					<li>
						<h3><span>仮）卒業するまでにどのくらいの期間がかかりますか？</span></h3>
						<div class="desc">短期集中コースであれば、3週間ぐらいで卒業可能ですが、普通コースですと入校される時期にもよりますが、1ヶ月から2ヶ月程度が目安となります。</div>
					</li>
				</ul>
				<p class="btn01"><span>一般料金よりも4万円以上もお得！</span></p>

				<p>List anchor 2</p>
				<ul class="anchor2 clearfix">
					<li><a href=""><span>概要</span></a></li>
					<li><a href=""><span>カリキュラム</span></a></li>
					<li><a href=""><span>入校申し込み</span></a></li>
					<li><a href=""><span>お支払いについて</span></a></li>
				</ul>

<p>table 1</p>
								<table class="tablePrice">
									<tr>
										<th>所持免許</th>
										<td>料金(税込み)</td>
									</tr>
									<tr>
										<th>免許なし</th>
										<td>296,400円<br>
<span>(320,112円)</span></td>
									</tr>
									<tr>
										<th>原付免許</th>
										<td>290,400円<br>
<span>（313,632円）</span></td>
									</tr>
									<tr>
										<th>自二免許</th>
										<td>209,400円<br>
<span>(226,152円)</span></td>
									</tr>
								</table>


								<p>table 2</p>

								<table class="tableStyle">
									<tr>
										<th colspan="2">例：ベーシックプラン</th>
									</tr>
									<tr>
										<td>座学 ＋ 適正検査 ＋ 運転技術診断 (3時間)</td>
										<td>000000円　※00名様参加の場合</td>
									</tr>
								</table>


								<p>table 3</p>

								<table class="tableStyle">
									<tr>
										<td>講習00分（一律）</td>
										<td>5,184円</td>
									</tr>
								</table>
								<p>img hover</p>
								<span class="img-hover"><img src="<?php echo APP_ASSETS; ?>img/classes/img_photo1.png" alt=""></span>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/footer.php'); ?>

<script type="text/javascript">
$(window).bind("load", function() { 
	$('.listTxt li h3').click(function(){
		$(this).next().slideToggle('fast');
		$(this).toggleClass('active');
	})
});
</script>
</body>
</html>