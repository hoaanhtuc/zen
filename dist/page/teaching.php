<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../');
include_once($path.'/app_config.php');
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="standard" class='teaching'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h2 class="bHead"><span><i>各種講習</i><b>TEACHING</b></span></h2>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li><a href="<?php echo APP_URL_CLASSES; ?>">教習案内</a></li>
				<li>各種講習</li>
			</ul>
			
			<div class="container-1000 tbox_01">
				<ul class="anchor clearfix">
					<li><a href="#anc01"><span><i class="ico"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_3.svg" width="52" height="26" alt=""></i><b><em>企業向け</em> 研修コース</b></span></a></li>
					<li><a href="#anc02"><span><i class="ico"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_4.svg" width="54" height="32" alt=""></i><b><em>学校様向け</em> 教育コース</b></span></a></li>
					<li><a href="#anc03"><span><i class="ico"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_5.svg" width="44" height="48" alt=""></i><b>高齢者講習</b></span></a></li>
					<li><a href="#anc04"><span><i class="ico"><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_6.svg" width="31" height="45" alt=""></i><b>ペーパードライバー講習</b></span></a></li>
				</ul>
			</div>
			
			<div class="box">
				<h3 id="anc01" class="mHead"><span>企業様向け 研修コース</span></h3>
				<div class="container-1000 tbox_02">
					<div class="container-800">
						<div class="box_text_circle">
							<div class="animal_text">
								<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="82" height="130" alt=""></i>
								<span>このような不安の声や、<br>お困りごとはありませんか？</span>
							</div>
							<ul>
								<li class="circle">よくぶつけてしまう</li>
								<li class="circle">市街地の運転が怖い、自信がない</li>
								<li class="circle">車庫入れが苦手、下手</li>
								<li class="circle">高速道路に慣れていない</li>
								<li class="circle">運転が荒い、慎重さに欠けている</li>
								<li class="circle">よく違反、事故を起こしてしまう</li>
								<li class="circle">同乗していて安心できない</li>
							</ul>
							<p class="txt">運転スキルの不足・安全運転意識の低下によって、時には大きな事故につながる可能性もあります。</p>
						</div>
					</div>
				</div>

				<div class="container-1000 tbox_03">
					<div class="container-800">
						<div class="box_point">
							<p class="box_point_title"><span>大きな事故</span>につながる前に</p>
							<p>尾西自動車学校では、業務において車を使う企業様に向けて、ドライバーの安全運転意識や技術を向上させることを目指した講習を行っています。</p>
							<div class="box_point_quote">
								<p class="txt_up">当校の講習ポイント！</p><span>point!</span>
								<p>尾西自動車学校では、業務において車を使う企業様に向けて、ドライバーの安全運転意識や技術を向上させることを目指した講習を行っています。</p>
							</div>
						</div>
					</div>
				</div>

				<div class="container-1000 tbox_04">
					<div class="container-800">
						<div class="box_point">
							<p class="box_point_title">企業の責任として<br class="sp"><span>従業員の安全運転教育</span>を</p>
							
							<div class="right">
								<p>業務中に交通事故が発生すると、金銭的コストの損失・人的損害、社会的なイメージダウンなど、企業にとって様々な損害を与えます。企業にとって大切な従業員の安全、そして社会的信頼を守るために、社内のドライバーを対象に交通安全教育を行いませんか？</p>
							</div>
							<div class="left">
								<img src="<?php echo APP_ASSETS_IMG;?>teaching/img01@2x.jpg" alt="">
							</div>
						</div>
					</div>
				</div>

				<h4 class="sHead"><span>対象車種</span></h4>
				<div class="container-800 tbox_00">
					<p>普通自動車/原動機付自転車</p>
				</div>

				<h4 class="sHead"><span>カリキュラム例</span></h4>
				<div class="container-800 tbox_05">
					<p>お客様によくご利用いただいているコースをご紹介します。</p>
					<p>
						<img class="pc" src="<?php echo APP_ASSETS_IMG;?>teaching/txt_01.svg" alt="">
						<img class="sp" src="<?php echo APP_ASSETS_IMG;?>teaching/txt_01_sp@2x.png" alt="">
					</p>
					<p class="pc">こちら以外にも、お客様のご要望をお聞きして受講時間や予算に応じたカリキュラムを作成することが可能です。お気軽にご相談ください。</p>
				</div>

				<h4 class="sHead"><span>料金・お問い合わせ</span></h4>
				<div class="container-800 tbox_06">
					<p class="sp">カリキュラムによって料金は変動しますが、お客様からご要望の多いカリキュラムの料金ご参考にしてください。</p>
					<table class="tableStyle">
						<tbody>
							<tr>
								<th colspan="2">例：ベーシックプラン</th>
							</tr>
							<tr>
								<td>座学 ＋ 適正検査 ＋ 運転技術診断 (3時間)</td>
								<td>000000円　※00名様参加の場合</td>
							</tr>
						</tbody>
					</table>
					<p>お客様のご要望をお聞きして受講時間や予算に応じたカリキュラムを作成することも可能ですので、まずはお問い合わせください。</p>
					<div class="btn_inside_yellow">
						<a href="javascript:void(0)">お問い合わせフォームへ</a>
					</div>
				</div>

			</div>

			<div class="box">
				<h3 id="anc02" class="mHead"><span>学校様向け 教育コース</span></h3>
				<div class="container-800 tbox_07">
					<div class="box_point">
						<p class="box_point_title"><span>子どもたちの安全を守り</span><br class="sp">地域に笑顔を増やしましょう</p>
						
						<div class="right">
							<p>当校のインストラクターが学校にご訪問し、「自転車の安全な乗り方」や「自転車マナー」を実技やビデオ上映などで、交通ルールについて学習します。</p>
						</div>
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>teaching/img02@2x.jpg" alt="">
						</div>
					</div>
					<div class="box_text_circle">
						<div class="animal_text">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="82" height="130" alt=""></i>
							<span>交通ルールの<br class="sp">認識の甘さによって<br>大きな事故を招く可能性が<br class="sp">あります</span>
						</div>
						<ul>
							<li class="circle">信号無視などのルール違反</li>
							<li class="circle">自転車通学の生徒同士の並進</li>
							<li class="circle">ヘッドホンをしながら通学</li>
							<li class="circle">自転車の傘差し運転</li>
							<li class="circle">自転車運転中の携帯電話使用</li>
							<li class="circle">夜間の通学にライトをつけない</li>
							<li class="circle">悪路や狭い道幅などの通学路</li>
						</ul>
						<p class="txt">仮）ここに補足の文章が入ります、ここに補足の文章が入ります。ここに補足の文章が入ります、ここに補足の文章が入ります。</p>
					</div>
				</div>

				<h4 class="sHead"><span>対象</span></h4>
				<div class="container-800 tbox_08">
					<ul class="default-col">
						<li class="circle">幼稚園児</li>
						<li class="circle">小学生（低学年）</li>
						<li class="circle">小学生（高学年）</li>
						<li class="circle">中学生</li>
						<li class="circle">高校生</li>
					</ul>
				</div>

				<h4 class="sHead"><span>カリキュラム例</span></h4>
				<div class="container-800 tbox_09">
					<p>対象の学年や目的に応じて、適したカリキュラムを組んで講習を行います。</p>
					<div class="col">
						<div class="col_half">
							<div class="col_head">
								<span>例：対象　幼稚園児</span>
							</div>
							<div class="col_body">
								<div class="imgs"><img src="<?php echo APP_ASSETS_IMG;?>teaching/img03@2x.jpg" alt=""></div>
								<div class="text"><p>サイっちと一緒に、交通ルールの基本を楽しく学びます！<br>（30分程度）</p></div>
							</div>
						</div>
						<div class="col_half">
							<div class="col_head">
								<span>例：対象　中学生</span>
							</div>
							<div class="col_body">
								<div class="imgs"><img src="<?php echo APP_ASSETS_IMG;?>teaching/img04@2x.jpg" alt=""></div>
								<div class="text"><p>自転車の安全な乗り方や交通マナーを、実技・ビデオ上映などで学びます。<br>（40〜50分程度）</p></div>
							</div>
						</div>
					</div>
					<p class="pc">こちら以外にも、お客様のご要望をお聞きして受講時間や予算に応じたカリキュラムを作成することが可能です。お気軽にご相談ください。</p>
				</div>

				<h4 class="sHead"><span>料金・お問い合わせ</span></h4>
				<div class="container-800 tbox_10">
					<table class="tableStyle">
						<tbody><tr>
							<th colspan="2">例：幼稚園児対象カリキュラム</th>
						</tr>
						<tr>
							<td>サイっちの着ぐるみと楽しく学ぶ (30分)</td>
							<td>000000円　※00名様参加の場合</td>
						</tr>
					</tbody></table>
					<table class="tableStyle">
						<tbody><tr>
							<th colspan="2">例：中学生対象カリキュラム</th>
						</tr>
						<tr>
							<td>自転車のルールとマナー<br class="sp">【映像＋講話】 (40-50分)</td>
							<td>000000円　※00名様参加の場合</td>
						</tr>
					</tbody></table>
					<p>お客様のご要望をお聞きして受講時間や予算に応じたカリキュラムを作成することも可能ですので、まずはお問い合わせください。</p>
					<div class="btn_inside_yellow">
						<a href="javascript:void(0)">お問い合わせフォームへ</a>
					</div>
				</div>
			</div>

			<div class="box">
				<h3 id="anc03" class="mHead"><span>高齢者講習</span></h3>
				<div class="container-800 tbox_11">
					<div class="box_point">
						<p class="box_point_title">いつまでも楽しく<br class="sp"><span>安全に運転</span>していただくために</span>を</p>
						
						
						<div class="right">
							<p>運転免許証の有効期限が切れる日に年齢が70歳以上になる方が運転免許証の更新（書き換え）をするために必要な講習です。<br><br>座学・運転適性・運転実技（小型特殊免許のみの方は除く）の講習で、その結果に基づいて担当員が助言・指導をさせていただきます。</p>
						</div>
						<div class="animal_text">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="82" height="130" alt=""></i>
							<span>試験ではありませんので、<br>安心して受講してください!</span>
						</div>
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>teaching/img05@2x.jpg" alt="">
						</div>
					</div>
				</div>
				<h4 class="sHead"><span>対象</span></h4>
				<div class="container-800 tbox_12">
					<p>運転免許証の有効期限満了日の年齢が70歳以上で、免許の更新を希望される方</p>
				</div>

				<h4 class="sHead"><span>講習実施日</span></h4>
				<div class="container-800 tbox_13">
					<p>毎週　月曜日～金曜日<br><br>※なお、時季により実施日が変更になる場合がありますのでご了承下さい。</p>
					<div class="box_quote">
						<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_warning.svg" width="60" height="123" alt=""></i>
						<p class="cl_red01">高齢者講習の受講希望者が多く、<br class="sp">大変予約が取りにくくなっております。</p>
						<p>警察から通知のハガキが届いたら、お早めに予約していただくことをおすすめします。</p>
					</div>
				</div>

				<h4 class="sHead"><span>講習の流れ</span></h4>
				<div class="container-800 tbox_14">
					<p>
						<img class="pc" src="<?php echo APP_ASSETS_IMG;?>teaching/txt_02.svg" width="" height="" alt="">
						<img class="sp" src="<?php echo APP_ASSETS_IMG;?>teaching/txt_02_sp@2x.png" width="sp" height="" alt="">
					</p>
				</div>

				<h4 class="sHead"><span>当日必要なもの</span></h4>
				<div class="container-800 tbox_15">
					<ul class="default-col">
						<li class="circle">運転免許証</li>
						<li class="circle">高齢者講習の受講ハガキ</li>
						<li class="circle">講習料金<br>
							<span class="pc">認知機能検査：750円<br>
							講習（合理化）：5,000円<br>
							講習（高度化）：7,900円</span>
							</li>
					</ul>
				</div>

				<h4 class="sHead"><span>料金</span></h4>
				<div class="container-800 tbox_16">
					<table class="tableStyle">
						<tbody><tr>
							<th colspan="2">75歳未満の方</th>
						</tr>
						<tr>
							<td>講習（2時間）</td>
							<td>5,100円</td>
						</tr>
					</tbody></table>
					<p>全ての方に２時間の講習を受けていただきます。</p>
					<table class="tableStyle">
						<tbody><tr>
							<th colspan="2">75歳以上の方</th>
						</tr>
						<tr>
							<td>認知機能検査</td>
							<td>750円</td>
						</tr>
						<tr>
							<td>講習（2時間）</td>
							<td>5,100円</td>
						</tr>
						<tr>
							<td>講習（3時間）</td>
							<td>7,950円</td>
						</tr>
					</tbody></table>
				</div>
			</div>

			<div class="box">
				<h3 id="anc04" class="mHead"><span>ペーパードライバー講習</span></h3>
				<div class="container-800 tbox_17">
					<div class="box_point">
						<p class="box_point_title">ブランクが長く、<br class="sp"><span>運転スキルや交通ルールに<br class="sp">自信のない</span>方へ</p>
						<p class="sp">尾西自動車学校では、ペーパードライバー講習を行っています。</p>
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>teaching/img06@2x.jpg" alt="">
						</div>
						<div class="right pc">
							<p>尾西自動車学校では、ペーパードライバー講習を行っています。<br>
車を運転しなければならない状況になった方、生活を便利にしたい方、趣味を充実させたい方。それぞれに、ペーパードライバーを卒業したい目的やご事情があるはず。<br>
お客様の不安な点をしっかりとお伺いし、より安全で充実したカーライフを送っていただけるよう、サポートいたします。<br>
些細なお悩みでも、お気軽にご相談ください。</p>
						</div>
					</div>
					<div class="box_text_circle">
						<div class="animal_text">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="82" height="130" alt=""></i>
							<span>一人ひとりの事情に合わせ<br>実践的な指導をします！</span>
						</div>
						<ul>
							<li class="circle">家族が増えたので車を買う予定。10年以上運転していないので、子どものためにも、ちゃんと運転できるようになりたい。</li>
							<li class="circle">駐車や車庫入れが苦手で、何度かぶつけてしまったことがある…</li>
						</ul>
					</div>
					<p class="sp">
車を運転しなければならない状況になった方、生活を便利にしたい方、趣味を充実させたい方。それぞれに、ペーパードライバーを卒業したい目的やご事情があるはず。<br>
お客様の不安な点をしっかりとお伺いし、より安全で充実したカーライフを送っていただけるよう、サポートいたします。<br>
些細なお悩みでも、お気軽にご相談ください。</p>
				</div>

				<h4 class="sHead"><span>対象</span></h4>
				<div class="container-800 tbox_18">
					<ul class="two-col">
						<li class="circle">運転に自信のない方</li>
						<li class="circle">運転の仕方を忘れてしまった方</li>
					</ul>
					<p>など、運転免許証を取得していればどなたでも受講していただけます。</p>
					<p class="cl_red01">ご注意</p>
					<p>※試験場へ行く前の講習は受けられません。<br>
						※高速講習は受けられません。 <br>
						※持ち込み車両での練習は受けられません。<br>
					</p>
				</div>

				<h4 class="sHead"><span>講習実施日</span></h4>
				<div class="container-800 tbox_19">
					<p>毎週　月曜日～金曜日<br class="sp">（2〜3月及び8〜9月を除く）</p>
				</div>

				<h4 class="sHead"><span>予約日</span></h4>
				<div class="container-800 tbox_20">
					<p>当日の電話予約のみ受け付けております。<br><br>※時期・時間帯により予約できない場合もあります。一般教習生の在校状況により予約ができない場合があります。 </p>
				</div>

				<h4 class="sHead"><span>当日必要になるもの</span></h4>
				<div class="container-800 tbox_21">
					<ul class="two-col">
						<li class="circle">運転免許証</li>
						<li class="circle">講習料金（一律5,184円）</li>
					</ul>
				</div>

				<h4 class="sHead"><span>料金</span></h4>
				<div class="container-800 tbox_22">
					<table class="tableStyle">
						<tbody>
						<tr>
							<td>講習00分（一律）</td>
							<td>5,184円</td>
						</tr>
					</tbody></table>
				</div>

			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
</body>
</html>