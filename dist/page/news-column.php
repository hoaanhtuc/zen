<?php
// Author: A+LIVE
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>
<body id="blog" class='blog'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h2 class="bHead"><span><i>ブログ</i><b>BLOG</b></span></h2>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li>ブログ</li>
			</ul>

			<div class="blogBlock clearfix">
				<ul class="clearfix blogList">
					<li>
						<a href="#">
							<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""><span class="iconNew"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_new.svg" alt=""></span></p>
							<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
							<strong>ブログのタイトルブログの、タイトルブログのタイトル！</strong></h3>
						</a>
					</li>
					<li>
						<a href="#">
							<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""></p>
							<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
							<strong>ブログのタイトルブログのタイトルブログのタイトルブログのタイトル！</strong></h3>
						</a>
					</li>
					<li>
						<a href="#">
							<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""></p>
							<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
							<strong>ブログのタイトルブログのタイトルブログのタイトルブログのタイトル！</strong></h3>
						</a>
					</li>
					<li>
						<a href="#">
							<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""></p>
							<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
							<strong>ブログのタイトルブログのタイトルブログのタイトルブログのタイトル！</strong></h3>
						</a>
					</li>
					<li>
						<a href="#">
							<p class="img"><img src="<?php echo APP_ASSETS; ?>img/blog/img_thumb.jpg" alt=""></p>
							<h3><span class="date"><i>2018.00.00</i><b>コラム</b></span>
							<strong>ブログのタイトルブログのタイトルブログのタイトルブログのタイトル！</strong></h3>
						</a>
					</li>
				</ul>

				<div class="wp-pagenavi">
					<a class="previouspostslink" rel="prev" href="#">PREV</a><span class="current">1</span><span class="pages">/ 6 ページ</span><a class="nextpostslink" rel="next" href="">NEXT</a>
					</div>

				<div class="catBlock">
					<div class="inner">
					<h3>CATEGORY</h3>
					<ul>
						<li><a href="">カテゴリ１が入ります ( 1 )</a></li>
						<li><a href="">カテゴリ2 ( 1 )</a></li>
						<li><a href="">カテゴリ3 ( 1 )</a></li>
						<li><a href="">カテゴリ4 ( 1 )</a></li>
					</ul>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/footer.php'); ?>

<script src="<?php echo APP_ASSETS; ?>js/lib/jquery.matchHeight.min.js"></script>
<script type="text/javascript">
$(window).bind("load", function() { 
	$('.blogList li a').matchHeight();
});
</script>
</body>
</html>