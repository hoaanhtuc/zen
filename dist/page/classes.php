<?php
// Author: A+LIVE
include_once('../app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>
<body id="classes" class='classes'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h2 class="bHead"><span><i>教習案内</i><b>CLASSES</b></span></h2>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li>教習案内</li>
			</ul>
			<div class="classesBlock">
				<div class="txtIntro">
					<p>仮）ここに教習案内の導入文が入りますここに教習案内の導入文が入りますここに教習案内の導入文が入りますここに教習案内の導入文が入ります。</p>
				</div>
				<ul class="imgList clearfix">
					<li class="pink"><a href="<?php echo APP_URL; ?>standard/" class="img-hover">
						<span><img src="<?php echo APP_ASSETS; ?>img/classes/img_photo1.png" alt=""></span>
						<strong><b>普通車</b></strong>
					</a></li>
					<li class="blue"><a href="<?php echo APP_URL; ?>motorbike/" class="img-hover">
						<span><img src="<?php echo APP_ASSETS; ?>img/classes/img_photo2.png" alt=""></span>
						<strong><b>二輪車</b></strong>
					</a></li>
				</ul>
				<p class="btnClass"><a href=""><span>普通車二種</span></a></p>
				<h3 class="mHead"><span>各種講習案内</span></h3>
				<div class="txtIntro">
					<p>仮）ここに教習案内の導入文が入りますここに教習案内の導入文が入りますここに教習案内の導入文が入りますここに教習案内の導入文が入ります。</p>
				</div>
				<div class="classesContent">
					<ul class="classItem clearfix">
						<li>
							<a href="" class="img-hover">
								<span class="img"><img src="<?php echo APP_ASSETS; ?>img/classes/img_thumb1.png" alt=""></span>
								<span class="ttl">
									<b><em>企業様向け</em>研修コース</b><span>会社の営業車を利用される方におすすめ</span>
								</span>
							</a>
						</li>
						<li>
							<a href="" class="img-hover">
								<span class="img"><img src="<?php echo APP_ASSETS; ?>img/classes/img_thumb2.png" alt=""></span>
								<span class="ttl">
									<b><em>学校様向け</em>教育コース</b><span>スタッフが学校を訪問して交通ルール講習を行います</span>
								</span>
							</a>
						</li>
						<li>
							<a href="" class="img-hover">
								<span class="img"><img src="<?php echo APP_ASSETS; ?>img/classes/img_thumb3.png" alt=""></span>
								<span class="ttl">
									<b>高齢者講習</b><span>70歳以上で運転免許証を<br>更新するのに受講が必要</span>
								</span>
							</a>
						</li>
						<li>
							<a href="" class="img-hover">
								<span class="img"><img src="<?php echo APP_ASSETS; ?>img/classes/img_thumb4.png" alt=""></span>
								<span class="ttl">
									<b>ペーパードライバー講習</b><span>運転に自信のない方のため<br>に丁寧に指導します</span>
								</span>
							</a>
						</li>
					</ul>
				</div>
				<h3 class="mHead"><span>インターネット学習</span></h3>
				<div class="classesContent">
					<p class="taC imgStyle"><img src="<?php echo APP_ASSETS; ?>img/classes/img_ttl.png" alt=""></p>
					<h4>24時間、<b>自宅のパソコンや<br class="sp">携帯電話でどこでも気軽に</b><br class="sp">アクセス可能！</h4>
					<div class="txtIntro">
						<ul class="listTxt">
							<li>問題は「模擬テスト形式」、「一問一答形式」の２つがあります。自分に合った問題で効率よく学習！</li>
							<li>「模擬テスト形式」は、仮免学科試験向けの「仮免前練習問題」効果測定や本免学科試験向けの「卒検前練習問題」「本免前練習問題」の３種類があります。実際の学科試験と同じ要領で出題されるので、実力をつけたり、実力を試すのに最適です。</li>
							<li>「一問一答形式」は、「カテゴリー別ランダム出題」と、 「教習項目別ポイント出題」、「弱点克服ピンポイント出題」の３種類があります。学科教習の復習や自分の苦手なポイントを集中的に学習することができます。</li>
						</ul>
					</div>
					<p class="taC btnStyle"><a href="https://mantensama.jp/bisai" target="_blank" class="button"><span>ログインはこちら</span></a></p>
				</div>
				<h3 class="mHead"><span>学科時間割</span></h3>
				<div class="classesContent">
					<div class="txtIntro">
						<p>普通自動車、自動二輪車の学科の時間割はこちらからご覧いただけます。</p>
					</div>
					<p class="taC btnStyle"><a href="#" class="button small"><span>学科時間割(PDFファイルが開きます)</span></a></p>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>