<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../');
include_once($path.'/app_config.php');
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="about" class='about'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h2 class="bHead"><span><i>学校案内/スタッフ紹介</i><b>ABOUT</b></span></h2>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li>学校案内/スタッフ紹介</li>
			</ul>
			
			<div class="container-1000">
				<ul class="anchor2 clearfix">
					<li><a href="#anc01"><span>施設紹介</span></a></li>
					<li><a href="#anc02"><span>スタッフ紹介</span></a></li>
					<li><a href="#anc03"><span>アクセス</span></a></li>
					<li><a href="#anc04"><span>学校概要</span></a></li>
				</ul>
			</div>
			<div class="box">
				<h3 id="anc01" class="mHead"><span>施設紹介</span></h3>
				<div class="container-1000 box_01">
					<div class="container-800">
						<p>仮）ここに学校紹介の文章が入ります、ここに学校紹介の文章が入ります。ここに学校紹介の文章が入ります、ここに学校紹介の文章が入ります。</p>
					</div>
					<ul class="list_introduction">
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img01@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>コース</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img02@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>受付</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img03@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>待合スペース</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img04@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>パウダールーム</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img05@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>教室</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img06@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>自習室</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="box blist_over">
				<h3 id="anc02" class="mHead"><span>スタッフ紹介</span></h3>

				<h4 class="sHead"><span>インストラクター</span></h4>
				<div class="container-1000 box_02">
					<div class="list_staff">
					<?php for($i=2;$i>0;$i--){ ?>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff01@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff02@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff03@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff04@2x.jpg" alt="">
								<figcaption class="woman">
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure <?php echo 2 == $i ? "class='mark'" : '';?>>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff05@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
					<?php } ?>
					</div>
				</div>

				<h4 class="sHead"><span>フロント</span></h4>
				<div class="container-1000 box_03">
					<div class="list_staff">
					<?php for($i=1;$i>0;$i--){ ?>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff01@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff02@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff03@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff04@2x.jpg" alt="">
								<figcaption class="woman">
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff05@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
					<?php } ?>
					</div>
				</div>

				<h4 class="sHead"><span>広報</span></h4>
				<div class="container-1000 box_04">
					<div class="list_staff">
					<?php for($i=1;$i>0;$i--){ ?>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff01@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff02@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff03@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff04@2x.jpg" alt="">
								<figcaption class="woman">
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
						<figure>
							<a href="javascript:void(0)">
								<img src="<?php echo APP_ASSETS_IMG;?>about/img_staff05@2x.jpg" alt="">
								<figcaption>
									<span>名字 名前</span>
									メッセージが入りますメッセージが入ります
								</figcaption>
							</a>
						</figure>
					<?php } ?>
					</div>
				</div>
				<div class="see_more">
					<div class="btn_inside_border">
						<a href="javascript:void(0)" onclick="toggle_content(this);">もっと見る</a>
					</div>
				</div>
			</div>
			<div class="box">
				<h3 id="anc03" class="mHead"><span>アクセス</span></h3>
				<div class="container-1000 box_05">
					<div class="ggmap">
						<div class="ggmap_iframe">
							<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3255.8719594213276!2d136.7703945510198!3d35.30916285779726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!3m2!1sen!2s!4v1538108526217" width="1000" height="370" frameborder="0" style="border:0" allowfullscreen></iframe> -->
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3255.8719594213276!2d136.7703945510198!3d35.30916285779726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6003a6c3d030c689%3A0x3245ccc84abd01d9!2s3+Chome-12-45+Kagoya%2C+Ichinomiya-shi%2C+Aichi-ken+494-0002%2C+Japan!5e0!3m2!1sen!2s!4v1538108526217" width="1000" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<p class="address">〒494-0002　<br class="sp">愛知県一宮市篭屋３丁目12-45</p>
						<p class="gglink"><a href="https://goo.gl/maps/RkzqTdxGTF22" target="_blank">大きな地図で見る</a></p>
						<p class="animal"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="78" height="123" alt=""></i></p>
					</div>
				</div>

				<h4 class="sHead"><span>自宅送迎バス</span></h4>
				<div class="container-800 box_06">
					<div class="box_point">
						<p class="box_point_title"><span>無料で便利！<br class="sp"></span>自宅までの送迎で通学も安心</p>
						<div class="left pc">
							<img src="<?php echo APP_ASSETS_IMG;?>/about/img07@2x.jpg" alt="">
						</div>
						<div class="right">
							<p>仮）普通免許取得コースの方は、自宅から当校までの個別送迎をご利用いただけます。（片道約25分以内のエリアが対象）<br>
								<img class="sp" src="<?php echo APP_ASSETS_IMG;?>/about/img07@2x.jpg" alt="">
前日までに予約すれば、雨の日や、夜間教習で帰りが暗くなったり寒かったりしても、ラクラク通学できるので安心です。<br>
また、自宅送迎のWeb予約システムにご登録いただくと、どこでも手軽に予約可能。ぜひこの便利なサービスをご利用ください！</p>
<p class="note">※普通免許以外の方は、一宮駅と当校間のシャトルバスのみご利用いただけます。</p>
						</div>
					</div>
					<div class="sp tranform">
						<h4 class="sHead"><span>当校 ⇆ 駅のシャトルバス</span></h4>
						<div class="box_point">
							<p class="box_point_title"><span>JR尾張一宮駅・名鉄一宮駅から</span>のスクールバスをご利用いただけます</p>
						</div>
					</div>
					<div class="box_table_time">
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>/about/img08@2x.jpg" alt="">
						</div>
						<div class="right">
							<p class="title">シャトルバス時刻表</p>
							<ul class="list_time">
								<li>
									<p class="list_time_head">
										一宮駅北口発
									</p>
									<p>9:20</p>
									<p>10:40</p>
									<p>11:40</p>
									<p>13:20</p>
									<p>14:20</p>
									<p>15:20</p>
									<p>16:40</p>
									<p>17:40</p>
									<p>18:40</p>
									<p>19:40</p>
									<p>-</p>
								</li>
								<li>
									<p class="list_time_head">
										自動車学校着
									</p>
									<p>9:30</p>
									<p>10:50</p>
									<p>11:50</p>
									<p>13:30</p>
									<p>14:30</p>
									<p>15:30</p>
									<p>16:50</p>
									<p>17:50</p>
									<p>18:50</p>
									<p>19:50</p>
									<p>-</p>
								</li>
								<li>
									<p class="list_time_head">
										自動車学校発
									</p>
									<p>-</p>
									<p>11:00</p>
									<p>12:40</p>
									<p>13:40</p>
									<p>14:00</p>
									<p>16:00</p>
									<p>17:00</p>
									<p>18:00</p>
									<p>19:00</p>
									<p>20:00</p>
									<p>21:00</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="box">
				<h3 id="anc03" class="mHead"><span>学校概要</span></h3>
				<div class="container-800 box_07">
					<table class="tablePrice">
						<tbody>
							<tr>
								<th>学校名</th>
								<td>尾西自動車学校</td>
							</tr>
							<tr>
								<th>住所</th>
								<td>〒494-0002　<br class="sp">愛知県一宮市篭屋３丁目12-45</td>
							</tr>
							<tr>
								<th>TEL</th>
								<td>0586-45-2882</td>
							</tr>
							<tr>
								<th>FAX</th>
								<td>0586-45-9576</td>
							</tr>
							<tr>
								<th>WEB</th>
								<td>https://www.bisai.co.jp</td>
							</tr>
							<tr>
								<th>取得車種</th>
								<td>普通自動車、普通2種、普通自動二輪、大型自動二輪</td>
							</tr>
							<tr>
								<th>取扱講習</th>
								<td>ペーバードライバー講習、高齢者講習</td>
							</tr>
						</tbody>
					</table>
					<div class="icon_approve">
						<i><img src="<?php echo APP_ASSETS_IMG;?>about/ico_auto.svg" width="60" height="60" alt=""></i>
						<span>愛知県公安委員会指定</span>
					</div>
				</div>

				<h4 class="sHead"><span>入校資格</span></h4>
				<div class="container-800 box_08">
					<div class="list_entrance">
						<div class="list_entrance_line">
							<span>普通免許</span>
							<p>年齢が18歳以上の方（※誕生日の約1ヶ月前から入校可）</p>
						</div>
						<div class="list_entrance_line">
							<span>普通二種免許</span>
							<p>年齢が21歳以上の方で大型・普通・大型特殊のいずれかの免許を通算して3年以上受けている方。</p>
						</div>
						<div class="list_entrance_line">
							<span>自動二輪免許</span>
							<p>
								大型二輪　年齢が18歳以上の方 <br>
								普通二輪　年齢が16歳以上の方（※誕生日の約1ヶ月前から入校可）
							</p>
						</div>
						<div class="list_entrance_line">
							<span>共通</span>
							<p>
								視力が両眼で0.7以上、片眼0.3以上 <br>
								（左右）で信号機の色の識別が出来る方（眼鏡、コンタクトレンズ可）<br>
								※やさしい漢字が読めて理解できる方。<br>
								※普通二種については、視力が両眼で0.8以上、片眼0.5以上（左右）で深視力の検査において誤差値が20ミリ以内又、補聴器を使用しないで10ｍの距離で90ｄｂの警音器の音が聞こえること。
							</p>
						</div>
					</div>
					<div class="box_text_T01">
						<p class="box_text_T01--title">下記に該当する方は、事前にご相談ください</p>
						<ul>
							<li><p>聴力について障害をお持ちの方。</p></li>
							<li><p>運動能力で、身体に障害をお持ちの方。<br>事前に運転免許試験場（運転適性相談窓口）にて適性相談をお受け下さい。</p></li>
							<li><p>違反歴のある方、取消歴のある方。</p></li>
						</ul>
					</div>
				</div>

				<h4 class="sHead"><span>入校時に必要なもの</span></h4>
				<div class="container-800 box_09">
					<p class="circle">免許証のある方…現在所持している免許証 </p>
					<p class="circle">免許証のない方…本籍記載の住民票1通と身分証明書（健康保険証、パスポートまたは住基カードのうちいずれか1点）</p>
					<p class="circle">写真5枚（写真は無帽・無背景・横2.5×縦3cm上半身正面写真5枚）<br>※当校でも撮影できます。（別途料金）</p>
					<p class="circle">入校申込金　5,000円（税込5,400円）</p>
				</div>

				<h4 class="sHead"><span>入校申し込み受付</span></h4>
				<div class="container-800 box_10">
					<p class="circle">入校希望日の前日までにお申込みください。</p>
					<p class="circle">視力検査など簡単な検査を行いますので、ご本人がおいでください。</p>
				</div>

				<h4 class="sHead"><span>入校日カレンダー</span></h4>
				<div class="container-800">
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
	<script src="<?php echo APP_ASSETS_JS;?>about.min.js"></script>
</body>
</html>