<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../');
include_once($path.'/app_config.php');
include(APP_PATH.'libs/head.php');
?>
</head>
<body id="price" class='price'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php get_banner_page_common();?>
			<div class="priceBlock priceAnchor">
				<ul class="anchor clearfix">
					<li><a href="#anchor01"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_1.svg" alt=""></i><b class="txt">普通車教習</b></span></a></li>
					<li><a href="#anchor02"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_2.svg" alt=""></i><b class="txt">二輪車教習</b></span></a></li>
					<li><a href="#anchor03"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_3.svg" alt=""></i><b class="txt"><em>企業様向け</em> 研修コース</b></span></a></li>
					<li><a href="#anchor04"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_4.svg" alt=""></i><b class="txt"><em>学校様向け</em> 教育コース</b></span></a></li>
					<li><a href="#anchor05"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_5.svg" alt=""></i><b class="txt">高齢者講習</b></span></a></li>
					<li><a href="#anchor06"><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_6.svg" alt=""></i><b class="txt">ペーパードライバー講習</b></span></a></li>
				</ul>
			</div>

			<h3 class="mHead" id="anchor01"><span>普通車教習</span></h3>
			<div class="priceBlock">
				<div class="priceInner">
					<p class="txtNote">5つのコースをご用意しています。<br>
教習料金は、選択するコース、車種や所持免許の有無や種類によって変動します。</p>
				</div>
				<ul class="listTxt">
					<li id="anc01">
						<h3><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_7.svg" width="60" height="45" alt=""></i><strong><b>基本プラン（フリーコース）</b><em>ライフスタイルに合わせて予約がとれるスタンダードプラン</em></strong></span></h3>
						<div class="desc">
							<div class="item">
								<table class="tablePrice">
									<tr>
										<th colspan="4">車種：AT</th>
									</tr>
									<tr class="first">
										<td>所持免許</td>
										<td>料金(税込み)</td>
										<td >技能数</td>
										<td>学科数</td>
									</tr>
									<tr>
										<td>免許なし</td>
										<td>296,400円<br>
<span>(320,112円)</span></td>
										<td>31</td>
										<td>26</td>
									</tr>
									<tr>
										<td>原付免許</td>
										<td>290,400円<br>
<span>（313,632円）</span></td>
										<td>31</td>
										<td>26</td>
									</tr>
									<tr>
										<td>自二免許</td>
										<td>209,400円<br>
<span>(226,152円)</span></td>
										<td>29</td>
										<td>2</td>
									</tr>
									<tr>
										<th colspan="4">車種：MT</th>
									</tr>
									<tr class="first">
										<td>所持免許</td>
										<td>料金(税込み)</td>
										<td >技能数</td>
										<td>学科数</td>
									</tr>
									<tr>
										<td>免許なし</td>
										<td>310,800円<br>
<span>(335,664円)</span></td>
										<td>34</td>
										<td>26</td>
									</tr>
									<tr>
										<td>原付免許</td>
										<td>304,800円<br>
<span>（329,184円）</span></td>
										<td>34</td>
										<td>26</td>
									</tr>
									<tr>
										<td>自二免許</td>
										<td>223,800円<br>
<span>(241,704円)</span></td>
										<td>32</td>
										<td>2</td>
									</tr>
									<tr>
										<td>限定解除</td>
										<td>56,000円<br>
<span>(60,480円)</span></td>
										<td>4</td>
										<td>-</td>
									</tr>
								</table>
								<p class="itemTxt">※上記料金の他に、仮免申請手数料1,700円、仮免交付手数料1,150円、本免申請手数料1,750円が必要です。</p>
							</div>
						</div>
					</li>
					<li id="anc02">
						<h3><span><i class="ico icoStyle"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_animal.svg" width="57" height="89" alt=""></i><strong><b><small>22歳以下限定！</small>サイっちプラン</b><em>上記フリーコースと同額で、うれしいオプションがついた、<br class="pc">22歳以下の方限定の大変お得なプランです！</em></strong></span></h3>
						<div class="desc">
							<div class="item">
								<div class="optionBox">
									<p>
										<span><i>OPTION</i><em>1</em></span>
										<b><i>スケジュール作成</i>無料！</b>
									</p>
									<p>
										<span><i>OPTION</i><em>2</em></span>
										<b><i>夜間の追加料金</i>無料！</b>
									</p>
									<p>
										<span><i>OPTION</i><em>3</em></span>
										<b><i>技能教習の追加料金</i>無料！</b>
									</p>
									<span class="img"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_animal.svg" alt=""></span>
								</div>

								<p class="btn01"><span>一般料金よりも4万円以上もお得！</span></p>
								<p>お客様のご予定に合わせてスタッフが技能教習のスケジュールを作成するから楽ちん！さらに、技能教習と夜間の追加料金がかからないので、安心してお申し込みいただけます。</p>
								<table class="tablePrice">
									<tr>
										<th>AT</th>
										<th>MT</th>
									</tr>
									<tr>
										<td>296,400円<br><span>(320,112円)</span></td>
										<td>310,800円<br><span>(335,664円)</span></td>
									</tr>
								</table>
								<p class="itemTxt">※現在、免許を取得していない方の場合の料金です。<br>
※再検定料、再試験料及びキャンセル料は別途必要になります。<br>
※上記料金の他に、仮免申請手数料1,700円、仮免交付手数料1,150円、本免申請手数料1,750円が必要です。</p>
							</div>
						</div>
					</li>
					<li id="anc03">
						<h3><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_17.svg" width="51" height="51" alt=""></i><strong><b>短期コース</b><em>目標は最短21日で卒業！<br>とにかく早く免許を取りたい方におすすめ。</em></strong></span></h3>
						<div class="desc">
							<div class="item">
								<p class="txtNote">当校オリジナルのスケジュールで短期間での免許取得を目指すコースです。 <br>
基本プランに２万円追加で、短期コースに変更可能です。</p>
								<table class="tablePrice">
									<tr>
										<th>AT</th>
										<th>MT</th>
									</tr>
									<tr>
										<td>316,400円<br><span>(341,712円)</span></td>
										<td>330,800円<br><span>(357,264円)</span></td>
									</tr>
								</table>
								<p class="itemTxt">※再検定料、再試験料、追加教習料、補習料及びキャンセル料は別途必要になります。<br>
※時季等によってオプション料・優先予約数が異なります。<br>
※上記料金の他に、仮免申請手数料1,700円、仮免交付手数料1,150円、本免申請手数料1,750円が必要です。</p>
							</div>
						</div>
					</li>
					<li  id="anc04">
						<h3><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_16.svg" width="52" height="52" alt=""></i><strong><b>すけじゅーるコース</b><em>お客様のスケジュールに合わせてプランを作成!</em></strong></span></h3>
						<div class="desc">
							<div class="item">
								<p class="txtNote">「クラブやサークル、アルバイトに勉強」と忙しい学生さんや、仕事をお持ちで通う時間が限られているお客様にオススメ！ ご予定に合わせて技能教習スケジュールを作成いたします。<br>
基本プランに２万円追加で、すけじゅーるコースに変更可能です。</p>
								<table class="tablePrice">
									<tr>
										<th>AT</th>
										<th>MT</th>
									</tr>
									<tr>
										<td>316,400円<br><span>(341,712円)</span></td>
										<td>330,800円<br><span>(357,264円)</span></td>
									</tr>
								</table>
								<p class="itemTxt">※再検定料、再試験料、追加教習料、補習料及びキャンセル料は別途必要になります。<br>
※時季等によってオプション料・優先予約数が異なります。<br>
※上記料金の他に、仮免申請手数料1,700円、仮免交付手数料1,150円、本免申請手数料1,750円が必要です。</p>
							</div>
						</div>
					</li>
					<li id="anc05">
						<h3><span><i class="ico"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_15.svg" width="55" height="55" alt=""></i><strong><b>安心パックコース</b><em>技能教習の追加料金なし!</em></strong></span></h3>
						<div class="desc">
							<div class="item">
								<p class="txtNote">卒業までの教習料金はもちろん検定料金まで全て含んだコースです。<br>
基本プランに２万円追加で、すけじゅーるコースに変更可能です。<br>
※追加教習時限が少なければ、キャッシュバックがあります。 </p>
								<table class="tablePrice">
									<tr>
										<th>AT</th>
										<th>MT</th>
									</tr>
									<tr>
										<td>311,000円<br><span>(335,880円)</span></td>
										<td>325,400円<br><span>(351,432円)</span></td>
									</tr>
								</table>
								<p class="itemTxt">※再検定料、再試験料、追加教習料、補習料及びキャンセル料は別途必要になります。<br>
※上記料金の他に、仮免申請手数料1,700円、仮免交付手数料1,150円、本免申請手数料1,750円が必要です。<br>
※上記料金は18～22歳までの料金一例となります。</p>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<h3 class="mHead" id="anchor02"><span>二輪車教習</span></h3>
			<div class="priceBlock">
				<div class="priceInner">
					<p class="txtNote">教習料金は、車種や所持免許の有無や種類によって変動します。</p>
					<table class="tablePrice sp mt20">
						<tr>
							<th colspan="4">大型自動二輪車(MTのみ)</th>
						</tr>
						<tr class="first">
							<td>所持免許</td>
							<td>料金(税込み)</td>
							<td >技能数</td>
							<td>学科数</td>
						</tr>
						<tr>
							<td>普通・大型<br>免許</td>
							<td>196,400円<br>
<span>(212,112円)</span></td>
							<td>31</td>
							<td>1</td>
						</tr>
						<tr>
							<td>普通自二<br>免許(MT)</td>
							<td>102,300円<br>
<span>（110,484円）</span></td>
							<td>12</td>
							<td>-</td>
						</tr>
						<tr>
							<td>普通自二<br>免許(AT)</td>
							<td>120,300円<br>
<span>(129,924円)</span></td>
							<td>16</td>
							<td>-</td>
						</tr>
						<tr>
							<td>小型自二<br>免許(MT)</td>
							<td>138,300円<br>
<span>(149,364円)</span></td>
							<td>20</td>
							<td>-</td>
						</tr>
						<tr>
							<td>小型自二<br>免許(AT)</td>
							<td>156,300円<br>
<span>(168,804円)</span></td>
							<td>24</td>
							<td>-</td>
						</tr>
					</table>
					<table class="tablePrice sp">
						<tr>
							<th colspan="4">普通自動二輪車MT(400cc以下)</th>
						</tr>
						<tr class="first">
							<td>所持免許</td>
							<td>料金(税込み)</td>
							<td >技能数</td>
							<td>学科数</td>
						</tr>
						<tr>
							<td>免許なし</td>
							<td>205,100円<br>
<span>(221,508円)</span></td>
							<td>19</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>124,000円<br>
<span>(133,920円)</span></td>
							<td>17</td>
							<td>1</td>
						</tr>
						<tr>
							<td>小型自二免許(MT)</td>
							<td>49,300円<br>
<span>(53,244円)</span></td>
							<td>5</td>
							<td>-</td>
						</tr>
					</table>
					<table class="tablePrice sp">
						<tr>
							<th colspan="4">普通自動二輪車AT(400cc以下)</th>
						</tr>
						<tr class="first">
							<td>所持免許</td>
							<td>料金(税込み)</td>
							<td >技能数</td>
							<td>学科数</td>
						</tr>
						<tr>
							<td>免許なし</td>
							<td>187,900円<br>
<span>(202,932円)</span></td>
							<td>15</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>106,800円<br>
<span>(115,344円)</span></td>
							<td>13</td>
							<td>1</td>
						</tr>
					</table>
					<table class="tablePrice sp">
						<tr>
							<th colspan="4">小型自動二輪車MT(125cc以下)</th>
						</tr>
						<tr class="first">
							<td>所持免許</td>
							<td>料金(税込み)</td>
							<td >技能数</td>
							<td>学科数</td>
						</tr>
						<tr>
							<td>免許なし</td>
							<td>175,000円<br>
<span>(189,000円)</span></td>
							<td>12</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>93,900円<br>
<span>(101,412円)</span></td>
							<td>10</td>
							<td>1</td>
						</tr>
					</table>
					<table class="tablePrice sp">
						<tr>
							<th colspan="4">小型自動二輪車AT(125cc以下)</th>
						</tr>
						<tr class="first">
							<td>所持免許</td>
							<td>料金(税込み)</td>
							<td >技能数</td>
							<td>学科数</td>
						</tr>
							<td>免許なし</td>
							<td>162,100円<br>
<span>(175,068円)</span></td>
							<td>9</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>85,300円<br>
<span>(92,124円)</span></td>
							<td>8</td>
							<td>1</td>
						</tr>
					</table>
					<table class="tablePrice pc">
						<tr class="first">
							<th rowspan="6">大型自動二輪車<br>
(MTのみ)</th>
							<td>所持免許</td>
							<td class="size210">料金(税込み)</td>
							<td class="size100">技能数</td>
							<td class="size100">学科数</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>196,400円<br>
<span>(212,112円)</span></td>
							<td>31</td>
							<td>1</td>
						</tr>
						<tr>
							<td>普通自二免許(MT)</td>
							<td>102,300円<br>
<span>（110,484円）</span></td>
							<td>12</td>
							<td>-</td>
						</tr>
						<tr>
							<td>普通自二免許(AT)</td>
							<td>120,300円<br>
<span>(129,924円)</span></td>
							<td>16</td>
							<td>-</td>
						</tr>
						<tr>
							<td>小型自二免許(MT)</td>
							<td>138,300円<br>
<span>(149,364円)</span></td>
							<td>20</td>
							<td>-</td>
						</tr>
						<tr>
							<td>小型自二免許(AT)</td>
							<td>156,300円<br>
<span>(168,804円)</span></td>
							<td>24</td>
							<td>-</td>
						</tr>
						<tr>
							<th rowspan="3">普通自動二輪車<br>
MT<br>
(400cc以下)</th>
							<td>免許なし</td>
							<td>205,100円<br>
<span>(221,508円)</span></td>
							<td>19</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>124,000円<br>
<span>(133,920円)</span></td>
							<td>17</td>
							<td>1</td>
						</tr>
						<tr>
							<td>小型自二免許(MT)</td>
							<td>49,300円<br>
<span>(53,244円)</span></td>
							<td>5</td>
							<td>-</td>
						</tr>
						<tr>
							<th rowspan="2">普通自動二輪車<br>
AT<br>
(400cc以下)</th>
							<td>免許なし</td>
							<td>187,900円<br>
<span>(202,932円)</span></td>
							<td>15</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>106,800円<br>
<span>(115,344円)</span></td>
							<td>13</td>
							<td>1</td>
						</tr>
						<tr>
							<th rowspan="2">小型自動二輪車<br>
MT<br>
 (125cc以下)</th>
							<td>免許なし</td>
							<td>175,000円<br>
<span>(189,000円)</span></td>
							<td>12</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>93,900円<br>
<span>(101,412円)</span></td>
							<td>10</td>
							<td>1</td>
						</tr>
						<tr>
							<th rowspan="2">小型自動二輪車<br>
AT<br>
 (125cc以下)</th>
							<td>免許なし</td>
							<td>162,100円<br>
<span>(175,068円)</span></td>
							<td>9</td>
							<td>26</td>
						</tr>
						<tr>
							<td>普通・大型免許</td>
							<td>85,300円<br>
<span>(92,124円)</span></td>
							<td>8</td>
							<td>1</td>
						</tr>
					</table>
				</div>
			</div>
			<h3 class="mHead" id="anchor03"><span>企業様向け 研修コース</span></h3>
			<div class="priceBlock">
				<div class="priceInner">
					<p class="txtNote">カリキュラムによって料金は変動しますが、お客様からご要望の多いカリキュラムの料金ご参考にしてください。</p>
					<table class="tableStyle">
						<tr>
							<th colspan="2">例：ベーシックプラン</th>
						</tr>
						<tr>
							<td>座学 ＋ 適正検査 ＋ 運転技術診断 (3時間)</td>
							<td>000000円　※00名様参加の場合</td>
						</tr>
					</table>
					<p class="txtNote">お客様のご要望をお聞きして受講時間や予算に応じたカリキュラムを作成することも可能ですので、まずはお問い合わせください。</p>
					<p class="btnStyle yellow"><a href="<?php echo APP_URL_CONTACT;?>" class="button"><span>お問い合わせフォームへ</span></a></p>
				</div>
			</div>
			<h3 class="mHead" id="anchor04"><span>学校様向け 教育コース</span></h3>
			<div class="priceBlock">
				<div class="priceInner">
					<p class="txtNote">カリキュラムによって料金は変動しますが、お客様からご要望の多いカリキュラムの料金ご参考にしてください。</p>
					<table class="tableStyle mb0">
						<tr>
							<th colspan="2">例：幼稚園児対象カリキュラム</th>
						</tr>
						<tr>
							<td>サイっちの着ぐるみと楽しく学ぶ (30分)</td>
							<td>000000円　※00名様参加の場合</td>
						</tr>
					</table>
					<table class="tableStyle mt0">
						<tr>
							<th colspan="2">例：中学生対象カリキュラム</th>
						</tr>
						<tr>
							<td>自転車のルールとマナー<br class="sp">【映像＋講話】 (40-50分)</td>
							<td>000000円　※00名様参加の場合</td>
						</tr>
					</table>
					<p class="txtNote">お客様のご要望をお聞きして受講時間や予算に応じたカリキュラムを作成することも可能ですので、まずはお問い合わせください。</p>
					<p class="btnStyle yellow"><a href="<?php echo APP_URL_CONTACT;?>" class="button"><span>お問い合わせフォームへ</span></a></p>
				</div>
			</div>
			<h3 class="mHead" id="anchor05"><span>高齢者講習</span></h3>
			<div class="priceBlock">
				<div class="priceInner">
					<table class="tableStyle anc05">
						<tr>
							<th colspan="2">75歳未満の方</th>
						</tr>
						<tr>
							<td>講習（2時間）</td>
							<td>5,100円</td>
						</tr>
					</table>
					<p>全ての方に２時間の講習を受けていただきます。</p>
					<table class="tableStyle">
						<tr>
							<th colspan="2">75歳以上の方</th>
						</tr>
						<tr>
							<td>認知機能検査</td>
							<td>750円</td>
						</tr>
					</table>
					<p class="iconArrow"></p>
					<table class="tableStyle">
						<tr>
							<td>講習（2時間）</td>
							<td>5,100円</td>
						</tr>
						<tr>
							<td>講習（2時間）</td>
							<td>7,950円</td>
						</tr>
					</table>
					<p class="txtNote">まず認知機能検査を受けていただいた後、その検査結果によって、受講しなければならない講習が決定します。</p>
					<p class="btnStyle"><a href="<?php echo APP_URL_TEACHING;?>#anc03" class="button"><span>高齢者講習の流れはこちら</span></a></p>
				</div>
			</div>
			<h3 class="mHead" id="anchor06"><span>ペーパードライバー講習</span></h3>
			<div class="priceBlock">
				<div class="priceInner">
					<table class="tableStyle">
						<tr>
							<td>講習00分（一律）</td>
							<td>5,184円</td>
						</tr>
					</table>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/footer.php'); ?>
<script type="text/javascript">

	$('.listTxt li h3').click(function(event){
		event.preventDefault();
		$(this).next().slideToggle('fast');
		$(this).toggleClass('active');
		return false;
	})

</script>
</body>
</html>