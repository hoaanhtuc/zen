<?php
session_start();
include_once('../app_config.php');
/*if(!empty($_SESSION['fromContact'])) unset($_SESSION['fromContact']);
else header('location: '.APP_URL);*/
include(APP_PATH."libs/head.php");

unset($_SESSION['ses_gtime_step2']);
unset($_SESSION['ses_from_step2']);
unset($_SESSION['ses_step3_user']);
unset($_SESSION['ses_step3_admin']);
?>
<meta http-equiv="refresh" content="15; url=<?php echo APP_URL ?>">
<script type="text/javascript">
history.pushState({ page: 1 }, "title 1", "#noback");
window.onhashchange = function (event) {
	window.location.hash = "#noback";
};
</script>
<style>
	#contact {
	    padding-top: 90px;
	}
	@media screen and (max-width:767px) {
		#contact {
		    padding-top: 60px;
		}
	} 
</style>
</head>
<body id="contact" class="indexThx_contact">

	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<h1 class="bHead"><span><i>仮入校お申込み</i><b>APPLICATION</b></span></h1>
			<ul class="breadCum">
				<li><a href="<?php echo APP_URL; ?>">TOP</a></li>
				<li>仮入校お申込み</li>
			</ul>
			<p class="txtStep pc"><img src="<?php echo APP_ASSETS; ?>img/contact/txt_step3.svg" alt="step 3"/></p>
			<p class="txtStep sp"><img src="<?php echo APP_ASSETS; ?>img/contact/txt_step3_sp.svg" alt="step 3"/></p>			
			<div class="indexThx_contact__content">

				<p class="indexThx_contact__content--title">仮入校のお申し込み<br class="sp">ありがとうございました</p>
				<p class="indexThx_contact__content--p01">送信が完了いたしました。確認後、折り返しご連絡させていただきます。<br>
				3営業日以上経ってもご連絡がない場合は、お電話にてお問い合わせください。</p>

				<p class="indexThx_contact__content--tel">TEL：0586-45-2882</p>

				<a class="indexThx_contact__content--link" href="<?php echo APP_URL; ?>">TOPページへ戻る</a>				
			</div>


		</main>
	</div>
	<?php // include(APP_PATH.'libs/contactBox.php') ?>
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include(APP_PATH.'libs/footer.php') ?>
	</body>
</html>