<?php
// session_start();
if(!defined("APP_URL")){
include('../wp/wp-load.php');
get_template_part( 404 );
exit();
}
// avoid clear session
$_SESSION['ses_from_step2'] = true;
//======================================================================================== お問い合わせ確認画面
$br_reg_content = nl2br($reg_content);
?>
<?php if(GOOGLE_RECAPTCHA_KEY_API != '' && GOOGLE_RECAPTCHA_KEY_SECRET != '') { ?>
	<script src="https://www.google.com/recaptcha/api.js?hl=ja" async defer></script>
	<script>function onSubmit(token) { document.getElementById("formSend").submit(); }</script>
	<style>.grecaptcha-badge {display: none}</style>
<?php } ?>
<p class="txtStep pc"><img src="<?php echo APP_ASSETS; ?>img/contact/txt_step2.svg" alt=""/></p>
<p class="txtStep sp"><img src="<?php echo APP_ASSETS; ?>img/contact/txt_step2_sp.svg" alt=""/></p>
<p class="txtContact">下記の情報に御記入の上、【入力内容を確認する】ボタンを押してください。</p>
<form method="post" class="form-1" action="<?php echo $script ?>?g=<?php echo $gtime ?>" name="form1" id="formSend">
	<div class="formBlock">
		<div>
			<p class="hid_url">Leave this empty: <input type="text" name="url" value="<?php echo $reg_url ?>"></p><!-- Anti spam part1: the contact form -->
			<table class="tableContact tableConfirm">
			<tr>
				<th><p>取得を希望する免許</p></th>
				<td><div><?php echo $reg_sl_license; ?></div></td>
			</tr>
			<tr>
				<th><p>取得済みの免許</p></th>
				<td><div><?php echo $reg_choose; ?><br>
					<?php
						$strCheckbox = implode(',', $reg_check01);
						echo $strCheckbox;
						?>
				</div></td>
			</tr>
		<?php if(!empty($reg_sl_person)) { ?>
			<tr>
				<th><p>希望コースがお決まりの方</p></th>
				<td><div><?php echo $reg_sl_person; ?></div></td>
			</tr>
		<?php } ?>
			<tr>
				<th><p>お名前</p></th>
				<td><div><?php echo $reg_name; ?></div></td>
			</tr>
		<?php if(!empty($reg_furi)) { ?>
			<tr>
				<th><p>フリガナ</p></th>
				<td><div><?php echo $reg_furi; ?></div></td>
			</tr>
		<?php } ?>
			<tr>
				<th><p>性別</p></th>
				<td><div><?php echo $reg_gender; ?></div></td>
			</tr>
			<tr>
				<th><p>生年月日</p></th>
				<td><div><?php echo $reg_date; ?></div></td>
			</tr>
			<tr>
				<th><p>住所</p></th>
				<td><div>〒<?php echo $reg_zipcode; ?><br><?php echo $reg_add; ?></div></td>
			</tr>
			<tr>
				<th><p>電話番号</p></th>
				<td><div><?php echo $reg_tel; ?></div></td>
			</tr>
			<tr>
				<th><p>メールアドレス</p></th>
				<td><div><?php echo $reg_email; ?></div></td>
			</tr>
		<?php if(!empty($reg_yearNum2) || !empty($reg_sl02)) { ?>
			<tr>
				<th><p>入校希望時期</p></th>
				<td><div><?php if(!empty($reg_yearNum2)){ echo $reg_yearNum2.'年 ';} ?><?php echo $reg_sl02; ?>月</div></td>
			</tr>
		<?php } ?>
		<?php if(!empty($reg_content)) { ?>
			<tr>
				<th><p>備考</p></th>
				<td><div><?php echo $br_reg_content; ?></div></td>
			</tr>
		<?php } ?>
		</table>
		</div>
		<input type="hidden" name="sl_license" value="<?php echo $reg_sl_license ?>">
		<input type="hidden" name="sl_person" value="<?php echo $reg_sl_person ?>">
		<input type="hidden" name="sl02" value="<?php echo $reg_sl02 ?>">
		<input type="hidden" name="choose" value="<?php echo $reg_choose ?>">
		<input type="hidden" name="checkAll01" value="<?php echo $strCheckbox ?>">
		<input type="hidden" name="nameuser" value="<?php echo $reg_name ?>">
		<input type="hidden" name="furi" value="<?php echo $reg_furi ?>">
		<input type="hidden" name="gender" value="<?php echo $reg_gender ?>">
		<input type="hidden" name="zipcode" value="<?php echo $reg_zipcode ?>">
		<input type="hidden" name="date" value="<?php echo $reg_date  ?>" />
		<input type="hidden" name="add" value="<?php echo $reg_add ?>">
		<input type="hidden" name="tel" value="<?php echo $reg_tel ?>">
		<input type="hidden" name="email" value="<?php echo $reg_email ?>">
		<input type="hidden" name="yearNum2" value="<?php echo $reg_yearNum2 ?>">
		<input type="hidden" name="content" value="<?php echo $reg_content ?>">
		<!-- always keep this -->
		<input type="hidden" name="url" value="<?php echo $reg_url ?>">
		<!-- end always keep this -->

		<p class="btnBack"><a href="javascript:history.back()">入力内容を修正する</a></p>
		<p class="btnConfirm">
			<?php if(GOOGLE_RECAPTCHA_KEY_API != '') { ?>
				<button name="action" value="send" class="g-recaptcha" data-size="invisible" data-sitekey="<?php echo GOOGLE_RECAPTCHA_KEY_API ?>" data-callback="onSubmit"><span>この内容で送信する</span></button>
			<?php } else { ?>
				<button id="btn"><span>この内容で送信する</span></button>
			<?php } ?>
			<input type="hidden" name="action" value="send">
		</p>
		<p class="txtContact01">上記フォームで送信できない場合は、必要項目をご記入の上、
		<a id="mailContact" href="#"></a>までメールをお送りください。</p><!-- Anti spam part2: clickable email address -->
	</div>
</form> 