<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../../');
include_once($path.'/app_config.php');
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="standard" class='motobike'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php get_banner_page_common();?>
			<div class="box-des-img">
				<div class="imgs">
					<img src="<?php echo APP_ASSETS_IMG;?>motobike/img00@2x.jpg" alt="二輪車">
				</div>
				<div class="text">
					<span>二輪車</span>
				</div>
				<div class="icon_animal">
					<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="113" height="180" alt=""></i>
					<span>教習案内</span>
				</div>
			</div>
			<div class="container-1000">
				<ul class="anchor2 clearfix">
					<li><a href="#anc01"><span>概要</span></a></li>
					<li><a href="#anc02"><span>カリキュラム</span></a></li>
					<li><a href="#anc03"><span>入校申し込み</span></a></li>
					<li><a href="#anc04"><span>お支払いについて</span></a></li>
				</ul>
			</div>
			<div class="box">
				<h3 id="anc01" class="mHead"><span>概要</span></h3>
				<div class="container-800 box_01">
					<table class="tablePrice">
						<tbody>
							<tr>
								<th rowspan="3" class="alignTop">取得車種</th>
								<td>大型自動二輪車（MTのみ）<br>排気量：制限なし</td>
							</tr>
							<tr>
								<td>普通自動二輪車（MT／AT）<br>排気量：400cc以下</td>
							</tr>
							<tr>
								<td>小型自動二輪車（MT／AT）<br>排気量：125cc以下</td>
							</tr>
							<tr>
								<th>教習時間</th>
								<td>仮月〜金　0:00-00:00 <br>仮土日祝　0:00-00:00</td>
							</tr>
							<tr>
								<th>検定日</th>
								<td>修了検定　仮・月・火・水<br>卒業検定　仮・月・火・水</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box">
				<h3 id="anc02" class="mHead"><span>カリキュラム</span></h3>
				<div class="container-1000 box_05">
					<p class="taC">技能数・学科数は、車種や所持免許の有無や種類によって変動します。</p>
				</div>
				<h4 class="sHead"><span>大型二輪の場合</span></h4>
				<div class="container-1000 box_02">
					<figure class="taC">
						<img class="pc" src="<?php echo APP_ASSETS_IMG;?>motobike/img01.svg" width="715" height="265" alt="">
						<img class="sp" src="<?php echo APP_ASSETS_IMG;?>motobike/img01_sp.svg" width="715" height="265" alt="">
						<figcaption>※当校では、普通二輪車の免許を取得済みの方のみ、大型二輪のコースを受けていただけます。</figcaption>
					</figure>
				</div>
				<h4 class="sHead"><span>普通-所持免許なしの場合</span></h4>
				<div class="container-1000 box_02">
					<figure class="taC">
						<img class="pc" src="<?php echo APP_ASSETS_IMG;?>motobike/img02.svg" width="715" height="265" alt="">
						<img class="sp" src="<?php echo APP_ASSETS_IMG;?>motobike/img02_sp.svg" width="715" height="265" alt="">
					</figure>
				</div>
			</div>
			<div class="box">
				<h3 id="anc03" class="mHead"><span>入校申し込みについて</span></h3>
				<h4 class="sHead"><span>入校資格</span></h4>
				<div class="container-800 box_03">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>年齢</th>
								<td>
									<p class="circle">大型自動二輪</p>
									満18歳以上（修了検定日までに18歳になっていること）<br><br>
									<p class="circle">普通二輪、小型限定</p>
									満16歳以上（卒業検定日までに16歳になっていること）
								</td>
							</tr>
							<tr>
								<th>視力</th>
								<td>両眼で0.7以上、かつ片眼でそれぞれ0.3以上（メガネ・コンタクト可）</td>
							</tr>
							<tr>
								<th>色別</th>
								<td>赤色、青色、黄色の識別ができる</td>
							</tr>
							<tr>
								<th>その他</th>
								<td>法令で定められた基準を満たすこと</td>
							</tr>
						</tbody>
					</table>
					<div class="box_text_T01">
						<p class="box_text_T01--title">下記に該当する方は、事前にご相談ください</p>
						<ul>
							<li><p>聴力について障害をお持ちの方。 </p></li>
							<li><p>運動能力で、身体に障害をお持ちの方。<br>事前に運転免許試験場（運転適性相談窓口）にて適性相談をお受け下さい。</p></li>
							<li><p>違反歴のある方、取消歴のある方。</p></li>
						</ul>
					</div>
				</div>
				<h4 class="sHead"><span>入校申し込み受付</span></h4>
				<div class="container-800 box_04">
					<p>月〜土　9:30-20:00 <br>日・祝　9:30-15:30</p>
					<div class="box_text_T01">
						<p class="box_text_T01--title">申し込みに必要なもの</p>
						<ul>
							<li><p>本籍地が入った住民票or運転免許証 </p></li>
							<li><p>メガネ・コンタクト（必要な方）</p></li>
							<li><p>健康保険証・パスポート・住民基本台帳カードのいずれか（免許のある方は不要） <br>※外国人の方は国籍地入りの住民票と外国人在留カード</p></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box">
				<h3 id="anc04" class="mHead"><span>お支払いについて</span></h3>
				<h4 class="sHead"><span>料金</span></h4>
				<div class="container-800 box_05">
					<p class='taC'>所持免許、MTかATかなど、条件よって料金が変わります。</p>
				</div>
				<h4 class="sHead"><span>一括払いの場合</span></h4>
				<div class="container-800 box_06">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>現金</th>
								<td>入校日当日または、それ以前にご用意下さい。</td>
							</tr>
							<tr>
								<th>振込</th>
								<td>入校日当日または、それ以前にお振込み下さい。</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4 class="sHead"><span>教育ローンをご利用の場合</span></h4>
				<div class="container-800 box_06">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>分割</th>
								<td>3～36回までの分割が可能です。</td>
							</tr>
							<tr>
								<th>審査</th>
								<td>事前に審査等がありますので、お手続きには多少お時間がかかります。</td>
							</tr>
						</tbody>
					</table>
					<div class="btn_inside_border">
						<a href="<?php echo APP_URL_PRICE;?>">料金ページで詳細を見る</a>
					</div>
				</div>
			</div>
			
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
</body>
</html>