<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../../');
include_once($path.'/app_config.php');
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="standard" class='standard'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php get_banner_page_common();?>
			<div class="box-des-img">
				<div class="imgs">
					<img src="<?php echo APP_ASSETS_IMG;?>standard/img00@2x.jpg" alt="普通車">
				</div>
				<div class="text">
					<span>普通車</span>
				</div>
				<div class="icon_animal">
					<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="113" height="180" alt=""></i>
					<span>教習案内</span>
				</div>
			</div>
			<div class="container-1000">
				<ul class="anchor2 clearfix">
					<li><a href="#anc01"><span>概要</span></a></li>
					<li><a href="#anc02"><span>カリキュラム</span></a></li>
					<li><a href="#anc03"><span>入校申し込み</span></a></li>
					<li><a href="#anc04"><span>お支払いについて</span></a></li>
				</ul>
			</div>
			<div class="box">
				<h3 id="anc01" class="mHead"><span>概要</span></h3>
				<div class="container-800 box_01">
					<table class="tablePrice">
						<tbody>
							<tr>
								<th>取得車種</th>
								<td>普通車（第一種運転免許）</td>
							</tr>
							<tr>
								<th>教習時間</th>
								<td>仮月〜金　0:00-00:00 <br>仮土日祝　0:00-00:00</td>
							</tr>
							<tr>
								<th>検定日</th>
								<td>修了検定　仮・月・火・水<br>卒業検定　仮・月・火・水</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box">
				<h3 id="anc02" class="mHead"><span>カリキュラム</span></h3>
				<div class="container-1000 box_02">
					<div class="box_text">現有免許なしの場合</div>
					<div class="pc">
						<img src="<?php echo APP_ASSETS_IMG;?>standard/img01.svg" width="1000" height="348" alt="">
					</div>
					<div class="sp"><img src="<?php echo APP_ASSETS_IMG;?>standard/img01_sp.svg" width="1000" height="348" alt=""></div>
				</div>
			</div>
			<div class="box">
				<h3 id="anc03" class="mHead"><span>入校申し込みについて</span></h3>
				<h4 class="sHead"><span>入校資格</span></h4>
				<div class="container-800 box_03">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>年齢</th>
								<td>満18歳以上（修了検定日までに18歳になっていること）</td>
							</tr>
							<tr>
								<th>視力</th>
								<td>両眼で0.7以上、かつ片眼でそれぞれ0.3以上（メガネ・コンタクト可）</td>
							</tr>
							<tr>
								<th>色別</th>
								<td>赤色、青色、黄色の識別ができる</td>
							</tr>
							<tr>
								<th>その他</th>
								<td>法令で定められた基準を満たすこと</td>
							</tr>
						</tbody>
					</table>
					<div class="box_text_T01">
						<p class="box_text_T01--title">下記に該当する方は、事前にご相談ください</p>
						<ul>
							<li><p>聴力について障害をお持ちの方。 </p></li>
							<li><p>運動能力で、身体に障害をお持ちの方。<br>事前に運転免許試験場（運転適性相談窓口）にて適性相談をお受け下さい。</p></li>
							<li><p>違反歴のある方、取消歴のある方。</p></li>
						</ul>
					</div>
				</div>
				<h4 class="sHead"><span>入校申し込み受付</span></h4>
				<div class="container-800 box_04">
					<p>月〜土　9:30-20:00 <br>日・祝　9:30-15:30</p>
					<div class="box_text_T01">
						<p class="box_text_T01--title">申し込みに必要なもの</p>
						<ul>
							<li><p>本籍地が入った住民票or運転免許証 </p></li>
							<li><p>メガネ・コンタクト（必要な方）</p></li>
							<li><p>健康保険証・パスポート・住民基本台帳カードのいずれか（免許のある方は不要） <br>※外国人の方は国籍地入りの住民票と外国人在留カード</p></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box box__a">
				<h3 id="anc04" class="mHead"><span>お支払いについて</span></h3>
				<h4 class="sHead"><span>料金</span></h4>
				<div class="container-800 box_05">
					<p>コース、所持免許、MTかATかなど、条件よって料金は変動します。詳細は<a href="<?php echo APP_URL_PRICE;?>">料金ページ</a>でご案内しております。</p>
					<ul class="box_list_T01">
						<li><a href="<?php echo APP_URL_PRICE;?>#anc01">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_7.svg" width="43"  height="32" alt="フリーコース"></i>
							<span>
								<b>フリーコース</b>
								ライフスタイルに合わせて予約がとれるスタンダードプラン
							</span>
						</a></li>
						<li><a href="<?php echo APP_URL_PRICE;?>#anc02">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="45"  height="71" alt="サイっちプラン"></i>
							<span>
								<b>サイっちプラン</b>
								22歳以下の方限定！当校1番人気！
							</span>
						</a></li>
						<li><a href="<?php echo APP_URL_PRICE;?>#anc03">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_17.svg" width="36"  height="36" alt="短期コース"></i>
							<span>
								<b>短期コース</b>
								目標は最短21日で卒業！とにかく早く免許を取りたい方におすすめ。
							</span>
						</a></li>
						<li><a href="<?php echo APP_URL_PRICE;?>#anc04">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_16.svg" width="39"  height="39" alt="すけじゅーるコース"></i>
							<span>
								<b>すけじゅーるコース</b>
								お客様のスケジュールに合わせてプランを作成!
							</span>
						</a></li>
						<li><a href="<?php echo APP_URL_PRICE;?>#anc05">
							<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_15.svg" width="43"  height="43" alt="安心パックコース"></i>
							<span>
								<b>安心パックコース</b>
								技能教習の追加料金なし!
							</span>
						</a></li>
					</ul>
				</div>
				<h4 class="sHead"><span>一括払いの場合</span></h4>
				<div class="container-800 box_06">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>現金</th>
								<td>入校日当日または、それ以前にご用意下さい。</td>
							</tr>
							<tr>
								<th>振込</th>
								<td>入校日当日または、それ以前にお振込み下さい。</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4 class="sHead"><span>3分割払い（無利息）</span></h4>
				<div class="container-800 box_07">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>1回目</th>
								<td>入校日当日または、それ以前にご用意下さい。</td>
							</tr>
							<tr>
								<th>2・3回目</th>
								<td>納入金額が不足次第、お支払いください。</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4 class="sHead"><span>教育ローンをご利用の場合</span></h4>
				<div class="container-800 box_08">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>分割</th>
								<td>3～36回までの分割が可能です。</td>
							</tr>
							<tr>
								<th>審査</th>
								<td>事前に審査等がありますので、お手続きには多少お時間がかかります。</td>
							</tr>
						</tbody>
					</table>
					<div class="btn_inside_border">
						<a href="<?php echo APP_URL_PRICE;?>">料金ページで詳細を見る</a>
					</div>
				</div>
			</div>
			<div id="group02" class="box-des-img">
				<div class="imgs">
					<img src="<?php echo APP_ASSETS_IMG;?>standard/img02@2x.jpg" alt="普通二種">
				</div>
				<div class="text">
					<span>普通二種</span>
				</div>
				<div class="icon_animal">
					<i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="113" height="180" alt=""></i>
					<span>教習案内</span>
				</div>
			</div>
			<div class="box">
				<div class="container-800 box_09">
					<p>お客さまを安全に目的地まで乗せて報酬を受け取るために高度な安全運転テクニックと安全意識が必要な運転免許です。</p>
				</div>
			</div>
			<div class="box">
				<h3 id="anc04" class="mHead"><span>概要</span></h3>
				<div class="container-800 box_10">
					<table class="tablePrice">
						<tbody>
							<tr>
								<th>取得車種</th>
								<td>普通車（第二種運転免許）</td>
							</tr>
							<tr>
								<th>教習時間</th>
								<td>仮月〜金　0:00-00:00 <br>仮土日祝　0:00-00:00</td>
							</tr>
							<tr>
								<th>検定日</th>
								<td>修了検定　仮・月・火・水<br>卒業検定　仮・月・火・水</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box">
				<h3 id="anc04" class="mHead"><span>入校申し込みについて</span></h3>
				<h4 class="sHead"><span>入校資格</span></h4>
				<div class="container-800 box_03">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>年齢</th>
								<td>満21歳以上 <br>（※大型・普通・大型特殊免許のいずれかを受けており、これらの免許を受けている期間 <br>（免許の効力が停止されている期間を除く）が通算して3年以上）</td>
							</tr>
							<tr>
								<th>視力</th>
								<td>両眼で0.7以上、かつ片眼でそれぞれ0.3以上（メガネ・コンタクト可）</td>
							</tr>
							<tr>
								<th>深視力</th>
								<td>検査3回の平均誤差が2cm以下</td>
							</tr>
							<tr>
								<th>聴力</th>
								<td>補聴器を使用しないで10ｍの距離で90dbの警音器の音が聞こえること。</td>
							</tr>
						</tbody>
					</table>
					<div class="box_text_T01">
						<p class="box_text_T01--title">下記に該当する方は、事前にご相談ください</p>
						<ul>
							<li><p>聴力について障害をお持ちの方。</p></li>
							<li><p>運動能力で、身体に障害をお持ちの方。<br>事前に運転免許試験場（運転適性相談窓口）にて適性相談をお受け下さい。</p></li>
							<li><p>違反歴のある方、取消歴のある方。</p></li>
						</ul>
					</div>
				</div>
				<h4 class="sHead"><span>入校申し込み受付</span></h4>
				<div class="container-800 box_04">
					<p>月〜土　9:30-20:00 <br>日・祝　9:30-15:30</p>
					<div class="box_text_T01">
						<p class="box_text_T01--title">申し込みに必要なもの</p>
						<ul>
							<li><p>運転免許証</p></li>
							<li><p>メガネ・コンタクト（必要な方）</p></li>
							<li><p>講習料金（入校日当日でも可）</p></li>
						</ul>
					</div>
				</div>
				<h4 class="sHead"><span>料金</span></h4>
				<div class="container-800 box_05">
					<p>所持免許、MTかATかなど、条件よって料金が変わります。詳しくは<a href="<?php echo APP_URL_PRICE;?>">こちら</a>をご覧ください。</p>
				</div>
				<h4 class="sHead"><span>一括払いの場合</span></h4>
				<div class="container-800 box_06">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>現金</th>
								<td>入校日当日または、それ以前にご用意下さい。</td>
							</tr>
							<tr>
								<th>振込</th>
								<td>入校日当日または、それ以前にお振込み下さい。</td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4 class="sHead"><span>教育ローンをご利用の場合</span></h4>
				<div class="container-800 box_07">
					<table class="table_T01">
						<tbody>
							<tr>
								<th>分割</th>
								<td>3～36回までの分割が可能です。</td>
							</tr>
							<tr>
								<th>審査</th>
								<td>事前に審査等がありますので、お手続きには多少お時間がかかります。</td>
							</tr>
						</tbody>
					</table>
					<div class="btn_inside_border">
						<a href="<?php echo APP_URL_PRICE;?>">料金ページで詳細を見る</a>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->
	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
	<script src="<?php echo APP_ASSETS_JS;?>lib/jquery.matchHeight.min.js"></script>
	<script>
			$('.box_list_T01 li').matchHeight();
	</script>
</body>
</html>