<?php
// Author: A+LIVE
$path = realpath(dirname(__FILE__).'/../');
include_once($path.'/app_config.php');
if(@file_exists(APP_PATH.'wp/wp-load.php')){
	include_once(APP_PATH.'wp/wp-load.php') ;
}else{
	include_once(APP_PATH.'wp-load.php') ;
}
include_once(APP_PATH.'libs/head.php');
?>
</head>
<body id="about" class='about'>
	<!-------------------------------------------------------------------------
	HEADER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/header.php'); ?>
	<div id="wrap">
		<!-- Main Content
		================================================== -->
		<main>
			<?php get_banner_page_common();?>
			
			<div class="container-1000">
				<ul class="anchor2 clearfix">
					<li><a href="#anc01"><span>施設紹介</span></a></li>
					<li><a href="#anc02"><span>スタッフ紹介</span></a></li>
					<li><a href="#anc03"><span>アクセス</span></a></li>
					<li><a href="#anc04"><span>学校概要</span></a></li>
				</ul>
			</div>
			<div class="box">
				<h3 id="anc01" class="mHead"><span>施設紹介</span></h3>
				<div class="container-1000 box_01">
					<div class="container-800">
						<p>仮）ここに学校紹介の文章が入ります、ここに学校紹介の文章が入ります。ここに学校紹介の文章が入ります、ここに学校紹介の文章が入ります。</p>
					</div>
					<ul class="list_introduction">
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img01@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>コース</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img02@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>受付</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img03@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>待合スペース</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img04@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>パウダールーム</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img05@2x.jpg" alt="">
									<i>
										<img class="pc" src="<?php echo APP_ASSETS_IMG;?>about/img_rb01.png" width="287" height="153" alt="ブランケットの貸出あり♪">
										<img class="sp" src="<?php echo APP_ASSETS_IMG;?>about/img_rb01_sp.png" width="238" height="126" alt="ブランケットの貸出あり♪">
									</i>
								</div>
								<div class="info">
									<span>教室</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
						<li>
							<div class="list_introduction_wrap">
								<div class="imgs">
									<img src="<?php echo APP_ASSETS_IMG;?>about/img06@2x.jpg" alt="">
								</div>
								<div class="info">
									<span>自習室</span>
									<p>仮）ここに施設についての文章が入ります、ここに施設についての文章が入ります。</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
<?php
	/*$args_staff = array(
		'post_type' => 'staff',
		'post_status' => 'publish',
		'posts_per_page' => -1,
	);
	$wp_query = new WP_Query();
	$staffs = $wp_query->query($args_staff);*/
	$args = array(
		'post_type'                => 'staff',
		'orderby'                  => 'order',
		'order'                    => 'asc',
		'hide_empty'               => 1,
		'taxonomy'                 => 'staffcat',
		'pad_counts'               => false ,
	);

	$categories = get_categories( $args );
?>
			<div class="box blist_over">
				<h3 id="anc02" class="mHead"><span>スタッフ紹介</span></h3>
<?php 
$i=1;
$j=0;
	foreach($categories as $cate){
		$i++;
?>
				<h4 class="sHead"><span><?php echo $cate->name;?></span></h4>
				<div class="container-1000 box_0<?php echo $i;?>">
					<div class="list_staff">
					<?php 
						$args_staff = array(
							'post_type'=>'staff',
							'order' => 'DESC',
							'post_status' => 'publish',
							'posts_per_page' => -1,
							'tax_query' => array(
								array(
									'taxonomy' => 'staffcat',
									'field'    => 'term_id',
									'terms'    => $cate->term_id,
								)
							),
						);
						$wp_query_staff = new WP_Query();
						$staffs = $wp_query_staff->query($args_staff);
						foreach($staffs as $staff){
							$j++;
							$img_ft = get_img_src($staff,true,'full');
							$defaults = array(
						        'img' => $img_ft,
						        'width' => 392,
						        'height' => 286,
						        'zoom' => 1,
						        'crop' => true,
						        'quality' => 100,
						    );
						   
					?>
						<figure <?php echo 5 == $j ? "class='mark'" : '';?>>
							<a href="javascript:void(0)">
								<img src="<?php echo thumbCrop($defaults);?>" alt="">
								<figcaption <?php echo get_field('sex',$staff->ID) == 'female' ? 'class="woman"' : '';?>>
									<span><?php echo $staff->post_title;?></span>
									<?php echo get_field('mess',$staff->ID);?>
								</figcaption>
							</a>
						</figure>
					<?php } ?>
					</div>
				</div>
<?php } ?>
				<div class="see_more">
					<div class="btn_inside_border">
						<a href="javascript:void(0)" onclick="toggle_content(this);">もっと見る</a>
					</div>
				</div>
			</div>
			<div class="box">
				<h3 id="anc03" class="mHead"><span>アクセス</span></h3>
				<div class="container-1000 box_05">
					<div class="ggmap">
						<div class="ggmap_iframe">
							<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3255.872445522776!2d136.77042381524865!3d35.309150780282735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6003a6c3d03124d9%3A0x89318cd9b72455b8!2z5bC-6KW_6Ieq5YuV6LuK5a2m5qCh!5e0!3m2!1sja!2s!4v1539073784117&hl=ja&output=embed" width="1000" height="370" frameborder="0" style="border:0" allowfullscreen></iframe> -->
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3255.872445522776!2d136.77042381524865!3d35.309150780282735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6003a6c3d03124d9%3A0x89318cd9b72455b8!2z5bC-6KW_6Ieq5YuV6LuK5a2m5qCh!5e0!3m2!1sja!2s!4v1539252078736&hl=ja&output=embed" width="1000" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<p class="address">〒494-0002　<br class="sp">愛知県一宮市篭屋３丁目12-45</p>
						<p class="gglink"><a href="https://goo.gl/maps/4fsPiPuRDT12" target="_blank">大きな地図で見る</a></p>
						<p class="animal"><i><img src="<?php echo APP_ASSETS_IMG;?>common/icon/ico_animal.svg" width="78" height="123" alt=""></i></p>
					</div>
				</div>

				<h4 id="anc03a" class="sHead"><span>自宅送迎バス</span></h4>
				<div class="container-800 box_06">
					<div class="box_point">
						<p class="box_point_title"><span>無料で便利！<br class="sp"></span>自宅までの送迎で通学も安心</p>
						<div class="left pc">
							<img src="<?php echo APP_ASSETS_IMG;?>/about/img07@2x.jpg" alt="">
						</div>
						<div class="right">
							<p>仮）普通免許取得コースの方は、自宅から当校までの個別送迎をご利用いただけます。（片道約25分以内のエリアが対象）<br>
								<img class="sp" src="<?php echo APP_ASSETS_IMG;?>/about/img07@2x.jpg" alt="">
前日までに予約すれば、雨の日や、夜間教習で帰りが暗くなったり寒かったりしても、ラクラク通学できるので安心です。<br>
また、自宅送迎のWeb予約システムにご登録いただくと、どこでも手軽に予約可能。ぜひこの便利なサービスをご利用ください！</p>
<p class="note">※普通免許以外の方は、一宮駅と当校間のシャトルバスのみご利用いただけます。</p>
						</div>
					</div>
					<div class="sp tranform">
						<h4 class="sHead"><span>当校 ⇆ 駅のシャトルバス</span></h4>
						<div class="box_point">
							<p class="box_point_title"><span>JR尾張一宮駅・名鉄一宮駅から</span>の<br class="sp">スクールバスをご利用いただけます</p>
						</div>
						<div class="sp"><div class="container-800 mb20">主要駅は巡回バスが毎時運行中！教習時間に間に合うよう一宮駅を出発します。駅へ向かう便も、教習の終わる時間に合わせて設定。
学校帰りや仕事帰りにも、直接お越しいただくのに便利です。</div></div>
					</div>
					<div class="box_table_time">
						<div class="left">
							<img src="<?php echo APP_ASSETS_IMG;?>/about/img08@2x.jpg" alt="">
						</div>
						<div class="right">
							<p class="title">シャトルバス時刻表</p>
							<ul class="list_time">
								<li>
									<p class="list_time_head">
										一宮駅北口発
									</p>
									<p>9:20</p>
									<p>10:40</p>
									<p>11:40</p>
									<p>13:20</p>
									<p>14:20</p>
									<p>15:20</p>
									<p>16:40</p>
									<p>17:40</p>
									<p>18:40</p>
									<p>19:40</p>
									<p>-</p>
								</li>
								<li>
									<p class="list_time_head">
										自動車学校着
									</p>
									<p>9:30</p>
									<p>10:50</p>
									<p>11:50</p>
									<p>13:30</p>
									<p>14:30</p>
									<p>15:30</p>
									<p>16:50</p>
									<p>17:50</p>
									<p>18:50</p>
									<p>19:50</p>
									<p>-</p>
								</li>
								<li>
									<p class="list_time_head">
										自動車学校発
									</p>
									<p>-</p>
									<p>11:00</p>
									<p>12:40</p>
									<p>13:40</p>
									<p>14:00</p>
									<p>16:00</p>
									<p>17:00</p>
									<p>18:00</p>
									<p>19:00</p>
									<p>20:00</p>
									<p>21:00</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="box">
				<h3 id="anc04" class="mHead"><span>学校概要</span></h3>
				<div class="container-800 box_07">
					<table class="tablePrice">
						<tbody>
							<tr>
								<th>学校名</th>
								<td>尾西自動車学校</td>
							</tr>
							<tr>
								<th>住所</th>
								<td>〒494-0002　<br class="sp">愛知県一宮市篭屋３丁目12-45</td>
							</tr>
							<tr>
								<th>TEL</th>
								<td><a class="phonesp" href="tel:<?php echo PHONE_T02;?>"><?php echo PHONE_T02;?></a></td>
							</tr>
							<tr>
								<th>FAX</th>
								<td><?php echo FAX_T01;?></td>
							</tr>
							<tr>
								<th>WEB</th>
								<td>https://www.bisai.co.jp</td>
							</tr>
							<tr>
								<th>取得車種</th>
								<td>普通自動車、普通2種、普通自動二輪、大型自動二輪</td>
							</tr>
							<tr>
								<th>取扱講習</th>
								<td>ペーバードライバー講習、高齢者講習</td>
							</tr>
						</tbody>
					</table>
					<div class="icon_approve">
						<i><img src="<?php echo APP_ASSETS_IMG;?>about/ico_auto.svg" width="60" height="60" alt=""></i>
						<span>愛知県公安委員会指定</span>
					</div>
				</div>

				<h4 class="sHead"><span>入校資格</span></h4>
				<div class="container-800 box_08">
					<div class="list_entrance">
						<div class="list_entrance_line">
							<span>普通免許</span>
							<p>年齢が18歳以上の方（※誕生日の約1ヶ月前から入校可）</p>
						</div>
						<div class="list_entrance_line">
							<span>普通二種免許</span>
							<p>年齢が21歳以上の方で大型・普通・大型特殊のいずれかの免許を通算して3年以上受けている方。</p>
						</div>
						<div class="list_entrance_line">
							<span>自動二輪免許</span>
							<p>
								大型二輪　年齢が18歳以上の方 <br>
								普通二輪　年齢が16歳以上の方（※誕生日の約1ヶ月前から入校可）
							</p>
						</div>
						<div class="list_entrance_line">
							<span>共通</span>
							<p>
								視力が両眼で0.7以上、片眼0.3以上 <br>
								（左右）で信号機の色の識別が出来る方（眼鏡、コンタクトレンズ可）<br>
								※やさしい漢字が読めて理解できる方。<br>
								※普通二種については、視力が両眼で0.8以上、片眼0.5以上（左右）で深視力の検査において誤差値が20ミリ以内又、補聴器を使用しないで10ｍの距離で90ｄｂの警音器の音が聞こえること。
							</p>
						</div>
					</div>
					<div class="box_text_T01">
						<p class="box_text_T01--title">下記に該当する方は、事前にご相談ください</p>
						<ul>
							<li><p>聴力について障害をお持ちの方。</p></li>
							<li><p>運動能力で、身体に障害をお持ちの方。<br>事前に運転免許試験場（運転適性相談窓口）にて適性相談をお受け下さい。</p></li>
							<li><p>違反歴のある方、取消歴のある方。</p></li>
						</ul>
					</div>
				</div>

				<h4 class="sHead"><span>入校時に必要なもの</span></h4>
				<div class="container-800 box_09">
					<p class="circle">免許証のある方…現在所持している免許証 </p>
					<p class="circle">免許証のない方…本籍記載の住民票1通と身分証明書（健康保険証、パスポートまたは住基カードのうちいずれか1点）</p>
					<p class="circle">写真5枚（写真は無帽・無背景・横2.5×縦3cm上半身正面写真5枚）<br>※当校でも撮影できます。（別途料金）</p>
					<p class="circle">入校申込金　5,000円（税込5,400円）</p>
				</div>

				<h4 class="sHead"><span>入校申し込み受付</span></h4>
				<div class="container-800 box_10">
					<p class="circle">入校希望日の前日までにお申込みください。</p>
					<p class="circle">視力検査など簡単な検査を行いますので、ご本人がおいでください。</p>
				</div>

				<h4 class="sHead"><span>入校日カレンダー</span></h4>
				<div class="box_11">
					<div class="container-800 calendars">
	                	<!-- <div class="wrap">
	                    <?php
						include_once(APP_PATH . 'libs/class_calendar.php');
							$calendar = new Calendar();
							$arrlist = $calendar->setup_data_calendar();
							if(!empty($arrlist)){
								$iz = 0;
								foreach($arrlist as $listday){
									if($iz == 2){
										break;
									}
									echo $calendar->show($listday);
									$iz++;
								}
							}
							/*booking*/
							//$list_select_form = $calendar->list_date_tt($arrlist);
							//usort($list_select_form, array('calendar','compare_lastname'));
						?>
	                    </div>
	                    <div class="notecl">
	                    	<span><i></i><b>お休み</b></span>
	                    	<span class="off"><i></i><b>毎週火曜日　18:00〜20:50まで<br>毎週土曜日　13:40〜16:30まで </b></span>
	                    	<p class="circle">入校説明・適正テスト・学科教習で3時間です</p>
	                    </div> -->
	                    <iframe src="https://calendar.google.com/calendar/b/1/embed?showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=a-livesystems.com_vc5dk81524b7ne07s3v8tlnffg%40group.calendar.google.com&amp;color=%23853104&amp;ctz=Asia%2FSaigon" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</div>
			</div>
		</main>
	</div><!-- #wrap -->


	<!-------------------------------------------------------------------------
	FOOTER
	--------------------------------------------------------------------------->
	<?php include_once(APP_PATH.'libs/footer.php'); ?>
	<script src="<?php echo APP_ASSETS_JS;?>about.min.js"></script>
</body>
</html>